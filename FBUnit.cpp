/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <TurbineEngine.h>
#include <TurbineMath.h>
#include "FBUnit.h"
#include "FBAttackNearest.h"

#include <iostream>

using Turbine::round;
using Turbine::Entity;
using Turbine::Model;
using Turbine::Colorf;
using Turbine::BBox;
using Turbine::Vector2d;
using Turbine::Vector3d;
using Turbine::Vector4d;
using Turbine::Matrix4;
using Turbine::GUIManager;
using Turbine::Engine;

namespace FB
{

Unit::Unit(Entity* entity, Model* bullet, Entity* selectIndicator, const UnitProperties& prop, int teamNum, Map* map) : _map(map), _entity(entity), _bullet(bullet), _selIndicator(selectIndicator), _prop(prop), _destination(-1, -1), _teamNum(teamNum), _HP(prop.health), _reloading(false), _bar(NULL), _barPlacement(0.0, 0.0, 0.0), _barOffset(0.0, 2.0, 0.0)
{
    _strategy = new AttackNearest(_map);

    GUIManager* guimgr = Engine::getInstance()->getGUIManager();

    Colorf colors[4];
    Colorf border;
    if(teamNum == 1)
    {
        colors[0] = Colorf(1.0, 0.69, 0.18, 1.0);
        colors[1] = Colorf(1.0, 0.69, 0.18, 1.0);
        colors[2] = Colorf(1.0, 0.69, 0.18, 1.0);
        colors[3] = Colorf(0.613, 0.39, 0.018, 1.0);
        border = Colorf(0.345, 0.220, 0.063, 1.0);
    }
    else
    {
        colors[0] = Colorf(1.0, 1.0, 1.0, 1.0);
        colors[1] = Colorf(1.0, 1.0, 1.0, 1.0);
        colors[2] = Colorf(1.0, 1.0, 1.0, 1.0);
        colors[3] = Colorf(0.5, 0.5, 0.5, 1.0);
        border = Colorf(0.2, 0.2, 0.2, 1.0);
    }

    _bar = guimgr->createHealthBar(5, 5, 10, colors, 1, border);

    _selIndicator->disable();

    updateHealthBar();
    updateSelectIndicator();
}

Unit::~Unit()
{
    Turbine::SceneManager* smgr = Engine::getInstance()->getSceneManager();
    smgr->destroyEntity(_entity);

    Turbine::GUIManager* guimgr = Engine::getInstance()->getGUIManager();
    guimgr->destroyHealthBar(_bar);

    delete _strategy;
}

void Unit::setPosition(const Vector3d& newpos)
{
    // get 2d screen coordinates from 3d _hudPlacement point
    _entity->setPosition(newpos);
}

void Unit::startUserMove()
{
    MapPosition unitPos = _map->findUnit(getName());
    _userPath = _map->findPath(unitPos, _destination, false);
    if(_userPath.size() > 0)
    {
        _destination.setInvalid();
        _userPath.pop_front();
    }
}

void Unit::idle()
{
    std::string actionName = _entity->getActionName();
    if(actionName.compare("Idle") != 0) // start idle
    {
        _entity->setAction("Idle");
        _entity->setActionLoop(true);
    }
}

void Unit::move()
{
    std::string actionName = _entity->getActionName();
    _entity->setAction("Move");
    _entity->setActionLoop(true);
    _entity->setForwardAction();
}

void Unit::rotate(float degrees)
{
    _entity->setAction("Rotate");
    _entity->setActionLoop(true);
    if(degrees <= 0)
        _entity->setBackwardAction(); // left
    else
        _entity->setForwardAction(); // right
}

void Unit::attack()
{
    _entity->setAction("Attack");
    _entity->setActionLoop(false);
    _entity->setForwardAction();
}

void Unit::die()
{
    _entity->setAction("Die");
    _entity->setActionLoop(false);
    _entity->setForwardAction();
}

void Unit::celebrate()
{
    std::string actionName = _entity->getActionName();
    if(actionName.compare("Celebrate") != 0)
    {
        _entity->setAction("Celebrate");
        _entity->setActionLoop(true);
        _entity->setForwardAction();
    }
}

void Unit::changeUnitStrategy(UNIT_STRATEGY strategy)
{
    delete _strategy;

    switch(strategy)
    {
        case STRAT_ATTACK_NEAREST:
        {
            _strategy = new AttackNearest(_map);
            break;
        }
    }
}

Event* Unit::getNextAction(float time)
{
    // test user move action
    if(_userPath.size() > 0)
    {
        // before move, test unit orientation
        MapPosition step = _userPath.front();
        float rotation = _map->getRotation(getName(), step);
        if(round(rotation) != 0.0)
            return new RotationEvent(shared_from_this(), time, rotation);

        _userPath.pop_front();
        return new MoveEvent(shared_from_this(), time, step);
    }

    // otherwise use the AI
    return _strategy->getNextAction(shared_from_this(), time);
}

void Unit::updateHealthBar()
{
    // get pelvis for x and z values
    Vector4d dotTrans = Vector4d() * _entity->getBonesMatrices()[2];

    // project 3d dot into view plane
    Vector3d dotTrans3(dotTrans.x, dotTrans.y, dotTrans.z);
    dotTrans3 += _barOffset;
    Matrix4 modelMatrix = _entity->getTransformation();
    Vector3d pos = Engine::getInstance()->project(dotTrans3, modelMatrix);

    // move the hud half-size left/down
    Vector2d offset;
    Vector2d windowSize = Engine::getInstance()->getWindowSize();
    offset.x = _bar->getImageSet()->width / 2.0;
    offset.y = _bar->getImageSet()->height / 2.0;
    // convert to 0..1 offset
    pos.x -= offset.x / windowSize.x;
    pos.y -= offset.y / windowSize.y;
    _bar->setOffset(Vector2d(pos.x, pos.y));
}


void Unit::updateSelectIndicator()
{
    Vector4d dotTrans = Vector4d() * _entity->getBonesMatrices()[2];
    Matrix4 modelMatrix = _entity->getTransformation();
    dotTrans = dotTrans * modelMatrix;
    _selIndicator->setPosition(Vector3d(dotTrans.x, 0.1, dotTrans.z));
}


void Unit::show()
{
    _entity->enable();
    _bar->enable();
}

void Unit::hide()
{
    _entity->disable();
    _bar->disable();
}

}
