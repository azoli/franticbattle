/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FBLEVEL_H
#define FBLEVEL_H

#include <TurbineSceneManager.h>
#include "FBMap.h"
#include "FBSimulator.h"

namespace FB
{

class Level
{
    Turbine::SceneManager* _smgr;
    Map _map;
    Simulator _sim;
    std::vector<std::pair<int, UnitParameters>> unitsParams;
    int _index;

    void _addUnits(std::pair<int, UnitParameters> params);

    public:

    Level();
    ~Level();
    Turbine::SceneManager* getSceneManager(){ return _smgr; }
    Map* getMap(){ return &_map; }
    Simulator* getSimulator(){ return &_sim; }
    void addUnits(unsigned int num, const UnitParameters& params);
    void start();
    void restart();
};

}

#endif
