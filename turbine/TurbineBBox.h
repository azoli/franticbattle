/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef BBOX_H
#define BBOX_H

#include <vector>
#include <iostream>
#include "TurbineVector3d.h"

namespace Turbine
{

class BBox
{
    Vector3d _min;
    Vector3d _max;

public:

    BBox();
    BBox(const Vector3d& v1, const Vector3d& v2);   // create BB from 2 vectors
    ~BBox(){}
    void init(const std::vector<Vector3d>& points); // create BB from a set of points

    Vector3d getMin(){ return _min; }
    Vector3d getmax(){ return _max; }
    // get corners: bottom-front-left, bottom-front-right, bottom-back-right,
    // bottom-back-left, top-front-left, top-front-right, top-back-right,
    // top-back-left
    std::vector<Vector3d> getCorners();
    Vector3d getCenter();
    float getHeight();
    float getWidth();
    float getDepth();

    friend std::ostream& operator<<(std::ostream& os, const BBox& bbox)
    {
        os << bbox._min;
        os << bbox._max;

        return os;
    }

};

}

#endif
