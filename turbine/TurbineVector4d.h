/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef VECTOR4D_H
#define VECTOR4D_H

#include <iostream>
#include "TurbineVector3d.h"

namespace Turbine
{

struct Vector4d
{
    float x, y, z, w;

    Vector4d() : x(0), y(0), z(0), w(1) {}
    Vector4d(float nx, float ny, float nz, float nw) : x(nx), y(ny), z(nz), w(nw){}
    Vector4d(const Vector4d& v) : x(v.x), y(v.y), z(v.z), w(v.w){}
    Vector4d(const Vector3d& v) : x(v.x), y(v.y), z(v.z), w(1.0){}

    // Setters
    void set(const float nx, const float ny, const float nz, const float nw)
    {
        x=nx;
        y=ny;
        z=nz;
        w=nw;
    }

    void set(const Vector4d& v)
    {
        x=v.x;
        y=v.y;
        z=v.z;
        w=v.w;
    }

    // Operators
    Vector4d operator-() const
    {
        return Vector4d(-x, -y, -z, -w);
    }

    Vector4d operator=(const Vector4d& v)
    {
        x=v.x;
        y=v.y;
        z=v.z;
        w=v.w;
        return *this;
    }

    Vector4d operator=(const Vector3d& v)
    {
        x=v.x;
        y=v.y;
        z=v.z;
        w=1.0;
        return *this;
    }

    Vector4d operator+(const Vector4d& v) const
    {
        return Vector4d(x+v.x, y+v.y, z+v.z, w+v.w);
    }

    Vector4d operator+=(const Vector4d& v)
    {
        x+=v.x;
        y+=v.y;
        z+=v.z;
        w+=v.w;
        return *this;
    }

    Vector4d operator-(const Vector4d& v) const
    {
        return Vector4d(x-v.x, y-v.y, z-v.z, w-v.w);
    }

    Vector4d operator-=(const Vector4d& v)
    {
        x-=v.x;
        y-=v.y;
        z-=v.z;
        w-=v.w;
        return *this;
    }

    Vector4d operator*(const Vector4d& v) const
    {
        return Vector4d(x*v.x, y*v.y, z*v.z, w*v.w);
    }

    Vector4d operator*=(const Vector4d& v)
    {
        x*=v.x;
        y*=v.y;
        z*=v.z;
        w*=v.w;
        return *this;
    }

    Vector4d operator*(const float v) const
    {
        return Vector4d(x*v, y*v, z*v, w*v);
    }

    Vector4d operator*=(const float v)
    {
        x*=v;
        y*=v;
        z*=v;
        w*=v;
        return *this;
    }

    Vector4d operator/(const Vector4d& v) const
    {
        return Vector4d(x/v.x, y/v.y, z/v.z, w/v.w);
    }

    Vector4d operator/=(const Vector4d& v)
    {
        x/=v.x;
        y/=v.y;
        z/=v.z;
        w/=v.w;
        return *this;
    }

    Vector4d operator/(const float v) const
    {
        float i=(float)1.0/v;
        return Vector4d(x*i, y*i, z*i, w*i);
    }

    Vector4d operator/=(const float v)
    {
        float i=(float)1.0/v;
        x*=i;
        y*=i;
        z*=i;
        w*=i;
        return *this;
    }

    bool operator<=(const Vector4d& v) const
    {
        return x<=v.x && y<=v.y && z<=v.z && w<=v.w;
    }

    bool operator>=(const Vector4d& v) const
    {
        return x>=v.x && y>=v.y && z>=v.z && w>=v.w;
    }

    bool operator<(const Vector4d& v) const
    {
        return x<v.x && y<v.y && z<v.z && w<v.w;
    }

    bool operator>(const Vector4d& v) const
    {
        return x>v.x && y>v.y && z>v.z && w>v.w;
    }

    bool operator==(const Vector4d& v) const
    {
        return x==v.x && y==v.y && z==v.z && w==v.w;
    }

    bool operator!=(const Vector4d& v) const
    {
        return x!=v.x || y!=v.y || z!=v.z || w!=v.w;
    }

    friend std::ostream& operator<<(std::ostream& os, const Vector4d& v)
    {
        os.precision(3);
        os.setf(std::ios::right | std::ios::showpoint);
        os << "{ ";
        os << std::fixed << v.x;
        os << ", ";
        os << std::fixed << v.y;
        os << ", ";
        os << std::fixed << v.z;
        os << ", ";
        os << std::fixed << v.w;
        os << " }" << std::endl;

        return os;
    }
};

}

#endif
