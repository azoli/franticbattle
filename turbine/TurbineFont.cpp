/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TurbineFont.h"

using namespace std;

//#define DEBUG_FONT 1

namespace Turbine
{

ImageSet* FontInstance::render(const string& str)
{
    ImageSet* set = new ImageSet();
    set->mat.texture = charmap;

    Quad quad;

#ifdef DEBUG_FONT
    cout << "charmap wh: " << charmap->width << " " << charmap->height << endl;
#endif
    int height = 0;
    int penx = 0;
    for(unsigned int i=0; i<str.size(); i++)
    {
        int index = (int) str[i];
        CharacterMetrics& m = metrics[index];

        float left = penx + m.bearX;
        float right = left + m.width;
        float bottom = m.bearY;
        float top = bottom + m.height;
        quad.coords[0] = Vector3d(left, bottom, 0.0f);
        quad.coords[1] = Vector3d(right, bottom, 0.0f);
        quad.coords[2] = Vector3d(right, top, 0.0f);
        quad.coords[3] = Vector3d(left, top, 0.0f);

        float uLeft = (float)(m.pos.x) / charmap->width;
        float uRight = (float)(m.pos.x+m.width) / charmap->width;
        float uBottom = (float)(charmap->height-m.pos.y) / charmap->height;
        float uTop = (float)(charmap->height-m.pos.y+m.height) / charmap->height;
        quad.texCoords[0] = Vector2d(uLeft, uBottom);
        quad.texCoords[1] = Vector2d(uRight, uBottom);
        quad.texCoords[2] = Vector2d(uRight, uTop);
        quad.texCoords[3] = Vector2d(uLeft, uTop);

#ifdef DEBUG_FONT
        cout << str[i] << endl;
        cout << "w, h: " << m.width << ", " << m.height << endl;
        cout << "quad (lrbt):     " << left << ", " << right << ", " << bottom << ", " << top << endl;
        cout << "texcoord (xy):   " << m.pos.x << ", " << charmap->height-m.pos.y-1 << endl;
        cout << "texcoord (lrbt): " << uLeft*charmap->width << ", " << uRight*charmap->width << ", " << uBottom*charmap->height << ", " << uTop*charmap->height << endl << endl;
#endif

        set->regions.push_back(quad);

        penx += m.advances;
        if(m.height > height)
            height = m.height;
    }

    set->width = penx;
    set->height = height;

    return set;
}

}
