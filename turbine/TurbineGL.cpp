/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <stdio.h>
#include <stdlib.h>
#include "TurbineGL.h"
#include "TurbineGLShaderSources.h"

using namespace std;

namespace Turbine
{

// Shader function pointers
PFNGLCREATEPROGRAMPROC glCreateProgram;
PFNGLCREATESHADERPROC glCreateShader;
PFNGLSHADERSOURCEPROC glShaderSource;
PFNGLCOMPILESHADERPROC glCompileShader;
PFNGLATTACHSHADERPROC glAttachShader;
PFNGLBINDATTRIBLOCATIONPROC glBindAttribLocation;
PFNGLLINKPROGRAMPROC glLinkProgram;
PFNGLUSEPROGRAMPROC glUseProgram;
PFNGLGETACTIVEUNIFORMPROC glGetActiveUniform;
PFNGLGETUNIFORMLOCATIONPROC glGetUniformLocation;
PFNGLGETSHADERIVPROC glGetShaderiv;
PFNGLGETSHADERINFOLOGPROC glGetShaderInfoLog;
PFNGLGETPROGRAMIVPROC glGetProgramiv;
PFNGLGETPROGRAMINFOLOGPROC glGetProgramInfoLog;
PFNGLUNIFORM1IPROC glUniform1i;
PFNGLUNIFORM1IVPROC glUniform1iv;
PFNGLUNIFORM1FPROC glUniform1f;
PFNGLUNIFORM1FVPROC glUniform1fv;
PFNGLUNIFORM2FVPROC glUniform2fv;
PFNGLUNIFORM3FVPROC glUniform3fv;
PFNGLUNIFORM4FVPROC glUniform4fv;
PFNGLUNIFORMMATRIX4FVPROC glUniformMatrix4fv;

// VAO function pointers
PFNGLGENVERTEXARRAYSPROC glGenVertexArrays;
PFNGLBINDVERTEXARRAYPROC glBindVertexArray;
PFNGLDELETEVERTEXARRAYSPROC glDeleteVertexArrays;

// VBO function pointers
PFNGLGENBUFFERSPROC glGenBuffers;
PFNGLBINDBUFFERPROC glBindBuffer;
PFNGLBUFFERDATAPROC glBufferData;
PFNGLENABLEVERTEXATTRIBARRAYPROC glEnableVertexAttribArray;
PFNGLVERTEXATTRIBPOINTERPROC glVertexAttribPointer;
PFNGLDELETEBUFFERSPROC glDeleteBuffers;

// Renderbuffer function pointers
PFNGLGENRENDERBUFFERSPROC glGenRenderbuffers;
PFNGLBINDFRAMEBUFFERPROC glBindRenderbuffer;
PFNGLRENDERBUFFERSTORAGEPROC glRenderbufferStorage;
PFNGLDELETERENDERBUFFERSPROC glDeleteRenderbuffers;

// FBO function pointers
PFNGLGENFRAMEBUFFERSPROC glGenFramebuffers;
PFNGLBINDFRAMEBUFFERPROC glBindFramebuffer;
PFNGLFRAMEBUFFERRENDERBUFFERPROC glFramebufferRenderbuffer;
PFNGLFRAMEBUFFERTEXTURE2DPROC glFramebufferTexture2D;
PFNGLCHECKFRAMEBUFFERSTATUSPROC glCheckFramebufferStatus;
PFNGLBLITFRAMEBUFFERPROC glBlitFramebuffer;
PFNGLGENERATEMIPMAPPROC glGenerateMipmap;
PFNGLDRAWBUFFERSPROC glDrawBuffers;
PFNGLDELETEFRAMEBUFFERSPROC glDeleteFramebuffers;

PFNGLACTIVETEXTUREPROC glActiveTexture;

GL::GL() : _lightingEnabled(true), _ambientLight(0.0f, 0.0f, 0.0f, 1.0f), _fbo(0)
{
#ifdef _WIN32
	glDev = new GLDeviceWin;
#elif __unix__
	glDev = new GLDeviceUnix;
#endif

	texLoaded.resize(MAX_TEXTURES, 0);
}

GL::~GL()
{
    // delete shaders
    delete _shaderStatic;
    delete _shaderStaticFlat;
    delete _shaderAnim;
    delete _shaderAnimFlat;
    delete _shaderGUI;
    delete _shaderBlur;
    delete _shaderCombine;

    delete glDev;
}

bool GL::attach(IWindow *window)
{
	win = window;

	if( !glDev->init(win) )
		return false;

    // get funtion pointers
    glCreateProgram = (PFNGLCREATEPROGRAMPROC) glDev->getProcAddress("glCreateProgram");
    glCreateShader = (PFNGLCREATESHADERPROC) glDev->getProcAddress("glCreateShader");
    glShaderSource = (PFNGLSHADERSOURCEPROC) glDev->getProcAddress("glShaderSource");
    glCompileShader = (PFNGLCOMPILESHADERPROC) glDev->getProcAddress("glCompileShader");
    glAttachShader = (PFNGLATTACHSHADERPROC) glDev->getProcAddress("glAttachShader");
    glBindAttribLocation = (PFNGLBINDATTRIBLOCATIONPROC) glDev->getProcAddress("glBindAttribLocation");
    glLinkProgram = (PFNGLLINKPROGRAMPROC) glDev->getProcAddress("glLinkProgram");
    glUseProgram = (PFNGLUSEPROGRAMPROC) glDev->getProcAddress("glUseProgram");
    glGetActiveUniform = (PFNGLGETACTIVEUNIFORMPROC) glDev->getProcAddress("glGetActiveUniform");
    glGetUniformLocation = (PFNGLGETUNIFORMLOCATIONPROC) glDev->getProcAddress("glGetUniformLocation");
    glGetShaderiv = (PFNGLGETSHADERIVPROC) glDev->getProcAddress("glGetShaderiv");
    glGetShaderInfoLog = (PFNGLGETSHADERINFOLOGPROC) glDev->getProcAddress("glGetShaderInfoLog");
    glGetProgramiv = (PFNGLGETPROGRAMIVPROC) glDev->getProcAddress("glGetProgramiv");
    glGetProgramInfoLog = (PFNGLGETPROGRAMINFOLOGPROC) glDev->getProcAddress("glGetProgramInfoLog");
    glUniform1i = (PFNGLUNIFORM1IPROC) glDev->getProcAddress("glUniform1i");
    glUniform1iv = (PFNGLUNIFORM1IVPROC) glDev->getProcAddress("glUniform1iv");
    glUniform1f = (PFNGLUNIFORM1FPROC) glDev->getProcAddress("glUniform1f");
    glUniform1fv = (PFNGLUNIFORM1FVPROC) glDev->getProcAddress("glUniform1fv");
    glUniform2fv = (PFNGLUNIFORM2FVPROC) glDev->getProcAddress("glUniform2fv");
    glUniform3fv = (PFNGLUNIFORM3FVPROC) glDev->getProcAddress("glUniform3fv");
    glUniform4fv = (PFNGLUNIFORM4FVPROC) glDev->getProcAddress("glUniform4fv");
    glUniformMatrix4fv = (PFNGLUNIFORMMATRIX4FVPROC) glDev->getProcAddress("glUniformMatrix4fv");

    glGenVertexArrays = (PFNGLGENVERTEXARRAYSPROC) glDev->getProcAddress("glGenVertexArrays");
    glBindVertexArray = (PFNGLBINDVERTEXARRAYPROC) glDev->getProcAddress("glBindVertexArray");
    glDeleteVertexArrays = (PFNGLDELETEVERTEXARRAYSPROC) glDev->getProcAddress("glDeleteVertexArrays");

    glGenBuffers = (PFNGLGENBUFFERSPROC) glDev->getProcAddress("glGenBuffers");
    glBindBuffer = (PFNGLBINDBUFFERPROC) glDev->getProcAddress("glBindBuffer");
    glBufferData = (PFNGLBUFFERDATAPROC) glDev->getProcAddress("glBufferData");
    glEnableVertexAttribArray = (PFNGLENABLEVERTEXATTRIBARRAYPROC) glDev->getProcAddress("glEnableVertexAttribArray");
    glVertexAttribPointer = (PFNGLVERTEXATTRIBPOINTERPROC) glDev->getProcAddress("glVertexAttribPointer");
    glDeleteBuffers = (PFNGLDELETEBUFFERSPROC) glDev->getProcAddress("glDeleteBuffers");

    glGenRenderbuffers = (PFNGLGENRENDERBUFFERSPROC) glDev->getProcAddress("glGenRenderbuffers");
    glBindRenderbuffer = (PFNGLBINDFRAMEBUFFERPROC) glDev->getProcAddress("glBindRenderbuffer");
    glRenderbufferStorage = (PFNGLRENDERBUFFERSTORAGEPROC) glDev->getProcAddress("glRenderbufferStorage");
    glDeleteRenderbuffers = (PFNGLDELETERENDERBUFFERSPROC) glDev->getProcAddress("glDeleteRenderbuffers");

    glGenFramebuffers = (PFNGLGENFRAMEBUFFERSPROC) glDev->getProcAddress("glGenFramebuffers");
    glBindFramebuffer = (PFNGLBINDFRAMEBUFFERPROC) glDev->getProcAddress("glBindFramebuffer");
    glFramebufferRenderbuffer = (PFNGLFRAMEBUFFERRENDERBUFFERPROC) glDev->getProcAddress("glFramebufferRenderbuffer");
    glFramebufferTexture2D = (PFNGLFRAMEBUFFERTEXTURE2DPROC) glDev->getProcAddress("glFramebufferTexture2D");
    glCheckFramebufferStatus = (PFNGLCHECKFRAMEBUFFERSTATUSPROC) glDev->getProcAddress("glCheckFramebufferStatus");
    glBlitFramebuffer = (PFNGLBLITFRAMEBUFFERPROC) glDev->getProcAddress("glBlitFramebuffer");
    glGenerateMipmap = (PFNGLGENERATEMIPMAPPROC) glDev->getProcAddress("glGenerateMipmap");
    glDrawBuffers = (PFNGLDRAWBUFFERSPROC) glDev->getProcAddress("glDrawBuffers");
    glDeleteFramebuffers = (PFNGLDELETEFRAMEBUFFERSPROC) glDev->getProcAddress("glDeleteFramebuffers");

    glActiveTexture = (PFNGLACTIVETEXTUREPROC) glDev->getProcAddress("glActiveTexture");
    
    return true;
}

bool GL::detach()
{
	if( !glDev->release() )
		return false;

	return true;
}

bool GL::init()
{
	resizeScene(win->getWidth(),win->getHeight());

	glEnable( GL_CULL_FACE );
	glEnable( GL_DEPTH_TEST );

    // enable alpha blending
    glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

    //load 1x1 white pixel for nontexture
    TextureLoader* tloader = TextureLoader::getInstance();
    _notex = tloader->getFreeIndex();
    glBindTexture(GL_TEXTURE_2D, _notex);
    GLchar white[3] = {(GLchar)254, (GLchar)254, (GLchar)254};
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, 1, 1, 0, GL_RGB, GL_UNSIGNED_BYTE, white);

    // load shader programs
    string vsSource, fsSource;

    vsSource = modelStaticVS;
    fsSource = modelFS + directionalLight + pointLight + spotLight;
    _shaderStatic = new Shader("Static", vsSource, fsSource);

    vsSource = modelAnimatedVS;
    _shaderAnim = new Shader("Animated", vsSource, fsSource);

    vsSource = modelStaticFlatVS;
    fsSource = modelFlatFS;
    _shaderStaticFlat = new Shader("StaticFlat", vsSource, fsSource);

    vsSource = modelAnimatedFlatVS;
    _shaderAnimFlat = new Shader("AnimatedFlat", vsSource, fsSource);

    vsSource = guiVS;
    fsSource = guiFS;
    _shaderGUI = new Shader("GUI", vsSource, fsSource);

    vsSource = passVS;
    fsSource = blurFS;
    _shaderBlur = new Shader("Blur", vsSource, fsSource);

    vsSource = passVS;
    fsSource = combineFS;
    _shaderCombine = new Shader("Combine", vsSource, fsSource);

    _shaderStatic->build();
    _shaderAnim->build();
    _shaderStaticFlat->build();
    _shaderAnimFlat->build();
    _shaderGUI->build();
    _shaderBlur->build();
    _shaderCombine->build();

    // workaround for a driver bug that consider active only finalMatrix[0]
    for(int i=0; i<80; i++)
    {
        std::stringstream ss;
        ss << "finalMatrix[" << i << "]";
        _shaderAnim->addUniformName(ss.str());
        _shaderAnimFlat->addUniformName(ss.str());
    }
    for(int i=0; i<KERNELSIZE; i++)
    {
        std::stringstream ss;
        ss << "offsets[" << i << "]";
        _shaderBlur->addUniformName(ss.str());
        ss.str("");
        ss << "weights[" << i << "]";
        _shaderBlur->addUniformName(ss.str());
    }

    _createFBO();

    // attachment draw order
    _fboBuffers[0] = GL_COLOR_ATTACHMENT0;
    _fboBuffers[1] = GL_COLOR_ATTACHMENT1;
    _fboBuffers[2] = GL_COLOR_ATTACHMENT2;

    // blur weights initialization
    weights[0] = 1.0/16.0;
    weights[1] = 4.0/16.0;
    weights[2] = 6.0/16.0;
    weights[3] = 4.0/16.0;
    weights[4] = 1.0/16.0;

    // allocate a fullscreen quad defined in NDC
    const GLfloat coords[] =
    {
        -1.0, -1.0,
         1.0, -1.0,
         1.0,  1.0,
         1.0,  1.0,
        -1.0,  1.0,
        -1.0, -1.0
    };
    glGenBuffers(1, &_fullscreenQuadVBO);
    glBindBuffer(GL_ARRAY_BUFFER, _fullscreenQuadVBO);
    glBufferData(GL_ARRAY_BUFFER, sizeof(coords), coords, GL_STATIC_DRAW);

    return true;
}

void GL::resizeScene(int width, int height)
{
	if (height==0)											// prevent a divide by zero
		height=1;
	glViewport(0,0,width,height);
	printErrors("GL::resizeScene glViewport");

    if(_fbo != 0)
    {
        _destroyFBO();
        _createFBO();
    }
}

void GL::beginScene()
{
    // use FBO to draw
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, _fbo);
    printErrors("GL::beginScene glBindFramebuffer");
    glDrawBuffers(3, _fboBuffers);
    printErrors("GL::beginScene glDrawBuffers");
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
    printErrors("GL::beginScene glClear");

   _lightNum = 0;
}

void GL::endScene()
{
    // post-processing

    glBindVertexArray(0);
    // Glow effect
    // 1- downsample the glow objects
    glBindTexture(GL_TEXTURE_2D, _colorBuffers[2]);
    glGenerateMipmap(GL_TEXTURE_2D);

    // 2- apply blur filter
    _shaderBlur->use();
    // first pass
    for(unsigned int i=0; i<NBUFFS; i++)
        _blur(_colorBuffers[2], i, _glowFBOs[0][i], true);
    for(unsigned int i=0; i<NBUFFS; i++)
        _blur(_glowColorBuffers[0], i, _glowFBOs[1][i], false);

    // 3- blend results
    _shaderCombine->use();
    glUniform1i(_shaderCombine->getUniform("scene"), 0);
    printErrors("GL::endScene uniform scene");
    glUniform1i(_shaderCombine->getUniform("glow"), 1);
    printErrors("GL::endScene uniform glow");
    glActiveTexture(GL_TEXTURE0 + 0);
    glBindTexture(GL_TEXTURE_2D, _colorBuffers[0]);
    printErrors("GL::endScene glBindFramebuffer");
    glActiveTexture(GL_TEXTURE0 + 1);
    glBindTexture(GL_TEXTURE_2D, _glowColorBuffers[1]);
    glActiveTexture(GL_TEXTURE0);
    // bind the destination fbo
    glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
    printErrors("GL::endScene glBindFramebuffer");
    glDrawBuffers(1, _fboBuffers);
    // draw the fullscreen quad
    glBindBuffer(GL_ARRAY_BUFFER, _fullscreenQuadVBO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
    printErrors("GL::endScene glVertexAttribPointer");
    glDrawArrays(GL_TRIANGLES, 0, 6);

    // setup default window-manager fbo for drawing into backbuffer
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, 0);
    printErrors("GL::endScene glBindFramebuffer");
    glDrawBuffer(GL_BACK);
    printErrors("GL::endScene glDrawBuffer(GL_BACK)");

    // setup my fbo for reading from attachment0
    glBindFramebuffer(GL_READ_FRAMEBUFFER, _fbo);
    printErrors("GL::endScene glBindFramebuffer");
    glReadBuffer(GL_COLOR_ATTACHMENT0);
    printErrors("GL::endScene glReadBuffer(GL_COLOR_ATTACHMENT0)");

    // raw copy gpu-wise
    glBlitFramebuffer(0, 0, win->getWidth(), win->getHeight(), 0, 0, win->getWidth(), win->getHeight(), GL_COLOR_BUFFER_BIT, GL_LINEAR);
    printErrors("GL::endScene glBlitFramebuffer");

    glDev->swapBuffers();
}

void GL::setTransform(TRANSFORMATION_STATE state, const Matrix4& mat)
{
    _matrix[state] = mat;
}

const Matrix4& GL::getTransform(TRANSFORMATION_STATE state)
{
    return _matrix[state];
}

void GL::enableLighting( bool enable )
{
   _lightingEnabled = enable;
}

bool GL::isEnabledLighting()
{
	return _lightingEnabled;
}

void GL::drawLight(Light* light)
{
    Vector3d att;
    Vector4d pos, dir, posEye, dirEye;
    Colorf amb, diff, spec;
    float exp, cutoff;

    if(!_lightingEnabled)
        return;

    LIGHT_TYPE type = light->getType();
    pos = light->getPosition();
    if(type == L_DIRECTIONAL)
        pos.w = 0.0;
    amb = light->getAmbientColor();
    diff = light->getDiffuseColor();
    spec = light->getSpecularColor();
    att = light->getAttenuation();
    cutoff = degToRad(light->getSpotCutoff());

    // get eye-coordinate light position/direction
    posEye = pos * _matrix[TS_VIEW];

    // get eye-coordinate light direction and exponent
    if(type == L_SPOT)
    {
        exp = light->getSpotExponent();
        dir = light->getSpotDirection();
        dir.w = 0;

        dirEye = dir * _matrix[TS_VIEW];
    }

    // update static shader uniforms
    int i = _lightNum;

    _shaderStatic->use();
    std::stringstream ss;
    ss << "lights[" << i << "].";
    std::string str = ss.str();
    glUniform1i(_shaderStatic->getUniform("numlights"), _lightNum+1 );
    glUniform4fv(_shaderStatic->getUniform(str+"position"), 1, (GLfloat*)&posEye);
    glUniform4fv(_shaderStatic->getUniform(str+"ambient"), 1, (GLfloat*)&amb);
    glUniform4fv(_shaderStatic->getUniform(str+"diffuse"), 1, (GLfloat*)&diff);
    glUniform4fv(_shaderStatic->getUniform(str+"specular"), 1, (GLfloat*)&spec);
    if(type == L_POINT || type == L_SPOT)
    {
        glUniform3fv(_shaderStatic->getUniform(str+"attenuation"), 1, (GLfloat*)&att);
        glUniform1f(_shaderStatic->getUniform(str+"spotcutoff"), cutoff);
    }
    if(type == L_SPOT)
    {
        glUniform3fv(_shaderStatic->getUniform(str+"spotdirection"), 1, (GLfloat*)&dirEye);
        glUniform1f(_shaderStatic->getUniform(str+"spotexponent"), exp);
    }

    // update animation shader uniforms
    _shaderAnim->use();
    glUniform1i(_shaderAnim->getUniform("numlights"), _lightNum+1 );
    glUniform4fv(_shaderAnim->getUniform(str+"position"), 1, (GLfloat*)&posEye);
    glUniform4fv(_shaderAnim->getUniform(str+"ambient"), 1, (GLfloat*)&amb);
    glUniform4fv(_shaderAnim->getUniform(str+"diffuse"), 1, (GLfloat*)&diff);
    glUniform4fv(_shaderAnim->getUniform(str+"specular"), 1, (GLfloat*)&spec);
    if(type == L_POINT || type == L_SPOT)
    {
        glUniform3fv(_shaderAnim->getUniform(str+"attenuation"), 1, (GLfloat*)&att);
        glUniform1f(_shaderAnim->getUniform(str+"spotcutoff"), cutoff);
    }
    if(type == L_SPOT)
    {
        glUniform3fv(_shaderAnim->getUniform(str+"spotdirection"), 1, (GLfloat*)&dirEye);
        glUniform1f(_shaderAnim->getUniform(str+"spotexponent"), exp);
    }

    _lightNum++;
    printErrors("drawLight");
}

void GL::loadTexture(Image *image)
{
    glGenTextures(1, &vTex[image->index]);
    glBindTexture(GL_TEXTURE_2D, vTex[image->index]);
    switch(image->type)
    {
        case RGB_24BIT_U:
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->data);
            break;
        }
        case RGBA_24BIT_U:
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->width, image->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->data);
            break;
        }
        case RGBA_32BIT_U:
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->width, image->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->data);
            break;
        }
        default:
        {
            cout << "wrong texture type" << endl;
        }
    }

    texLoaded[image->index] = image->index;

    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);

    printErrors("GL::loadTexture");
}

bool GL::createHardwareBuffers(Model* model)
{
    GLHardwareBuffers* buffs = new GLHardwareBuffers();
    model->buffs = buffs;

    // load not-yet-loaded textures
    vector<Material>::iterator material;
    for(material = model->materials.begin(); material != model->materials.end(); material++)
    {
        Image* tex = (*material).texture;
        if( tex != NULL && texLoaded[tex->index] == 0 )
            loadTexture(tex);
    }

    //Create VAO
    glGenVertexArrays(1, &buffs->VAOID);
    glBindVertexArray(buffs->VAOID);
    printErrors("GL::glBindVertexArray vaoid");

    //Create interleaved VBO
    int vertCount = model->vertices.size();
    glGenBuffers(1, &buffs->VBOID[0]);
    glBindBuffer(GL_ARRAY_BUFFER, buffs->VBOID[0]);
    glBufferData(GL_ARRAY_BUFFER, vertCount*sizeof(Vertex), &model->vertices[0], GL_STATIC_DRAW);
    printErrors("GL::glBufferData for VBO");

    for(int i=0; i < 5; i++)
        glEnableVertexAttribArray(i);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)0);
    printErrors("GL::glVertexAttribPointer vertex");

    glVertexAttribPointer(1, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)(sizeof(float)*3));
    printErrors("GL::glVertexAttribPointer normal");

    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)(sizeof(float)*6));
    printErrors("GL::glVertexAttribPointer texcoords");

    glVertexAttribPointer(3, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)(sizeof(float)*8));
    printErrors("GL::glVertexAttribPointer bones indices");

    glVertexAttribPointer(4, 4, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)(sizeof(float)*12));
    printErrors("GL::glVertexAttribPointer bones weights");

    //Create the IBO
    int indicesCount = model->faces.size()*3;
    glGenBuffers(1, &buffs->IBOID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffs->IBOID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesCount*sizeof(int), &model->faces[0], GL_STATIC_READ);
    printErrors("GL::glBufferData for IBO");

    return true;
}

void GL::destroyHardwareBuffers(Model* model)
{
    // delete buffers
    GLHardwareBuffers* buffs = (GLHardwareBuffers*) model->buffs;
    glDeleteVertexArrays(1, &buffs->VAOID);
    glDeleteBuffers(1, buffs->VBOID);
    glDeleteBuffers(1, &buffs->IBOID);

    // delete Textures
    list<SmoothingGroup>::iterator group;
    for(group = model->smoothGroups.begin(); group != model->smoothGroups.end(); group++)
    {
        Material mtr = model->materials[(*group).mtlIndex];
        if(mtr.texture != NULL)
        {
            glDeleteTextures(1, &vTex[mtr.texture->index]);
            texLoaded[mtr.texture->index] = 0;
        }
    }
}

void GL::drawEntity(Entity* entity)
{
    if(entity == NULL)
    {
        cout << "error: NULL entity pointer" << endl;
        return;
    }

    Model* model = entity->getModel();
    GLHardwareBuffers* buffs = (GLHardwareBuffers*) model->buffs;

    Shader* shader;
    if(_lightingEnabled)
    {
        if(model->isAnimated)
            shader = _shaderAnim;
        else
            shader = _shaderStatic;
    }
    else
    {
        if(model->isAnimated)
            shader = _shaderAnimFlat;
        else
            shader = _shaderStaticFlat;
    }
    shader->use();

    glBindVertexArray(buffs->VAOID);
    printErrors("GL::drawEntity glBindVertexArray");

    // update shader matrices
    Matrix4 modelViewProjection = _matrix[TS_MODEL] * _matrix[TS_VIEW] * _matrix[TS_PROJECTION];
    glUniformMatrix4fv(shader->getUniform("mvpMatrix"), 1, GL_FALSE, (GLfloat*)&modelViewProjection);
    if(_lightingEnabled)
    {
        Matrix4 modelView = _matrix[TS_MODEL] * _matrix[TS_VIEW];
        glUniformMatrix4fv(shader->getUniform("mvMatrix"), 1, GL_FALSE, (GLfloat*)&modelView);
    }
    printErrors("GL::drawEntity update shader matrices");

    GLfloat colorID[4];
    // use the entity ID for color-picking
    unsigned int color = entity->getID();
    colorID[3] = 1.0;
    colorID[2] = ((color << 8) >> 24) / 255.0f;
    colorID[1] = ((color << 16) >> 24) / 255.0f;
    colorID[0] = ((color << 24) >> 24) / 255.0f;
    glUniform4fv(shader->getUniform("colorid"), 1, colorID);

    // update bones matrices
    if(model->isAnimated)
    {
        const vector<Matrix4>& frames = entity->getBonesMatrices();
        for(unsigned int i=0; i<frames.size(); i++)
        {
            stringstream ss;
            ss << "finalMatrix[" << i << "]";
            glUniformMatrix4fv(shader->getUniform(ss.str()), 1, GL_FALSE, (GLfloat*)&frames[i]);
            printErrors("GL::drawEntity updateBones");
        }
    }

    list<SmoothingGroup>::iterator group;
    for(group = model->smoothGroups.begin(); group != model->smoothGroups.end(); group++)
    {
        Material mtr = model->materials[(*group).mtlIndex];
        bool alphaBlending = false;
        Image* img = mtr.texture;
        if( !alphaBlending && img && (img->type == RGBA_24BIT_U || img->type == RGBA_32BIT_U) )
        {
            glEnable(GL_BLEND);
            alphaBlending = true;
        }

        GLint glow = mtr.useGlow;

        Colorf ambient(mtr.kd.r*mtr.ambient, mtr.kd.g*mtr.ambient, mtr.kd.b*mtr.ambient, mtr.ambient);
        glUniform4fv(shader->getUniform("mtl.ambient"), 1, (GLfloat*)&ambient);
        glUniform4fv(shader->getUniform("mtl.diffuse"), 1, (GLfloat*)&mtr.kd);
        glUniform4fv(shader->getUniform("mtl.specular"), 1, (GLfloat*)&mtr.ks);
        if(_lightingEnabled)
            glUniform1fv(shader->getUniform("mtl.shininess"), 1, (GLfloat*)&mtr.ns);
        glUniform1iv(shader->getUniform("mtl.glow"), 1, &glow);

        if( mtr.texture != NULL )
            glBindTexture(GL_TEXTURE_2D, vTex[ mtr.texture->index ]);
        else
            glBindTexture(GL_TEXTURE_2D, _notex);

        glDrawElements(GL_TRIANGLES, ((*group).end-(*group).start)*3, GL_UNSIGNED_INT, ((unsigned int*)NULL+((*group).start*3)));

        if(alphaBlending)
            glDisable(GL_BLEND);
    }
    glFlush();
    printErrors("GL::drawEntity");
}

bool GL::createHardwareBuffers(GUIElement* element)
{
    // for now it uses a Model, after Mesh and SubMesh it will be use Mesh
    Model* model = new Model();
    ImageSet* set = element->getImageSet();
    model->appendStaticMeshData(set->regions, set->mat);

    // load not-yet-loaded textures
    Image* tex = element->getImageSet()->mat.texture;
    if( tex != NULL && texLoaded[tex->index] == 0 )
        loadTexture(tex);

    // setup mesh hardware buffers
    GLHardwareBuffers* buffs = new GLHardwareBuffers();
    model->isAnimated = false;
    model->buffs = buffs;
    element->buffs = buffs;
    buffs->modelPtr = model;

    glGenVertexArrays(1, &buffs->VAOID);
    glBindVertexArray(buffs->VAOID);
    printErrors("GL::glBindVertexArray vaoid");

    //Create interleaved VBO
    int vertCount = model->vertices.size();
    glGenBuffers(1, &buffs->VBOID[0]);
    glBindBuffer(GL_ARRAY_BUFFER, buffs->VBOID[0]);
    glBufferData(GL_ARRAY_BUFFER, vertCount*sizeof(Vertex), &model->vertices[0], GL_DYNAMIC_DRAW);
    printErrors("GL::glBufferData for VBO");

    glEnableVertexAttribArray(0);
    glEnableVertexAttribArray(2);

    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)0);
    printErrors("GL::glVertexAttribPointer vertex");

    glVertexAttribPointer(2, 2, GL_FLOAT, GL_FALSE, sizeof(Vertex), (const GLvoid*)(sizeof(float)*6));
    printErrors("GL::glVertexAttribPointer texcoords");

    //Create the IBO
    int indicesCount = model->faces.size()*3;
    glGenBuffers(1, &buffs->IBOID);
    glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, buffs->IBOID);
    glBufferData(GL_ELEMENT_ARRAY_BUFFER, indicesCount*sizeof(int), &model->faces[0], GL_STREAM_DRAW);
    printErrors("GL::glBufferData for IBO");

    return true;
}

bool GL::updateHardwareBuffers(GUIElement* element)
{
    GLHardwareBuffers* buffs = (GLHardwareBuffers*) element->buffs;
    Model* model = buffs->modelPtr;
    ImageSet* set = element->getImageSet();
    model->destroyMeshData();
    model->appendStaticMeshData(set->regions, set->mat);

    glBindVertexArray(buffs->VAOID);
    printErrors("GL::glBindVertexArray vaoid");

    // update vbo
    int vertCount = model->vertices.size();
    glBindBuffer(GL_ARRAY_BUFFER, buffs->VBOID[0]);
    glBufferData(GL_ARRAY_BUFFER, 0, NULL, GL_DYNAMIC_DRAW);
    glBufferData(GL_ARRAY_BUFFER, vertCount*sizeof(Vertex), &model->vertices[0], GL_STREAM_DRAW);
    printErrors("GL::glBufferData for VBO");

    return true;
}

void GL::destroyHardwareBuffers(GUIElement* element)
{
    // delete buffers
    GLHardwareBuffers* buffs = (GLHardwareBuffers*) element->buffs;
    glDeleteVertexArrays(1, &buffs->VAOID);
    glDeleteBuffers(1, buffs->VBOID);
    glDeleteBuffers(1, &buffs->IBOID);

    // delete Textures
    Material mtr = element->getImageSet()->mat;
    if(mtr.texture != NULL)
    {
        glDeleteTextures(1, &vTex[mtr.texture->index]);
        texLoaded[mtr.texture->index] = 0;
    }
}

void GL::drawGUIElement(GUIElement* element)
{
    if(element == NULL)
    {
        cout << "error: NULL model pointer" << endl;
        return;
    }
    glDrawBuffers(1, _fboBuffers);

    GLHardwareBuffers* buffs = (GLHardwareBuffers*) element->buffs;
    Model* model = buffs->modelPtr;

    _shaderGUI->use();

    bool alphaBlending = false;
    Image* img = element->getImageSet()->mat.texture;
    if( !alphaBlending && (img->type == RGBA_24BIT_U || img->type == RGBA_32BIT_U) )
    {
        glEnable(GL_BLEND);
        alphaBlending = true;
    }

    glBindVertexArray(buffs->VAOID);
    printErrors("GL::glBindVertexArray vaoid");

    Matrix4 modelProjection = _matrix[TS_MODEL] * _matrix[TS_PROJECTION];
    glUniformMatrix4fv(_shaderGUI->getUniform("mvpMatrix"), 1, GL_FALSE, (GLfloat*)&modelProjection);

    list<SmoothingGroup>::iterator group;
    for(group = model->smoothGroups.begin(); group != model->smoothGroups.end(); group++)
    {

        glBindTexture(GL_TEXTURE_2D, vTex[ img->index ]);
        glDrawElements(GL_TRIANGLES, ((*group).end-(*group).start)*3, GL_UNSIGNED_INT, ((unsigned int*)NULL+((*group).start*3)));
    }

    if(alphaBlending)
        glDisable(GL_BLEND);

    glFlush();
    printErrors("GL::drawGUIElement");
}

void GL::printCapabilities()
{
    GLint val;
    glGetIntegerv(GL_MAX_LIGHTS, &val);
    cout << "Max lights: " << val << endl;

    glGetIntegerv(GL_MAX_DRAW_BUFFERS, &val);
    cout << "Max draw buffers: " << val << endl;
}

unsigned int GL::pickupColor(int x, int y)
{
    glBindFramebuffer(GL_READ_FRAMEBUFFER, _fbo);
    printErrors("GL::endScene glBindFramebuffer");
    glReadBuffer(GL_COLOR_ATTACHMENT1);
    printErrors("GL::pickupColor glReadBuffer(GL_COLOR_ATTACHMENT1)");

    unsigned int color;
    glReadPixels(x, win->getHeight()-y, 1, 1, GL_RGB, GL_UNSIGNED_BYTE, &color);
    printErrors("GL::pickupColor");

    return color;
}

void GL::_createFBO()
{
    // create the FBO for the scene
    glGenFramebuffers(1, &_fbo);
    glBindFramebuffer(GL_FRAMEBUFFER, _fbo);
    printErrors("GL::init FBO");
    // create depth renderbuffer
    glGenRenderbuffers(1, &_depthBuffer);
    glBindRenderbuffer(GL_RENDERBUFFER, _depthBuffer);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT32, win->getWidth(), win->getHeight());
    printErrors("GL::init create depth renderbuffer");
    // create texture for the scene
    glGenTextures(1, &_colorBuffers[0]);
    glBindTexture(GL_TEXTURE_2D, _colorBuffers[0]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, win->getWidth(), win->getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    printErrors("GL::init cannot create scene texbuffer");
    // create color renderbuffer for color picking
    glGenRenderbuffers(1, &_colorBuffers[1]);
    glBindRenderbuffer(GL_RENDERBUFFER, _colorBuffers[1]);
    glRenderbufferStorage(GL_RENDERBUFFER, GL_RGBA8, win->getWidth(), win->getHeight());
    printErrors("GL::init create 2 color renderbuffers");
    // create texture for glow objects
    glGenTextures(1, &_colorBuffers[2]);
    glBindTexture(GL_TEXTURE_2D, _colorBuffers[2]);
    glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, win->getWidth(), win->getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
    glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
    printErrors("GL::init cannot create scene texbuffer");
    // attach depthbuffer, renderbuffers and glow texture to the FBO
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, _depthBuffer);
    printErrors("GL::init _fbo depth buffer");
    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _colorBuffers[0], 0);
    printErrors("GL::init _fbo attachment0");
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_RENDERBUFFER, _colorBuffers[1]);
    printErrors("GL::init _fbo attachment1");
    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, _colorBuffers[2], 0);
    printErrors("GL::init _fbo attachment2");
    // check fbo complentess
    _checkFBOCompleteness();
    // generate mipmaps for texture _colorBuffers[2]
    glGenerateMipmap(GL_TEXTURE_2D);

    // create glow effect FBOs
    for(unsigned int pass=0; pass<PASSES; pass++)
    {
        glGenFramebuffers(NBUFFS, &_glowFBOs[pass][0]);
        // first fbo
        glBindFramebuffer(GL_FRAMEBUFFER, _glowFBOs[pass][0]);
        printErrors("GL::init horizzontal pass FBO");
        // create texture
        glGenTextures(1, &_glowColorBuffers[pass]);
        glBindTexture(GL_TEXTURE_2D, _glowColorBuffers[pass]);
        glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA8, win->getWidth(), win->getHeight(), 0, GL_RGBA, GL_UNSIGNED_BYTE, 0);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
        glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
        printErrors("GL::init cannot create glow horizzontal pass texbuffer");
        // generate mipmaps for texture _glowColorBuffers[pass]
        glGenerateMipmap(GL_TEXTURE_2D);

        for(unsigned int i=0; i<NBUFFS; i++)
        {
            // i-th fbo
            glBindFramebuffer(GL_FRAMEBUFFER, _glowFBOs[pass][i]);
            printErrors("GL::init horizzontal pass FBO");
            // attach i-th downsample
            glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, _glowColorBuffers[pass], i);
            printErrors("GL::init attach texture to glow horizzontal pass FBO");
            // check FBO completeness
            _checkFBOCompleteness();
        }
    }

    // set offsets
    hoffsets[0] = Vector2d(-2.0, 0.0);
    hoffsets[1] = Vector2d(-1.0, 0.0);
    hoffsets[2] = Vector2d(0.0, 0.0);
    hoffsets[3] = Vector2d(1.0, 0.0);
    hoffsets[4] = Vector2d(2.0, 0.0);

    voffsets[0] = Vector2d(0.0, -2.0);
    voffsets[1] = Vector2d(0.0, -1.0);
    voffsets[2] = Vector2d(0.0, 0.0);
    voffsets[3] = Vector2d(0.0, 1.0);
    voffsets[4] = Vector2d(0.0, 2.0);

    for(unsigned int i=0; i<KERNELSIZE; i++)
    {
        hoffsets[i].x /= win->getWidth();
        voffsets[i].y /= win->getHeight();
    }
}

void GL::_destroyFBO()
{
    // bind fbo
    glBindFramebuffer(GL_FRAMEBUFFER, _fbo);

    // detach renderbuffers
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, 0);
    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
    glFramebufferRenderbuffer(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT1, GL_RENDERBUFFER, 0);
    glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT2, GL_TEXTURE_2D, 0, 0);
    printErrors("GL::_destroyFBO detach _fbo buffers");

    // delete renderbuffers
    glDeleteRenderbuffers(1, &_depthBuffer);
    glDeleteTextures(1, &_colorBuffers[0]);
    glDeleteRenderbuffers(1, &_colorBuffers[1]);
    glDeleteTextures(1, &_colorBuffers[2]);
    printErrors("GL::_destroyFBO delete _fbo texture or renderbuffer");

    // delete fbo
    glDeleteFramebuffers(1, &_fbo);
    printErrors("GL::_destroyFBO delete _fbo");

    // detach glow textures and delete relative framebuffers
    for(unsigned int pass=0; pass<PASSES; pass++)
    {
        for(unsigned int i=0; i<NBUFFS; i++)
        {
            glBindFramebuffer(GL_FRAMEBUFFER, _glowFBOs[pass][i]);
            glFramebufferTexture2D(GL_DRAW_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, 0, 0);
            printErrors("GL::_destroyFBO detach textures");
            glDeleteFramebuffers(1, &_glowFBOs[pass][i]);
            printErrors("GL::_destroyFBO delete glow framebuffers");
        }
    }
    // delete textures
    glDeleteTextures(PASSES, _glowColorBuffers);
    printErrors("GL::_destroyFBO delete glow textures");
}

void GL::disableDepthBuffer()
{
    glDisable(GL_DEPTH_TEST);
}

void GL::enableDepthBuffer()
{
    glEnable(GL_DEPTH_TEST);
}

bool GL::_checkFBOCompleteness()
{
    //Does the GPU support current FBO configuration?
    GLenum status = glCheckFramebufferStatus(GL_FRAMEBUFFER);
    switch(status)
    {
        case GL_FRAMEBUFFER_COMPLETE_EXT:
        {
            return true;
        }
        case GL_FRAMEBUFFER_UNDEFINED:
        {
            std::cout << "target is the default framebuffer, but the default framebuffer does not exist." << std::endl;
            return false;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_ATTACHMENT:
        {
            std::cout << "any of the framebuffer attachment points are framebuffer incomplete." << std::endl;
            return false;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_MISSING_ATTACHMENT:
        {
            std::cout << "the framebuffer does not have at least one image attached to it." << std::endl;
            return false;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_DRAW_BUFFER:
        {
            std::cout << "the value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for any color attachment point(s) named by GL_DRAWBUFFERi." << std::endl;
            return false;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_READ_BUFFER:
        {
            std::cout << "GL_READ_BUFFER is not GL_NONE and the value of GL_FRAMEBUFFER_ATTACHMENT_OBJECT_TYPE is GL_NONE for the color attachment point named by GL_READ_BUFFER." << std::endl;
            return false;
        }
        case GL_FRAMEBUFFER_UNSUPPORTED:
        {
            std::cout << "the combination of internal formats of the attached images violates an implementation-dependent set of restrictions." << std::endl;
            return false;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_MULTISAMPLE:
        {
            std::cout << "the value of GL_RENDERBUFFER_SAMPLES is not the same for all attached renderbuffers; if the value of GL_TEXTURE_SAMPLES is the not same for all attached textures; or, if the attached images are a mix of renderbuffers and textures, the value of GL_RENDERBUFFER_SAMPLES does not match the value of GL_TEXTURE_SAMPLES. Is also returned if the value of GL_TEXTURE_FIXED_SAMPLE_LOCATIONS is not the same for all attached textures; or, if the attached images are a mix of renderbuffers and textures, the value of GL_TEXTURE_FIXED_SAMPLE_LOCATIONS is not GL_TRUE for all attached textures." << std::endl;
            return false;
        }
        case GL_FRAMEBUFFER_INCOMPLETE_LAYER_TARGETS:
        {
            std::cout << "any framebuffer attachment is layered, and any populated attachment is not layered, or if all populated color attachments are not from textures of the same target." << std::endl;
            return false;
        }
        default:
        {
            std::cout << "fbo error unknown" << status << std::endl;
        }
    }
    return false;
}

void GL::_blur(GLuint srcTex, float srcLevel, GLuint dstFBO, bool horizontal)
{
    // bind source texture for lookup
    glBindTexture(GL_TEXTURE_2D, srcTex);
    printErrors("GL::_blur glBindTexture");

    // bind the destination fbo
    glBindFramebuffer(GL_DRAW_FRAMEBUFFER, dstFBO);
    printErrors("GL::_blur glBindFramebuffer");
    glDrawBuffers(1, _fboBuffers);

    // upload uniforms
    Vector2d* off = (horizontal) ? hoffsets : voffsets;
    for(unsigned int i=0; i<KERNELSIZE; i++)
    {
        std::stringstream ss;
        ss << "offsets[" << i << "]";
        glUniform2fv(_shaderBlur->getUniform(ss.str()), 1, (GLfloat*)&off[i]);
        printErrors("GL::_blur loading uniform "+ss.str());
        ss.str("");
        ss << "weights[" << i << "]";
        glUniform1fv(_shaderBlur->getUniform(ss.str()), 1, (GLfloat*)&weights[i]);
        printErrors("GL::_blur loading uniform "+ss.str());
    }

    glUniform1f(_shaderBlur->getUniform("level"), srcLevel);
    printErrors("GL::_blur loading uniform offsets");

    // draw the fullscreen quad
    glBindBuffer(GL_ARRAY_BUFFER, _fullscreenQuadVBO);
    glEnableVertexAttribArray(0);
    glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 0, (const GLvoid*)0);
    printErrors("GL::_blur glVertexAttribPointer");

    glDrawArrays(GL_TRIANGLES, 0, 6);
}

}
