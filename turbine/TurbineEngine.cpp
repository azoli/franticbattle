/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <memory>
#include <stdlib.h>
#include <stdexcept>
#include <fstream>

#include "TurbineEngine.h"
#include "TurbineSceneManager.h"
#include "TurbineEventReceiverDefault.h"
#include "TurbineGL.h"
#include "TurbineGLLegacy.h"
#include "TurbineVector4d.h"

#ifdef _WIN32
#include "TurbineTimerWin.h"
#elif __unix__
#include "TurbineTimerUnix.h"
#endif

using std::shared_ptr;

//#define DEBUG 1

namespace Turbine
{

shared_ptr<Engine> Engine::ePtr;

shared_ptr<Engine> Engine::getInstance()
{
    if(!ePtr)
        ePtr = shared_ptr<Engine>(new Engine);

    return ePtr;
}

void Engine::create(VDriverType dtype, const WindowSettings& settings, IEventReceiver *eventReceiver)
{
    // TODO check if already created
#ifdef _WIN32
	window = new WindowWin(settings);
#elif __unix__
	window = new WindowUnix(settings);
#endif

	switch(dtype)
	{
		case OPENGL:
		{
			renderer = new GL;
			break;
		}

		case OPENGL_LEGACY:
		{
			renderer = new GLLegacy;
			break;
		}
		
		// add other renderer here

		default:
		{
			throw std::invalid_argument("Unknown driver");
		}
	}

    if( !window->create() )
        throw std::runtime_error("Cannot create window");

    if( !renderer->attach(window) )
        throw std::runtime_error("Cannot attach graphic context to the window");

    renderer->init();

    int width = window->getWidth();
    int height = window->getHeight();

    // setup default projections matrix
    _fov = degToRad(45.0);
    _aspectRatio = width/(height*1.0f);
    _zNear = 1.0f;
    _zFar = 2000.0f;
    _projectionMatrix.buildPerspectiveMatrix(_fov, _aspectRatio, _zNear, _zFar);

    // setup default orthogonal matrix
    _orthoMatrix.buildOrthogonalMatrix(-width/2.0, width/2.0, height/2.0, -height/2.0, _zNear, _zFar);
    _orthoMatrix.setTranslation(Vector3d(-1.0, -1.0, 0.0));

	window->show();

    if(eventReceiver)
        receiver = eventReceiver;

	smgr = new SceneManager(renderer);
    guimgr = new GUIManager(renderer);
}

Engine::~Engine()
{
    delete renderer;
    delete window;
    delete smgr;
    delete guimgr;
}

bool Engine::run()
{
	if (renderer != NULL)
	{
		Event event;

		while(window->eventQueued())			// process all event in queue
		{
			window->nextEvent(&event);
			if(event.type == EV_RESIZE)
			    _updateViewport = true;
			if(!receiver->onEvent(event))		// returning false on onEvent failed (stop running)
				return false;
		}
		return true;								// and true else
	}		

	return false;	// finally return false (stop running) if not renderer loaded return false
}

void Engine::draw()
{
    int width = window->getWidth();
    int height = window->getHeight();

    // TODO: move into run..
    if(_updateViewport)
    {
        renderer->resizeScene(width, height);

        _aspectRatio = width/(height*1.0f);
        _projectionMatrix.buildPerspectiveMatrix(_fov, _aspectRatio, _zNear, _zFar);

        _orthoMatrix.buildOrthogonalMatrix(-width/2.0, width/2.0, height/2.0, -height/2.0, _zNear, _zFar);
        _orthoMatrix.setTranslation(Vector3d(-1.0, -1.0, 0.0));
        _updateViewport = false;
    }

    renderer->beginScene();

    renderer->setTransform(TS_PROJECTION, _projectionMatrix);
    smgr->drawAll();

    renderer->setTransform(TS_PROJECTION, _orthoMatrix);
    guimgr->drawAll(width, height);

    renderer->endScene();
}

bool Engine::setWindowTitle(const std::string& title)
{
	return window->setTitle(title);
}

void Engine::setEventReceiver(IEventReceiver* eventReceiver)
{
    receiver = eventReceiver;
}

Renderer *Engine::getRenderer()
{
	return renderer;
}

SceneManager *Engine::getSceneManager()
{
	return smgr;
}

GUIManager *Engine::getGUIManager()
{
    return guimgr;
}

std::string Engine::getAbsolutePath(std::string resource)
{
    std::string paths[3] = { "./", "./data/", "/usr/share/FranticBattle/" };

    for(unsigned int i=0; i<3; i++)
    {
        std::string pathname = paths[i] + resource;
        std::ifstream iss;
#ifdef DEBUG
        std::cout << "trying to open: " << pathname << std::endl;
#endif
        iss.open(pathname.c_str(), std::ios::in | std::ios::binary);
        if(iss.is_open())
        {
#ifdef DEBUG
            std::cout << "found: " << pathname << std::endl;
#endif
            return pathname;
        }
    }

#ifdef DEBUG
    std::cout << "not found" << std::endl;
#endif
    return std::string();
}

void Engine::toggleFullscreen()
{
    window->toggleFullscreen();
    renderer->resizeScene(window->getWidth(), window->getHeight());
}

Vector3d Engine::project(const Vector3d& point, const Matrix4& model)
{
    Matrix4 view = smgr->getActiveCamera()->getViewMatrix();
    Vector4d pointClip = Vector4d(point) * model * view * _projectionMatrix;
    Vector3d pointNDC(pointClip.x/pointClip.w, pointClip.y/pointClip.w, pointClip.z/pointClip.w);
    pointNDC.x = pointNDC.x * 0.5 + 0.5;
    pointNDC.y = pointNDC.y * 0.5 + 0.5;
    pointNDC.z = (pointNDC.z +1.0) * 0.5;

    return pointNDC;
}

std::shared_ptr<Timer> Engine::getNewTimer()
{
#ifdef _WIN32
	return std::shared_ptr<Timer>(new TimerWin);
#elif __unix__
	return std::shared_ptr<Timer>(new TimerUnix);
#endif
}

}
