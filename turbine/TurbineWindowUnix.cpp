/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <X11/Xlib.h>
#include <X11/keysym.h>
#include <X11/Xutil.h>
#include <X11/Xatom.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
//without keysym do not work in faculty...

#include <iostream>

using namespace std;

#include "TurbineWindowUnix.h"

namespace Turbine
{

bool WindowUnix::create()
{
  	XSetWindowAttributes attr;
	dpy = XOpenDisplay(0);
	screen = DefaultScreen(dpy);
	int defaultdepth = DefaultDepth(dpy,screen);

	XVisualInfo vinfo;
	XVisualInfo *vinfo_ret;
	int numitems, maxdepth;

	vinfo.c_class = TrueColor;

	vinfo_ret = XGetVisualInfo(dpy, VisualClassMask, &vinfo, &numitems);

	if (numitems == 0)
	{
		cout << "Visual selection failed: Colorbits not supported" << endl;
		return false;
	}

	maxdepth = 0;
	while(numitems > 0)
	{
		if (vinfo_ret[numitems-1].depth > maxdepth) {
			maxdepth = vinfo_ret[numitems-1 ].depth;
		}
		numitems--;
	}
	XFree((void *) vinfo_ret);

	if(defaultdepth < maxdepth) maxdepth=defaultdepth;
	if(_depthbits < maxdepth) maxdepth=_depthbits;

	if (XMatchVisualInfo(dpy, screen, maxdepth, TrueColor, &vinfo))
	{
		_depthbits = maxdepth;
		visual = vinfo.visual;
	}
	else
	{
		cout << "Visual selection failed: Cannot find a valid TrueColor, DirectColor or PseudoColor visual" << endl;
		return false;
	}

	// passed value 0, we use current width and height
	if(_width == 0 || _height == 0)
	{
		_width = DisplayWidth(dpy, 0);
		_height = DisplayHeight(dpy, 0);
	}

    _bitsperpixel = vinfo.bits_per_rgb;

	if(_fullscreen)
	{
		attr.override_redirect = True;
		attr.event_mask = ExposureMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask | StructureNotifyMask;
		attr.background_pixel = XBlackPixel(dpy, screen);

		win = XCreateWindow(dpy, RootWindow(dpy, screen), 0, 0, _width, _height, 0, _depthbits, InputOutput, visual, CWEventMask | CWOverrideRedirect | CWBackPixel, &attr);

		XWarpPointer(dpy, None, win, 0, 0, 0, 0, 0, 0);
		XMapRaised(dpy, win);
		XGrabKeyboard(dpy, win, True, GrabModeAsync, GrabModeAsync, CurrentTime);
		XGrabPointer(dpy, win, True, ButtonPressMask, GrabModeAsync, GrabModeAsync, win, None, CurrentTime);
	}
	else
	{
		/* create a window in window mode*/
		attr.event_mask = ExposureMask | KeyPressMask | KeyReleaseMask | ButtonPressMask | ButtonReleaseMask | PointerMotionMask | StructureNotifyMask;
		attr.background_pixel = XBlackPixel(dpy, screen);

		win = XCreateWindow(dpy, RootWindow(dpy, screen), 0, 0, _width, _height, 0, _depthbits, InputOutput, visual, CWEventMask | CWBackPixel, &attr);
	}

	setTitle(_title);

	show();

    // test if the window manager support EWMH fullscreen
    int form;
    unsigned long len, j, remain;
    Atom *actions;
    Atom act, prop, type;

    prop = XInternAtom(dpy,"_NET_WM_ALLOWED_ACTIONS",False);
    act = XInternAtom(dpy,"_NET_WM_ACTION_FULLSCREEN",False);

    _EWMHSupport = false;
    if (XGetWindowProperty(dpy, win, prop, 0, 1024, False, XA_ATOM, &type, &form, &len, &remain, (unsigned char**)&actions) == Success)
        for (j=0;j<len;j++) 
            if (act == actions[j]) _EWMHSupport = true;

    return true;
}

void WindowUnix::toggleFullscreen()
{
    if(_EWMHSupport)
    {
        XEvent event;
        event.type = ClientMessage;
        event.xclient.display = dpy;
        event.xclient.window = win;
        event.xclient.message_type = XInternAtom(dpy, "_NET_WM_STATE", False);
        event.xclient.format = 32;
        event.xclient.data.l[0] = 2;  /* set (2 is toggle) */
        event.xclient.data.l[1] = XInternAtom(dpy,"_NET_WM_STATE_FULLSCREEN",False);
        event.xclient.data.l[2] = 0;
        event.xclient.data.l[3] = 0;
        event.xclient.data.l[4] = 0;

        XSendEvent(dpy, DefaultRootWindow(dpy), False, StructureNotifyMask | ResizeRedirectMask, &event);

        _fullscreen = !_fullscreen;
    }
}

bool WindowUnix::kill()
{
	XDestroyWindow(dpy, win);
	XCloseDisplay(dpy);

	return true;
}

void WindowUnix::refresh()
{
	XFlush(dpy);
}

bool WindowUnix::setTitle(const std::string& title)
{
	if(dpy == 0)
		return false;

    XSetStandardProperties(dpy, win, title.c_str(), title.c_str(), None, 0, 0, 0);

	return true;
}

void WindowUnix::show()
{
	XMapRaised(dpy, win);
}

void WindowUnix::hide()
{
	XUnmapWindow(dpy, win);
}


/** eventQueued **************************************************************/
bool WindowUnix::eventQueued()
{
	while(!eventPresent)
	{
		if(XPending(dpy) > 0)
			processXevent();
		else
			return false;
	}

	return true;
}

/** nextEvent ****************************************************************/
bool WindowUnix::nextEvent(Event *ev)
{
	if( (!eventPresent) && (!eventQueued()) )
			return false;

	*ev = event;
	eventPresent = false;

	return true;
}

/** processXevent ************************************************************/
void WindowUnix::processXevent()
{
	XEvent xevent;

	XNextEvent(dpy, &xevent);

	memset(&event, 0, sizeof(event));

    switch (xevent.type)
    {
        case ConfigureNotify:
        {
            if(xevent.xconfigure.width != _width || xevent.xconfigure.height != _height)
            {
                _width = xevent.xconfigure.width;
                _height = xevent.xconfigure.height;
                event.type = EV_RESIZE;
                eventPresent = true;
            }
            break;
        }
		case Expose:
		{
			if (xevent.xexpose.count == 0)
			{
				event.type = EV_DRAW;
				eventPresent = true;
			}
			break;
		}
		case KeyPress:
		case KeyRelease:
		{
			event.evKey.type = EV_KEYBOARD;
			char asciiKey;
			int key;
			XLookupString(&xevent.xkey, &asciiKey, 1, (KeySym*)&key, 0);

			Key enumKey = keyMap[key];

			if(enumKey != 0)
			{
				event.evKey.asciiKey = asciiKey;
				event.evKey.key = enumKey;
				event.evKey.pressed = (xevent.type == KeyPress);
				event.evKey.control	= (xevent.xkey.state & ControlMask) != 0;
				event.evKey.shift		= (xevent.xkey.state & ShiftMask) != 0;
				event.evKey.alt		= (xevent.xkey.state & (Mod1Mask | Mod4Mask)) != 0;
				eventPresent = true;
			}
			break;
		}
		
		case ButtonPress:
		case ButtonRelease:
		{
			event.evMouse.type = EV_MOUSE;
			event.evMouse.pressed = (xevent.xbutton.type == ButtonPress);
			event.evMouse.x = xevent.xbutton.x;
			event.evMouse.y = xevent.xbutton.y;
			
			eventPresent = true;
			
			switch(xevent.xbutton.button)
			{
				case Button1:
					event.evMouse.button = BTN_LEFT;
					break;

				case Button3:
					event.evMouse.button = BTN_RIGHT;
					break;

				case  Button2:
					event.evMouse.button = BTN_MIDDLE;
					break;

				case  Button4:
					event.evMouse.button = BTN_WHEELUP;
					break;

				case  Button5:
					event.evMouse.button = BTN_WHEELDOWN;
					break;
				
				default:
					eventPresent = false;
			}
			
			break;
		}

		case MotionNotify:
		{
			
			event.evMouse.type = EV_MOUSE;
			event.evMouse.x = xevent.xbutton.x;
			event.evMouse.y = xevent.xbutton.y;
			
			eventPresent = true;
			
			event.evMouse.button = BTN_MOVE;
			break;
		}
				
		case ClientMessage:
		{
			if( *XGetAtomName(dpy, xevent.xclient.message_type) == *"WM_PROTOCOLS")
				event.type = EV_QUIT;

				eventPresent = true;
			break;
		}
		
		default:
		{
			break;
		}
	}
}

void WindowUnix::createKeyMap()
{
	keyMap[XK_BackSpace] = KEY_BACK;
	keyMap[XK_Tab] = KEY_TAB;
	keyMap[XK_KP_Tab] = KEY_TAB;
	keyMap[XK_Clear] = KEY_CLEAR;
	keyMap[XK_Return] = KEY_RETURN;
	keyMap[XK_KP_Enter] = KEY_RETURN;
	keyMap[XK_Pause] = KEY_PAUSE;
	keyMap[XK_Caps_Lock] = KEY_CAPITAL;
	keyMap[XK_Shift_Lock] = KEY_CAPITAL;
	keyMap[XK_Escape] = KEY_ESCAPE;
	keyMap[XK_KP_Space] = KEY_SPACE;
	keyMap[XK_space] = KEY_SPACE;

	keyMap[XK_Prior] = KEY_PAGEUP;
	keyMap[XK_Next] = KEY_PAGEDOWN;
	keyMap[XK_Page_Up] = KEY_PAGEUP;
	keyMap[XK_Page_Down] = KEY_PAGEDOWN;
	keyMap[XK_KP_Prior] = KEY_PAGEUP;
	keyMap[XK_KP_Next] = KEY_PAGEDOWN;
	keyMap[XK_KP_Page_Up] = KEY_PAGEUP;
	keyMap[XK_KP_Page_Down] = KEY_PAGEDOWN;

	keyMap[XK_End] = KEY_END;
	keyMap[XK_KP_End] = KEY_END;
	keyMap[XK_Home] = KEY_HOME;
	keyMap[XK_Begin] = KEY_HOME;
	keyMap[XK_KP_Begin] = KEY_HOME;

	keyMap[XK_Left] = KEY_LEFT;
	keyMap[XK_Up] = KEY_UP;
	keyMap[XK_Right] = KEY_RIGHT;
	keyMap[XK_Down] = KEY_DOWN;
	keyMap[XK_KP_Left] = KEY_LEFT;
	keyMap[XK_KP_Up] = KEY_UP;
	keyMap[XK_KP_Right] = KEY_RIGHT;
	keyMap[XK_KP_Down] = KEY_DOWN;

	keyMap[XK_KP_Insert] = KEY_INSERT;
	keyMap[XK_KP_Delete] = KEY_DELETE;
	keyMap[XK_Delete] = KEY_DELETE;

	keyMap[XK_0] = KEY_KEY_0;
	keyMap[XK_1] = KEY_KEY_1;
	keyMap[XK_2] = KEY_KEY_2;
	keyMap[XK_3] = KEY_KEY_3;
	keyMap[XK_4] = KEY_KEY_4;
	keyMap[XK_5] = KEY_KEY_5;
	keyMap[XK_6] = KEY_KEY_6;
	keyMap[XK_7] = KEY_KEY_7;
	keyMap[XK_8] = KEY_KEY_8;
	keyMap[XK_9] = KEY_KEY_9;
	keyMap[XK_KP_0] = KEY_KEY_0;
	keyMap[XK_KP_1] = KEY_KEY_1;
	keyMap[XK_KP_2] = KEY_KEY_2;
	keyMap[XK_KP_3] = KEY_KEY_3;
	keyMap[XK_KP_4] = KEY_KEY_4;
	keyMap[XK_KP_5] = KEY_KEY_5;
	keyMap[XK_KP_6] = KEY_KEY_6;
	keyMap[XK_KP_7] = KEY_KEY_7;
	keyMap[XK_KP_8] = KEY_KEY_8;
	keyMap[XK_KP_9] = KEY_KEY_9;

	keyMap[XK_A] = KEY_KEY_A;
	keyMap[XK_B] = KEY_KEY_B;
	keyMap[XK_C] = KEY_KEY_C;
	keyMap[XK_D] = KEY_KEY_D;
	keyMap[XK_E] = KEY_KEY_E;
	keyMap[XK_F] = KEY_KEY_F;
	keyMap[XK_G] = KEY_KEY_G;
	keyMap[XK_H] = KEY_KEY_H;
	keyMap[XK_I] = KEY_KEY_I;
	keyMap[XK_J] = KEY_KEY_J;
	keyMap[XK_K] = KEY_KEY_K;
	keyMap[XK_L] = KEY_KEY_L;
	keyMap[XK_M] = KEY_KEY_M;
	keyMap[XK_N] = KEY_KEY_N;
	keyMap[XK_O] = KEY_KEY_O;
	keyMap[XK_P] = KEY_KEY_P;
	keyMap[XK_Q] = KEY_KEY_Q;
	keyMap[XK_R] = KEY_KEY_R;
	keyMap[XK_S] = KEY_KEY_S;
	keyMap[XK_T] = KEY_KEY_T;
	keyMap[XK_U] = KEY_KEY_U;
	keyMap[XK_V] = KEY_KEY_V;
	keyMap[XK_W] = KEY_KEY_W;
	keyMap[XK_X] = KEY_KEY_X;
	keyMap[XK_Y] = KEY_KEY_Y;
	keyMap[XK_Z] = KEY_KEY_Z;
	keyMap[XK_a] = KEY_KEY_A;
	keyMap[XK_b] = KEY_KEY_B;
	keyMap[XK_c] = KEY_KEY_C;
	keyMap[XK_d] = KEY_KEY_D;
	keyMap[XK_e] = KEY_KEY_E;
	keyMap[XK_f] = KEY_KEY_F;
	keyMap[XK_g] = KEY_KEY_G;
	keyMap[XK_h] = KEY_KEY_H;
	keyMap[XK_i] = KEY_KEY_I;
	keyMap[XK_j] = KEY_KEY_J;
	keyMap[XK_k] = KEY_KEY_K;
	keyMap[XK_l] = KEY_KEY_L;
	keyMap[XK_m] = KEY_KEY_M;
	keyMap[XK_n] = KEY_KEY_N;
	keyMap[XK_o] = KEY_KEY_O;
	keyMap[XK_p] = KEY_KEY_P;
	keyMap[XK_q] = KEY_KEY_Q;
	keyMap[XK_r] = KEY_KEY_R;
	keyMap[XK_s] = KEY_KEY_S;
	keyMap[XK_t] = KEY_KEY_T;
	keyMap[XK_u] = KEY_KEY_U;
	keyMap[XK_v] = KEY_KEY_V;
	keyMap[XK_w] = KEY_KEY_W;
	keyMap[XK_x] = KEY_KEY_X;
	keyMap[XK_y] = KEY_KEY_Y;
	keyMap[XK_z] = KEY_KEY_Z;

	keyMap[XK_Meta_L] = KEY_LWIN;
	keyMap[XK_Meta_R] = KEY_RWIN;

	keyMap[XK_KP_Multiply] = KEY_MULTIPLY;
	keyMap[XK_KP_Add] = KEY_ADD;
	keyMap[XK_KP_Separator] = KEY_SEPARATOR;
	keyMap[XK_KP_Subtract] = KEY_SUBTRACT;
	keyMap[XK_KP_Decimal] = KEY_DECIMAL;
	keyMap[XK_KP_Divide] = KEY_DIVIDE;

	keyMap[XK_F1] = KEY_F1;
	keyMap[XK_F2] = KEY_F2;
	keyMap[XK_F3] = KEY_F3;
	keyMap[XK_F4] = KEY_F4;
	keyMap[XK_F5] = KEY_F5;
	keyMap[XK_F6] = KEY_F6;
	keyMap[XK_F7] = KEY_F7;
	keyMap[XK_F8] = KEY_F8;
	keyMap[XK_F9] = KEY_F9;
	keyMap[XK_F10] = KEY_F10;
	keyMap[XK_F11] = KEY_F11;
	keyMap[XK_F12] = KEY_F12;

	keyMap[XK_Scroll_Lock] = KEY_SCROLL;

	keyMap[XK_Shift_L] = KEY_LSHIFT;
	keyMap[XK_Shift_R] = KEY_RSHIFT;
	keyMap[XK_Control_L] = KEY_LCONTROL;
	keyMap[XK_Control_R] = KEY_RCONTROL;
	keyMap[XK_Alt_L] = KEY_LALT;
	keyMap[XK_Menu] = KEY_LALT;
	keyMap[XK_Alt_R] = KEY_RALT;
	keyMap[XK_ISO_Level3_Shift] = KEY_RALT;

	keyMap[XK_plus] = KEY_PLUS;
	keyMap[XK_comma] = KEY_COMMA;
	keyMap[XK_minus] = KEY_MINUS;
	keyMap[XK_period] = KEY_PERIOD;
}

}
