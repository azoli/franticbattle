/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdlib.h>
#include "TurbineGLLegacy.h"

using namespace std;

namespace Turbine
{

GLLegacy::GLLegacy() : _lightingEnabled(true), _ambientLight(0.0f, 0.0f, 0.0f, 1.0f)
{
#ifdef _WIN32
	glDev = new GLDeviceWin;
#elif __unix__
	glDev = new GLDeviceUnix;
#endif
	
	texLoaded.reserve(20);
	for(int i = 0; i < 20; i++)
		texLoaded[i] = 0;
}

GLLegacy::~GLLegacy()
{
    delete glDev;
}

bool GLLegacy::attach(IWindow *window)
{
	win = window;

	if( !glDev->init(win) )
		return false;

	return true;
}

bool GLLegacy::detach()
{
	if( !glDev->release() )
		return false;

	return true;
}

bool GLLegacy::init()
{
	resizeScene(win->getWidth(),win->getHeight());

	glGenTextures(20, &vTex[0]);
	
	glShadeModel(GL_SMOOTH);								// enable smooth shading
	glClearColor(0.0f, 0.0f, 0.0f, 1.0f);				// black background
	glClearDepth(1.0f);

	glEnable(GL_DEPTH_TEST);
	glEnable(GL_LIGHTING);

	glDepthFunc(GL_LEQUAL);
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST);

	// init projection matrix
	GLfloat glmat[16];
	
	glEnable( GL_CULL_FACE );
	
	createGLMatrix(glmat, _matrix[TS_PROJECTION]);
	glMatrixMode(GL_PROJECTION);
	glLoadMatrixf(glmat);
	glMatrixMode(GL_MODELVIEW);						//return to default matrix (modelview)
	
	glClear(GL_COLOR_BUFFER_BIT);						// clear screen
	glDev->swapBuffers();								// show black screen
	
	return true;
}

void GLLegacy::resizeScene(int width, int height)
{
	if (height==0)											// prevent a divide by zero
		height=1;

	glViewport(0,0,width,height);
}

void GLLegacy::beginScene()
{
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);	// clear screen and depth buffer
   _index = 0;
}

void GLLegacy::endScene()
{
	glDev->swapBuffers();
}

void GLLegacy::setTransform(TRANSFORMATION_STATE state, const Matrix4& mat)
{
	GLfloat glmat[16];

	_matrix[state] = mat;

	switch(state)
	{
		case TS_VIEW:
		case TS_MODEL:
		{
			Matrix4 modelview = _matrix[TS_VIEW] * _matrix[TS_MODEL];
			createGLMatrix(glmat, modelview);
			glLoadMatrixf(glmat);
			break;
		}
		case TS_PROJECTION:
		{
			createGLMatrix(glmat, mat);
			glMatrixMode(GL_PROJECTION);
			glLoadMatrixf(glmat);
			glMatrixMode(GL_MODELVIEW);			//return to default matrix (modelview)
			break;
		}
		default:
		{
			cerr << "warning: matrix trasformation type unknown (ignoring)" << endl;
			break;
		}
	}
}

const Matrix4& GLLegacy::getTransform(TRANSFORMATION_STATE state)
{
	return _matrix[state];
}

void GLLegacy::createGLMatrix(GLfloat* outMatrix, const Matrix4& sourceMatrix)
{
	for(int i=0; i<16; i++)
		outMatrix[i] = sourceMatrix[i];
}

void GLLegacy::enableLighting( bool enable )
{
	if(_lightingEnabled != enable)
	{
		_lightingEnabled = enable;
		(_lightingEnabled) ? glEnable(GL_LIGHTING) : glDisable(GL_LIGHTING);
	}
}

bool GLLegacy::isEnabledLighting()
{
	return _lightingEnabled;
}

void GLLegacy::drawLight(Light* light)
{
    GLfloat data[4];
    Vector3d position = light->getPosition();
    Colorf diffuseColor = light->getDiffuseColor();
    Colorf specularColor = light->getSpecularColor();
    Colorf ambientColor = light->getAmbientColor();

    int idx = GL_LIGHT0 + _index;

    // set position
    data[0] = position.x;
    data[1] = position.y;
    data[2] = position.z;
    if(light->getType() == L_DIRECTIONAL)
        data[3] = 1.0;
    else
        data[3] = 0.0;
    glLightfv(idx, GL_POSITION, data);

    // set diffuse color
    data[0] = diffuseColor.r;
    data[1] = diffuseColor.g;
    data[2] = diffuseColor.b;
    data[3] = diffuseColor.a;
    glLightfv(idx, GL_DIFFUSE, data);

    // set specular color
    data[0] = specularColor.r;
    data[1] = specularColor.g;
    data[2] = specularColor.b;
    data[3] = specularColor.a;
    glLightfv(idx, GL_SPECULAR, data);

    // set ambient color
    data[0] = ambientColor.r;
    data[1] = ambientColor.g;
    data[2] = ambientColor.b;
    data[3] = ambientColor.a;
    glLightfv(idx, GL_AMBIENT, data);

    // TODO support spotlights and pointlights
}

void GLLegacy::loadTexture(Image *image)
{
    glBindTexture(GL_TEXTURE_2D, vTex[image->index]);
    switch(image->type)
    {
        case RGB_24BIT_U:
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, image->width, image->height, 0, GL_RGB, GL_UNSIGNED_BYTE, image->data);
            break;
        }
        case RGBA_32BIT_U:
        {
            glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, image->width, image->height, 0, GL_RGBA, GL_UNSIGNED_BYTE, image->data);
//          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
//          glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
            break;
        }
        default:
        {
            cout << "wrong texture type" << endl;
        }

        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MAG_FILTER,GL_LINEAR);
        glTexParameteri(GL_TEXTURE_2D,GL_TEXTURE_MIN_FILTER,GL_LINEAR);

        texLoaded[image->index] = image->index;
    }
}

bool GLLegacy::createHardwareBuffers(Model *model)
{
    vector<Material>::iterator material;
    for(material = model->materials.begin(); material != model->materials.end(); material++)
    {
        Image* tex = (*material).texture;
        if( tex && texLoaded[tex->index]!=0 )
            loadTexture(tex);
    }

    return true;
}

void GLLegacy::destroyHardwareBuffers(Model* model)
{}

void GLLegacy::drawEntity(Entity* entity)
{
//	glPolygonMode(GL_FRONT_AND_BACK,GL_LINE);
    if(entity == NULL)
    {
        cout << "error in GLLegacy::drawModel: passing NULL model pointer" << endl;
        return;
    }

    Model* model = entity->getModel();
    list<SmoothingGroup>::iterator group;
    for(group = model->smoothGroups.begin(); group != model->smoothGroups.end(); group++)
    {
        Material mtr = model->materials[(*group).mtlIndex];

        GLfloat ambient[] = { mtr.ambient*mtr.kd.r, mtr.ambient*mtr.kd.g, mtr.ambient*mtr.kd.b, mtr.ambient*mtr.kd.a };
	GLfloat diffuse[] = { mtr.kd.r, mtr.kd.g, mtr.kd.b, mtr.kd.a };
	GLfloat specular[] = { mtr.ks.r, mtr.ks.g, mtr.ks.b, mtr.ks.a };
	GLfloat shininess[] = { mtr.ns };

        glMaterialfv(GL_FRONT, GL_AMBIENT, ambient);
        glMaterialfv(GL_FRONT, GL_DIFFUSE, diffuse);
        glMaterialfv(GL_FRONT, GL_SPECULAR, specular);
        glMaterialfv(GL_FRONT, GL_SHININESS, shininess);

        if( mtr.texture != NULL )
        {
            glBindTexture(GL_TEXTURE_2D, vTex[mtr.texture->index] );
            glEnable(GL_TEXTURE_2D);
        }
        else
            glDisable(GL_TEXTURE_2D);

        glBegin (GL_TRIANGLES);
        for(int facenum=(*group).start; facenum<(*group).end; facenum++)
        {
            for(int d=0; d<3; d++)
            {
                int index = model->faces[facenum].index[d];
                Vertex v = model->vertices[index];
                glTexCoord2f(v.texCoord.x, v.texCoord.y);
                glNormal3f(v.normal.x, v.normal.y, v.normal.z);
                glVertex3f(v.coord.x, v.coord.y, v.coord.z);
            }
        }
        glEnd();
    }
	
    glFlush();
}

unsigned int GLLegacy::pickupColor(int x, int y)
{
    // TODO stub
    return 0;
}

}
