/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ENGINE_H
#define ENGINE_H

#include <memory>

#include "TurbineMatrix4.h"
#include "TurbineIWindow.h"
#include "TurbineIEventReceiver.h"
#include "TurbineEventReceiverDefault.h"
#include "TurbineRenderer.h"
#include "TurbineSceneManager.h"
#include "TurbineGUIManager.h"
#include "TurbineTimer.h"

namespace Turbine
{

enum VDriverType
{
	OPENGL,
	OPENGL_LEGACY,
	DIRECTX8,
	DIRECTX9,
	DIRECTX10
};

class Engine
{

public:

    ~Engine();

    // get a singleton instance pointer
    static std::shared_ptr<Engine> getInstance();

    // NEEDED: setup the engine (works only the first time it is called)
    // TODO: move to config file
    void create(VDriverType dtype, const WindowSettings& settings, IEventReceiver *eventReceiver);

/** Run **********************************************************************/
	bool run();
        void draw();

/** Window *******************************************************************/
	bool setWindowTitle(const std::string& title);

	void toggleFullscreen();

/** Event Receiver ***********************************************************/
    void setEventReceiver(IEventReceiver *eventReceiver);

/** Interface gets ***********************************************************/
	Renderer* getRenderer();
	SceneManager* getSceneManager();
    GUIManager* getGUIManager();

/** Locate resources**********************************************************/
    std::string getAbsolutePath(std::string resource);

/** Projection ***************************************************************/
    void setDefaultProjectionMatrix()
    {
        _fov = degToRad(45.0);
        _aspectRatio = window->getWidth()/(window->getHeight()*1.0f);
        _zNear = 1.0f;
        _zFar = 2000.0f;
        _projectionMatrix.buildPerspectiveMatrix(_fov, _aspectRatio, _zNear, _zFar);
        renderer->setTransform(TS_PROJECTION, _projectionMatrix);
    }

    void setProjectionMatrix(double fov, double aspectRatio, double zNear, double zFar)
    {
        _fov = fov;
        _aspectRatio = aspectRatio;
        _zNear = zNear;
        _zFar = zFar;
        
        _projectionMatrix.buildPerspectiveMatrix(_fov, _aspectRatio, _zNear, _zFar);
        renderer->setTransform(TS_PROJECTION, _projectionMatrix);
    }

    const Matrix4& getProjectionMatrix()
    {
        return _projectionMatrix;
    }

    double getFov(){ return _fov; }
    double getAspectRatio(){ return _aspectRatio; }
    double getNearPlane(){ return _zNear; }
    double getFarPlane(){ return _zFar; }

    Vector3d project(const Vector3d& point, const Matrix4& model);

    Vector2d getWindowSize()
    {
        return Vector2d(window->getWidth(), window->getHeight());
    }

    void toggleLighting()
    {
        renderer->enableLighting(!renderer->isEnabledLighting());
    }

	std::shared_ptr<Timer> getNewTimer();

private:

    Engine() : window(0), receiver(&eventReceiverDefault), renderer(0), _updateViewport(true), quit(false) {}

    static std::shared_ptr<Engine> ePtr;

	IWindow *window;
    EventReceiverDefault eventReceiverDefault;
	IEventReceiver *receiver;
	Renderer *renderer;
	SceneManager *smgr;
    GUIManager *guimgr;

    Matrix4 _projectionMatrix;
    Matrix4 _orthoMatrix;
    double _fov, _aspectRatio, _zNear, _zFar;
    bool _updateViewport;

	bool quit;
};

}

#endif /* ENGINE_H */
