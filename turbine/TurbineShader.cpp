/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>
#include <stdexcept>

#include "TurbineShader.h"

using std::string;
using std::stringstream;

namespace Turbine
{

//#define DEBUG 1

void printErrors(const string& glfunc)
{
    GLenum error;
    stringstream stream;
    while( (error = glGetError()) != GL_NO_ERROR )
        stream << "GL error in function " << glfunc << "(): " << error << std::endl;

    const string& str = stream.str();
    if( str.size() > 0)
        throw std::runtime_error(str);
}

Shader::Shader(const string name, string vertShaderSrc, string fragShaderSrc) : _name(name), _vsSrc(vertShaderSrc), _fsSrc(fragShaderSrc)
{
    _handle = glCreateProgram();

    printErrors("Shader::Shader glCreateProgram");
}

void Shader::_throwCompileErrors(GLuint handle)
{
    GLint testVal;
    glGetShaderiv(handle, GL_COMPILE_STATUS, &testVal);
    printErrors("Shader::_throwCompileErrors glGetShaderiv");
    if(testVal == GL_FALSE)
    {
        char infoLog[1024];
        glGetShaderInfoLog(handle, 1024, NULL, infoLog);
        stringstream stream;
        stream << "The shader " << handle << " failed to compile with the following error:\n" << infoLog << std::endl;
        throw std::runtime_error(stream.str());
    }
}

void Shader::_throwLinkingErrors()
{
    GLint testVal;
    glGetProgramiv(_handle, GL_LINK_STATUS, &testVal);
    printErrors("Shader::build glGetProgramiv");
    if(testVal == GL_FALSE)
    {
        char infoLog[1024];
        glGetProgramInfoLog(_handle, 1024, NULL, infoLog);
        stringstream stream;
        stream  << "The program" << _handle << " failed to link with the following error:\n" << infoLog << std::endl;
        throw std::runtime_error(stream.str());
    }
}


bool Shader::build()
{
#ifdef DEBUG
    std::cout << _name << std::endl;
#endif
    // loading sources
    _vs = glCreateShader(GL_VERTEX_SHADER);
    printErrors("Shader::build glCreateShader VS");

    _fs = glCreateShader(GL_FRAGMENT_SHADER);
    printErrors("Shader::build glCreateShader FS");

    const GLchar* vsSrc = _vsSrc.c_str();
    const GLint vsSize = _vsSrc.size();
    glShaderSource(_vs, 1, &vsSrc, &vsSize);
    printErrors("ShaderObject::Load glShaderSource VS");

    const GLchar* fsSrc = _fsSrc.c_str();
    const GLint fsSize = _fsSrc.size();
    glShaderSource(_fs, 1, &fsSrc, &fsSize);
    printErrors("ShaderObject::Load glShaderSource FS");

    // compiling
    glCompileShader(_vs);
    printErrors("Shader::build glCompileShader VS");
    _throwCompileErrors(_vs);
    glCompileShader(_fs);
    printErrors("Shader::build glCompileShader FS");
    _throwCompileErrors(_fs);

    // attach to the shader program
    glAttachShader(_handle, _fs);
    printErrors("Shader::build glAttachShader FS");
    glAttachShader(_handle, _vs);
    printErrors("Shader::build glAttachShader VS");

    // use fixed attribute locations
    glBindAttribLocation(_handle, 0, "vertex");
    printErrors("Shader::build vertex attribute");
    glBindAttribLocation(_handle, 1, "normal");
    printErrors("Shader::build normal attribute");
    glBindAttribLocation(_handle, 2, "texcoord");
    printErrors("Shader::build texcoord attribute");
    glBindAttribLocation(_handle, 3, "bIndices");
    printErrors("Shader::build bIndices attribute");
    glBindAttribLocation(_handle, 4, "bWeights");
    printErrors("Shader::build bWeights attribute");

    // linking
    glLinkProgram(_handle);
    printErrors("Shader::build glLinkProgram");

    use();

    // get uniform locations
    GLint uniformNum;
    glGetProgramiv(_handle, GL_ACTIVE_UNIFORMS, &uniformNum);
    GLint size;
    GLenum type;
    GLchar name[512];
    for(int i=0; i<uniformNum; i++)
    {
        glGetActiveUniform(_handle, i, 512, NULL, &size, &type, name);
        printErrors("Shader::build glGetActiveUniform");
        GLint location = glGetUniformLocation(_handle, name);
        printErrors("Shader::build glGetUniformLocation");
        uniforms[name] = location;
#ifdef DEBUG
        std::cout << name << " " << location << std::endl;
#endif
    }

    return true;
}

void Shader::addUniformName(std::string name)
{
#ifdef DEBUG
    std::cout << _name << std::endl;
#endif
    GLint location = glGetUniformLocation(_handle, name.c_str());
    uniforms[name.c_str()] = location;
#ifdef DEBUG
    std::cout << name << " " << location << std::endl;
#endif
}

int Shader::getUniform(std::string name)
{
    std::map<std::string, GLint>::iterator it = uniforms.find(name);
    if(it != uniforms.end())
        return (*it).second;
    return -1;
}

void Shader::use()
{
    glUseProgram(_handle);
    printErrors("Shader::use glUseProgram");
}

}

