/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <windows.h>
#include <windowsx.h>

#include "TurbineWindowWin.h"

namespace Turbine
{

WindowWin::WindowWin(const WindowSettings& settings) : IWindow(settings), fullscreen(false), hDC(0), hRC(0), hWnd(0)
{
    _keyMap[VK_BACK] = KEY_BACK;
    _keyMap[VK_TAB] = KEY_TAB;
    _keyMap[VK_CLEAR] = KEY_CLEAR;
    _keyMap[VK_RETURN] = KEY_RETURN;
    _keyMap[VK_PAUSE] = KEY_PAUSE;
    _keyMap[VK_CAPITAL] = KEY_CAPITAL;
    _keyMap[VK_SHIFT] = KEY_CAPITAL;
    _keyMap[VK_ESCAPE] = KEY_ESCAPE;
    _keyMap[VK_SPACE] = KEY_SPACE;

    _keyMap[VK_PRIOR] = KEY_PAGEUP;
    _keyMap[VK_NEXT] = KEY_PAGEDOWN;

    _keyMap[VK_END] = KEY_END;
    _keyMap[VK_HOME] = KEY_HOME;

    _keyMap[VK_LEFT] = KEY_LEFT;
    _keyMap[VK_UP] = KEY_UP;
    _keyMap[VK_RIGHT] = KEY_RIGHT;
    _keyMap[VK_DOWN] = KEY_DOWN;

    _keyMap[VK_INSERT] = KEY_INSERT;
    _keyMap[VK_DELETE] = KEY_DELETE;

    _keyMap[0x30] = KEY_KEY_0;
    _keyMap[0x31] = KEY_KEY_1;
    _keyMap[0x32] = KEY_KEY_2;
    _keyMap[0x33] = KEY_KEY_3;
    _keyMap[0x34] = KEY_KEY_4;
    _keyMap[0x35] = KEY_KEY_5;
    _keyMap[0x36] = KEY_KEY_6;
    _keyMap[0x37] = KEY_KEY_7;
    _keyMap[0x38] = KEY_KEY_8;
    _keyMap[0x39] = KEY_KEY_9;

    _keyMap[0x41] = KEY_KEY_A;
    _keyMap[0x42] = KEY_KEY_B;
    _keyMap[0x43] = KEY_KEY_C;
    _keyMap[0x44] = KEY_KEY_D;
    _keyMap[0x45] = KEY_KEY_E;
    _keyMap[0x46] = KEY_KEY_F;
    _keyMap[0x47] = KEY_KEY_G;
    _keyMap[0x48] = KEY_KEY_H;
    _keyMap[0x49] = KEY_KEY_I;
    _keyMap[0x4A] = KEY_KEY_J;
    _keyMap[0x4B] = KEY_KEY_K;
    _keyMap[0x4C] = KEY_KEY_L;
    _keyMap[0x4D] = KEY_KEY_M;
    _keyMap[0x4E] = KEY_KEY_N;
    _keyMap[0x4F] = KEY_KEY_O;
    _keyMap[0x50] = KEY_KEY_P;
    _keyMap[0x51] = KEY_KEY_Q;
    _keyMap[0x52] = KEY_KEY_R;
    _keyMap[0x53] = KEY_KEY_S;
    _keyMap[0x54] = KEY_KEY_T;
    _keyMap[0x55] = KEY_KEY_U;
    _keyMap[0x56] = KEY_KEY_V;
    _keyMap[0x57] = KEY_KEY_W;
    _keyMap[0x58] = KEY_KEY_X;
    _keyMap[0x59] = KEY_KEY_Y;
    _keyMap[0x5A] = KEY_KEY_Z;

    _keyMap[VK_LWIN] = KEY_LWIN;
    _keyMap[VK_RWIN] = KEY_RWIN;

    _keyMap[VK_MULTIPLY] = KEY_MULTIPLY;
    _keyMap[VK_ADD] = KEY_ADD;
    _keyMap[VK_SEPARATOR] = KEY_SEPARATOR;
    _keyMap[VK_SUBTRACT] = KEY_SUBTRACT;
    _keyMap[VK_DECIMAL] = KEY_DECIMAL;
    _keyMap[VK_DIVIDE] = KEY_DIVIDE;

    _keyMap[VK_F1] = KEY_F1;
    _keyMap[VK_F2] = KEY_F2;
    _keyMap[VK_F3] = KEY_F3;
    _keyMap[VK_F4] = KEY_F4;
    _keyMap[VK_F5] = KEY_F5;
    _keyMap[VK_F6] = KEY_F6;
    _keyMap[VK_F7] = KEY_F7;
    _keyMap[VK_F8] = KEY_F8;
    _keyMap[VK_F9] = KEY_F9;
    _keyMap[VK_F10] = KEY_F10;
    _keyMap[VK_F11] = KEY_F11;
    _keyMap[VK_F12] = KEY_F12;

    _keyMap[VK_SCROLL] = KEY_SCROLL;

    _keyMap[VK_LSHIFT] = KEY_LSHIFT;
    _keyMap[VK_RSHIFT] = KEY_RSHIFT;
    _keyMap[VK_LCONTROL] = KEY_LCONTROL;
    _keyMap[VK_RCONTROL] = KEY_RCONTROL;
    _keyMap[VK_MENU] = KEY_LALT;

    _keyMap[VK_OEM_PLUS] = KEY_PLUS;
    _keyMap[VK_OEM_COMMA] = KEY_COMMA;
    _keyMap[VK_OEM_MINUS] = KEY_MINUS;
    _keyMap[VK_OEM_PERIOD] = KEY_PERIOD;
}

/** wndProc ******************************************************************/
LRESULT CALLBACK WindowWin::wndProc(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    Event levent;
    bool newEvent = false;

	switch (uMsg)
	{
        case WM_SYSCOMMAND:
        {
            switch (wParam)
            {
                case SC_SCREENSAVE:   // screensaver trying to start?
                case SC_MONITORPOWER: // monitor trying to enter powersave?
                {
                    return 0;  // prevent from happening
                }

                case SC_MAXIMIZE:
                case SC_SIZE:
                {
                    // TODO
                    levent.type = EV_RESIZE;
                    newEvent = true;
                    break;
                }
            }
            break;
        }

        case WM_QUIT:
        case WM_CLOSE:
        {
            levent.type = EV_QUIT;
            newEvent = true;
            break;
        }

		case WM_KEYDOWN:
        case WM_KEYUP:
		{
            levent.evKey.type = EV_KEYBOARD;
            levent.evKey.asciiKey = (char)wParam;
            levent.evKey.key = _keyMap[wParam];

            if(levent.evKey.key != 0)
            {
                levent.evKey.alt = (GetKeyState(VK_MENU) > 0);
                levent.evKey.control = (GetKeyState(VK_CONTROL) > 0);
                levent.evKey.shift = (GetKeyState(VK_SHIFT) > 0);
                levent.evKey.pressed = (uMsg == WM_KEYDOWN);

                eventQueue.push(levent);
                newEvent = true;
            }
            break;
		}

        case WM_LBUTTONDOWN:
        case WM_LBUTTONUP:
        {
            levent.evMouse.type = EV_MOUSE;
            levent.evMouse.pressed = (uMsg == WM_LBUTTONDOWN);
            levent.evMouse.x = GET_X_LPARAM(lParam);
            levent.evMouse.y = GET_Y_LPARAM(lParam);
            levent.evMouse.button = BTN_LEFT;
            newEvent = true;
            break;
        }

        case WM_MBUTTONDOWN:
        case WM_MBUTTONUP:
        {
            levent.evMouse.type = EV_MOUSE;
            levent.evMouse.pressed = (uMsg == WM_MBUTTONDOWN);
            levent.evMouse.x = GET_X_LPARAM(lParam);
            levent.evMouse.y = GET_Y_LPARAM(lParam);
            levent.evMouse.button = BTN_MIDDLE;
            newEvent = true;
            break;
        }

        case WM_RBUTTONDOWN:
        case WM_RBUTTONUP:
        {
            levent.evMouse.type = EV_MOUSE;
            levent.evMouse.pressed = (uMsg == WM_RBUTTONDOWN);
            levent.evMouse.x = GET_X_LPARAM(lParam);
            levent.evMouse.y = GET_Y_LPARAM(lParam);
            levent.evMouse.button = BTN_RIGHT;
            newEvent = true;
            break;
        }

        case WM_MOUSEWHEEL:
        {
            // TODO
        }

        case WM_SIZE:
        {
            levent.type = EV_RESIZE;
            newEvent = true;
            break;
        }
    }

    if(newEvent)
    {
        eventQueue.push(levent);
        return 0;
    }

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

/** wndProcRouter*************************************************************/
LRESULT CALLBACK WindowWin::wndProcRouter(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam)
{
    LONG_PTR userData = GetWindowLongPtr(hWnd, GWLP_USERDATA);
    if(userData)
        return ((WindowWin*)userData)->wndProc(hWnd, uMsg, wParam, lParam);

    switch(uMsg)
    {
        case WM_CREATE:
        {
            // get the pointer to the window
            LPCREATESTRUCT createStruct = (LPCREATESTRUCT)lParam;
            SetWindowLongPtr(hWnd, GWLP_USERDATA, (LONG)createStruct->lpCreateParams );
        }
    }

    return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

/** create *******************************************************************/
bool WindowWin::create()
{
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
    WindowRect.left=(long)0;            // Set Left Value To 0
    WindowRect.right=(long)_width;      // Set Right Value To Requested Width
    WindowRect.top=(long)0;             // Set Top Value To 0
    WindowRect.bottom=(long)_height;    // Set Bottom Value To Requested Height

	this->fullscreen=fullscreen;			// Set The Global Fullscreen Flag

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= wndProcRouter;				// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;											// Return false
	}

	if (_fullscreen)												// Attempt Fullscreen Mode?
	{
		DEVMODE dmScreenSettings;								// Device Mode
		memset(&dmScreenSettings,0,sizeof(dmScreenSettings));	// Makes Sure Memory's Cleared
		dmScreenSettings.dmSize=sizeof(dmScreenSettings);		// Size Of The Devmode Structure
        dmScreenSettings.dmPelsWidth = _width;        // Selected Screen Width
        dmScreenSettings.dmPelsHeight = _height;      // Selected Screen Height
        dmScreenSettings.dmBitsPerPel = _colorbits;    // Selected Bits Per Pixel
		dmScreenSettings.dmFields=DM_BITSPERPEL|DM_PELSWIDTH|DM_PELSHEIGHT;

		// Try To Set Selected Mode And Get Results.  NOTE: CDS_FULLSCREEN Gets Rid Of Start Bar.
		if (ChangeDisplaySettings(&dmScreenSettings,CDS_FULLSCREEN)!=DISP_CHANGE_SUCCESSFUL)
		{
			// If The Mode Fails, Offer Two Options.  Quit Or Use Windowed Mode.
			if (MessageBox(NULL,"The Requested Fullscreen Mode Is Not Supported By\nYour Video Card. Use Windowed Mode Instead?","NeHe GL",MB_YESNO|MB_ICONEXCLAMATION)==IDYES)
			{
				_fullscreen=false;		// Windowed Mode Selected.  Fullscreen = false
			}
			else
			{
				// Pop Up A Message Box Letting User Know The Program Is Closing.
				MessageBox(NULL,"Program Will Now Close.","ERROR",MB_OK|MB_ICONSTOP);
				return false;									// Return false
			}
		}
	}

	if (_fullscreen)												// Are We Still In Fullscreen Mode?
	{
		dwExStyle=WS_EX_APPWINDOW;								// Window Extended Style
		dwStyle=WS_POPUP;										// Windows Style
		ShowCursor( false );										// Hide Mouse Pointer
	}
	else
	{
		dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
		dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	}

	AdjustWindowRectEx(&WindowRect, dwStyle, false, dwExStyle);		// Adjust Window To True Requested Size

    // Create The Window
    if (!(hWnd=CreateWindowEx(dwExStyle, "OpenGL", _title.c_str(), dwStyle | WS_CLIPSIBLINGS | WS_CLIPCHILDREN, 0, 0, WindowRect.right-WindowRect.left, WindowRect.bottom-WindowRect.top, NULL, NULL, hInstance, this)))
    {
        kill();    // Reset The Display
        MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
        return false;
    }

    hDC = GetDC(hWnd);

	return true;
}

/** kill *********************************************************************/
bool WindowWin::kill()								// Properly Kill The Window
{
	if (_fullscreen)										// Are We In Fullscreen Mode?
	{
		ChangeDisplaySettings(NULL,0);					// If So Switch Back To The Desktop
		ShowCursor(true);								// Show Mouse Pointer
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}

	return true;
}

/** show *********************************************************************/
void WindowWin::show()
{
	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
}

/** show *********************************************************************/
void WindowWin::refresh()
{
	//TODO
}

/** setTitle *****************************************************************/
bool WindowWin::setTitle(const std::string& title)
{
	//TODO
	return true;
}

/** eventQueued **************************************************************/
bool WindowWin::eventQueued()
{
    MSG msg;

    if( !PeekMessage(&msg,NULL,0,0,PM_REMOVE) )
        return false;

    TranslateMessage(&msg);
    DispatchMessage(&msg);

	return true;
}

/** nextEvent ****************************************************************/
bool WindowWin::nextEvent(Event *ev)
{
    if(eventQueue.size() == 0)
        return false;

    *ev = eventQueue.front();
    eventQueue.pop();

    return true;
}

}
