/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
/* #include <unistd.h> */
#include <fcntl.h>
#include <string>
#include <string.h>
#include <iostream>
#include <png.h>

#include "TurbineTextureLoader.h"

using std::string;

namespace Turbine
{

//#define DEBUG 1

TextureLoader* TextureLoader::getInstance()
{
    if(tloader == 0)
        tloader = new TextureLoader();

    return tloader;
}

TextureLoader* TextureLoader::tloader = 0;

TextureLoader::TextureLoader()
{
    // the index 0 is reserved generally (like OpenGL)
    for(int i=MAX_TEXTURES; i>0; i--)
        indexes.push(i);
}

/* Load a bmp file to Image memory struct. Assume depth of image 24 bit.
 * The Image struct is useful for opengl. On error return NULL */
/* TODO I/O must be rewritten for cross-platform (es. fopen)
/*Image* TextureLoader::loadBMP(const string& filename)
{
  int fd;

  bmp_header header;
  bmp_info info;

  int i;
  char temp;

  // open file
  fd = open(filename.c_str(), O_RDONLY);
  if (fd < 0)
  {
    fprintf(stderr, "Error while try opening file %s\n", filename.c_str());
    return NULL;
  }

  // read bitmap header separately due to dealign problem on short
  if (read(fd, &header.type, sizeof(header.type)) != (sizeof(header.type)))
  {
    fprintf(stderr, "Error: %s bitmap file corrupted\n", filename.c_str());
    close(fd);
    return NULL;
  }
  if (read(fd, &header.size, sizeof(header.size)) != (sizeof(header.size)))
  {
    fprintf(stderr, "Error: %s bitmap file corrupted\n", filename.c_str());
    close(fd);
    return NULL;
  }
  if (read(fd, &header.reserved1, sizeof(header.reserved1)) !=
      (sizeof(header.reserved1)))
  {
    fprintf(stderr, "Error: %s bitmap file corrupted\n", filename.c_str());
    close(fd);
    return NULL;
  }
  if (read(fd, &header.reserved2, sizeof(header.reserved2)) !=
      (sizeof(header.reserved2)))
  {
    fprintf(stderr, "Error: %s bitmap file corrupted\n", filename.c_str());
    close(fd);
    return NULL;
  }
  if (read(fd, &header.offset, sizeof(header.offset)) !=
      (sizeof(header.offset)))
  {
    fprintf(stderr, "Error: %s bitmap file corrupted\n", filename.c_str());
    close(fd);
    return NULL;
  }

  // header must be "BM"
  if (header.type != 0x4d42)
  {
    fprintf(stderr, "Error: %s is not a valid bitmap\n", filename.c_str());
    close(fd);
    return NULL;
  }

#ifdef DEBUG
  printf("\n%s HEADER\n", filename.c_str());
  printf("type = %x\n", header.type);
  printf("size = %u\n", header.size);
  printf("offset = %u\n", header.offset);
  fflush(stdout);
#endif

  // the structure bmp_header and the read, think in multiple of 4, then sizeof(bmp_header) is 16 and
  // the read before after reading point to 16 byte of file. Therefore we must lseek to 14.
  if (lseek(fd, 14, SEEK_SET) == -1)
  {
    fprintf(stderr, "Error while seeking %s\n", filename.c_str());
    close(fd);
    return NULL;
  }

  // read bitmap info struct size
  if ((read(fd, &info.size, sizeof(info.size)) != sizeof(info.size)))
  {
    fprintf(stderr, "Error: %s bad bitmap type\n", filename.c_str());
    close(fd);
    return NULL;
  }
  if (info.size != sizeof(info))
  {
    fprintf(stderr, "Error: %s bad bitmap type\n", filename.c_str());
    close(fd);
    return NULL;
  }

  // same problem above sizeof info.size isn't multiple of 4
  if (lseek(fd, 14, SEEK_SET) == -1)
  {
    fprintf(stderr, "Error while seeking %s\n", filename.c_str());
    close(fd);
    return NULL;
  }

  // now we read the entire info struct
  if ((read(fd, &info, sizeof(info)) != sizeof(info)))
  {
    fprintf(stderr, "Error: %s bitmap file corrupted\n", filename.c_str());
    close(fd);
    return NULL;
  }

#ifdef DEBUG
  printf("\n%s INFO\n", filename.c_str());
  printf("header size = %d\n", info.size);
  printf("width = %d\n", info.width);
  printf("height = %d\n", info.height);
  printf("planes = %hu\n", info.planes);
  printf("bits per pixel = %hu\n", info.bits);
  printf("compression = %u\n", info.compression);
  printf("image size = %u\n", info.imagesize);
  printf("x pixel per meter = %d\n", info.xresolution);
  printf("y pixel per meter = %d\n", info.yresolution);
  printf("number of colours = %u\n", info.ncolours);
  printf("important colours = %u\n", info.importantcolours);
  fflush(stdout);
#endif

  // assume to be error load compress bitmap or load not 24 bit per pixel bitmap
  if ((info.compression != 0) || (info.bits != 24))
  {
    fprintf(stderr, "Error: %s bitmap is compressed or depth isn't 24\n",
	    filename.c_str());
    close(fd);
    return NULL;
  }

  Image* image = new Image();
  image->width = info.width;
  image->height = info.height;

  // malloc data

  int size = image->width * image->height * 3;

  image->data = (unsigned char*) malloc (size);
  if (image->data < 0)
  {
    fprintf(stderr, "Malloc error while loading %s image", filename.c_str());
    close(fd);
    return NULL;
  }

  if (lseek(fd, header.offset, SEEK_SET) == -1)
  {
    fprintf(stderr, "Error while seeking %s\n", filename.c_str());
    close(fd);
    return NULL;
  }

  // read bitmap info struct size
  if (read(fd, image->data, size ) != size )
  {
    fprintf(stderr, "Error: %s bad bitmap type\n", filename.c_str());
    close(fd);
    return NULL;
  }

  // reverse bgr in rgb
  for ( i=0; i<size; i+=3 )
  {
    temp = image->data[i];
    image->data[i] = image->data[i+2];
    image->data[i+2] = temp;
  }

  close(fd);
  
    image->index = getFreeIndex();
  image->type = RGB_24BIT_U;
  
  return image;
} */

/* Load a png file to Image memory struct. Assume depth of image 24 bit.
 * On error return NULL */
Image* TextureLoader::loadPNG(const string& filename)
{
    // open file
    FILE* infile = fopen(filename.c_str(), "r");
    if (infile == NULL)
    {
        std::cerr << "Error while try opening file " << filename.c_str() << std::endl;
        return NULL;
    }
#ifdef DEBUG_
    std::cout << "Loading " << filename.c_str() << std::endl;
#endif

    // check png signature
    unsigned char sig[8];
    fread(sig, 1, 8, infile);
    if (!png_check_sig(sig, 8))
    {
        std::cerr << "Not valid PNG signature" << std::endl;
        fclose(infile);
        return NULL;
    }

    png_structp png_ptr = NULL;
    png_infop info_ptr = NULL;

    // could pass pointers to user-defined error handlers instead of NULLs:
    png_ptr = png_create_read_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);
    if (!png_ptr)
    {
        std::cerr << "Out of memory" << std::endl;
        fclose(infile);
        return NULL;
    }

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
    {
        std::cerr << "Out of memory" << std::endl;
        png_destroy_read_struct(&png_ptr, NULL, NULL);
        fclose(infile);
        return NULL;
    }

    // setjmp() must be called in every function that calls a PNG-reading libpng function
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(infile);
        return NULL;
    }

    png_init_io(png_ptr, infile);
    png_set_sig_bytes(png_ptr, 8);  // we already read the 8 signature bytes

    png_read_info(png_ptr, info_ptr);  // read all PNG info up to image data

    unsigned int width, height;
    int bit_depth, color_type;
    png_get_IHDR(png_ptr, info_ptr, &width, &height, &bit_depth, &color_type, NULL, NULL, NULL);

#ifdef DEBUG
    std::cout << "Width " << width << std::endl;
    std::cout << "Height " << height << std::endl;
    std::cout << "Bit depth " << bit_depth  << std::endl;
    std::cout << "Color type " << color_type << std::endl;
#endif

    // setjmp() must be called in every function that calls a PNG-reading libpng function
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(infile);
        return NULL;
    }

    /* expand palette images to RGB, low-bit-depth grayscale images to 8 bits,
     * transparency chunks to full alpha channel; strip 16-bit-per-sample
     * images to 8 bits per sample; and convert grayscale to RGB[A] */

    if (color_type == PNG_COLOR_TYPE_PALETTE)
        png_set_expand(png_ptr);
    if (color_type == PNG_COLOR_TYPE_GRAY && bit_depth < 8)
        png_set_expand(png_ptr);
    if (png_get_valid(png_ptr, info_ptr, PNG_INFO_tRNS))
        png_set_expand(png_ptr);
    if (bit_depth == 16)
        png_set_strip_16(png_ptr);
    if (color_type == PNG_COLOR_TYPE_GRAY || color_type == PNG_COLOR_TYPE_GRAY_ALPHA)
        png_set_gray_to_rgb(png_ptr);

    // apply gamma correction if set up
    double gamma;
    float display_exponent = 1.0 * 2.2; // LUT * CRT exponents for common PC
    if (png_get_gAMA(png_ptr, info_ptr, &gamma))
    {
#ifdef DEBUG
        std::cout << "Gamma correction set up " << gamma << std::endl;
#endif
        png_set_gamma(png_ptr, display_exponent, gamma);
    }

    // all transformations have been registered; now update info_ptr data, get rowbytes and channels
    png_read_update_info(png_ptr, info_ptr);

    int rowbytes = png_get_rowbytes(png_ptr, info_ptr);

#ifdef DEBUG
    std::cout << "Rowbytes " << rowbytes << std::endl;
    int channels = png_get_channels(png_ptr, info_ptr);
    std::cout << "Channels " << channels << std::endl;
#endif

    // allocate Image struct and its data
    Image* image = new Image();
    image->width = width;
    image->height = height;
    if(color_type == PNG_COLOR_TYPE_RGB)
        image->type = RGB_24BIT_U;
    else
        image->type = RGBA_24BIT_U;

    int size = rowbytes * height;
    image->data = (unsigned char*) malloc(size);
    if(image->data == NULL)
    {
        std::cerr << "Malloc error" << std::endl;
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        fclose(infile);
        return NULL;
    }

    // setup row pointers and read the whole image
    png_bytepp  row_pointers = NULL;
    if ((row_pointers = (png_bytepp)malloc(height*sizeof(png_bytep))) == NULL)
    {
        png_destroy_read_struct(&png_ptr, &info_ptr, NULL);
        free(image->data);
        delete image;
        return NULL;
    }
    for (unsigned int i=0;  i<height; i++)
        row_pointers[height-1-i] = image->data + i*rowbytes;

    png_read_image(png_ptr, row_pointers);

    // all data correctly loaded, closing file, clean up and return the image
    free(row_pointers);
    png_read_end(png_ptr, NULL);
    fclose(infile);

    // setup a new free index for the image
    image->index = getFreeIndex();

    return image;
}

void TextureLoader::writePNG(Image* image, const std::string& filename)
{
    std::cout << "writing " << filename << std::endl;
    png_structp png_ptr;
    png_infop info_ptr;
    png_byte bit_depth = 8;
    int color_type;
    if(image->type == RGB_24BIT_U)
        color_type = PNG_COLOR_TYPE_RGB;
    else
        color_type = PNG_COLOR_TYPE_RGBA;
    png_bytep * row_pointers;

    // create file
    FILE *fp = fopen(filename.c_str(), "wb");
    if (!fp)
        std::cerr << "File " << filename << " could not be opened for writing" << std::endl;

    // initialize stuff
    png_ptr = png_create_write_struct(PNG_LIBPNG_VER_STRING, NULL, NULL, NULL);

    if (!png_ptr)
        std::cerr << "png_create_write_struct failed" << std::endl;

    info_ptr = png_create_info_struct(png_ptr);
    if (!info_ptr)
        std::cerr << "png_create_info_struct failed" << std::endl;

    if (setjmp(png_jmpbuf(png_ptr)))
        std::cerr << "Error during init_io" << std::endl;

    png_init_io(png_ptr, fp);

    // write header
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        std::cerr << "Error during writing header" << std::endl;
        return;
    }

    png_set_IHDR(png_ptr, info_ptr, image->width, image->height,
                    bit_depth, color_type, PNG_INTERLACE_NONE,
                    PNG_COMPRESSION_TYPE_BASE, PNG_FILTER_TYPE_BASE);

    png_write_info(png_ptr, info_ptr);

    // write bytes
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        std::cerr << "Error during writing bytes" << std::endl;
        return;
    }

    if ((row_pointers = (png_bytepp)malloc(image->height*sizeof(png_bytep))) == NULL)
    {
        std::cerr  << "Error during row_pointers allocation" << std::endl;
        return;
    }

    int rowbytes = image->width * 4;
    for (unsigned int i=0;  i<image->height; i++)
    {
        row_pointers[image->height-1-i] = image->data + i*rowbytes;
    }

    png_write_image(png_ptr, row_pointers);

    // end write
    if (setjmp(png_jmpbuf(png_ptr)))
    {
        std::cerr << "Error during end of write" << std::endl;
        return;
    }

    png_write_end(png_ptr, NULL);

    // cleanup heap allocation
    free(row_pointers);

    fclose(fp);
}



}
