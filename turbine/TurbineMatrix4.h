/* This file is a part of the Turbine game engine.
  This is a modified version of matrix4.h Irrlicht source.

  Copyright (C) 2002-2010 Nikolaus Gebhardt
  Copyright (C) 2012 Andrea Zoli

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef MATRIX4_H
#define MATRIX4_H

#include <string>
#include <iostream>
#include <iomanip>
#include <string.h>

#include "TurbineMath.h"
#include "TurbineVector3d.h"
#include "TurbineVector4d.h"

namespace Turbine
{

class Matrix4
{

public:
/** Constructor/Destructor ***************************************************/
    Matrix4()
    {
        makeIdentity();
    }

    Matrix4(const Vector3d& position, const Vector3d& rotation, Vector3d scale)
    {
        setTranslation(position);
        setRotationDegrees(rotation);
        setScale(scale);
    }

    ~Matrix4(){}

	float& operator[](unsigned int index) { return M[index]; }
	const float& operator[](const unsigned int index) const { return M[index]; }

/** Operator *****************************************************************/
    float& operator()(const int row, const int col) { return M[col*4 + row]; }
    const float& operator()(const int row, const int col) const { return M[col*4 + row]; }

	inline Matrix4& operator=(const Matrix4& mat);
	inline Matrix4& operator=(const float& scalar);

	inline bool operator==(const Matrix4& mat) const;
	inline bool operator!=(const Matrix4& mat) const;

	inline Matrix4 operator+(const Matrix4& mat) const;
	inline Matrix4& operator+=(const Matrix4& mat);

	inline Matrix4 operator-(const Matrix4& mat) const;
	inline Matrix4& operator-=(const Matrix4& mat);

	inline Matrix4 operator*(const Matrix4& mat) const;
	inline Matrix4& operator*=(const Matrix4& mat);

	inline Matrix4 operator*(const float& scalar) const;
	inline Matrix4& operator*=(const float& scalar);

/** Identity *****************************************************************/
	inline void makeIdentity();

	inline bool isIdentity() const;

/** Transformation ***********************************************************/
    inline void setTranslation( const Vector3d& translation );
    inline Vector3d getTranslation() const;
    inline void setRotationDegrees( const Vector3d& rotation );
    inline void setRotationRadians( const Vector3d& rotation );
    void setScale( const Vector3d& scale );

/** View/projection matrices *************************************************/
    inline void buildViewMatrix(const Vector3d& position, const Vector3d& target, const Vector3d& upVector);
    inline void buildPerspectiveMatrix(double fov, double aspectRatio, double zNear, double zFar);
    inline void buildOrthogonalMatrix(double left, double right, double top, double bottom, double near, double far);

/** Print ********************************************************************/
    friend std::ostream& operator<<(std::ostream& os, const Matrix4& mat)
    {
        os.precision(3);
        os.setf(std::ios::right | std::ios::showpoint);
        os << "Matrix{ ";

        for(int i = 0; i < 4; i++)
        {
            for(int j = 0; j < 4; j++)
            {
                if(i > 0 && j == 0)
                    os << std::fixed << std::setw(16) << mat[i*4+j];
                else
                    os << std::fixed << std::setw(8) << mat[i*4+j];
            }
            if(i==3)
                os << " }";
            os << std::endl;
        }

        return os;
    }

private:

	float M[16];
};

Matrix4& Matrix4::operator=(const Matrix4& mat)
{
	if (this==&mat)
		return *this;

	memcpy(M, mat.M, 16*sizeof(float));

	return *this;
}

Matrix4& Matrix4::operator=(const float& scalar)
{
	for (int i=0; i < 16; ++i)
		M[i]=scalar;

	return *this;
}

bool Matrix4::operator==(const Matrix4& mat) const
{
	for (int i=0; i < 16; ++i)
		if (M[i] != mat.M[i])
			return false;

	return true;
}

bool Matrix4::operator!=(const Matrix4& mat) const
{
	return !(*this == mat);
}

Matrix4 Matrix4::operator+(const Matrix4& mat) const
{
	Matrix4 temp;

	temp[0] = M[0]+mat[0];
	temp[1] = M[1]+mat[1];
	temp[2] = M[2]+mat[2];
	temp[3] = M[3]+mat[3];
	temp[4] = M[4]+mat[4];
	temp[5] = M[5]+mat[5];
	temp[6] = M[6]+mat[6];
	temp[7] = M[7]+mat[7];
	temp[8] = M[8]+mat[8];
	temp[9] = M[9]+mat[9];
	temp[10] = M[10]+mat[10];
	temp[11] = M[11]+mat[11];
	temp[12] = M[12]+mat[12];
	temp[13] = M[13]+mat[13];
	temp[14] = M[14]+mat[14];
	temp[15] = M[15]+mat[15];

	return temp;
}

inline Matrix4& Matrix4::operator+=(const Matrix4& mat)
{
	M[0]+=mat[0];
	M[1]+=mat[1];
	M[2]+=mat[2];
	M[3]+=mat[3];
	M[4]+=mat[4];
	M[5]+=mat[5];
	M[6]+=mat[6];
	M[7]+=mat[7];
	M[8]+=mat[8];
	M[9]+=mat[9];
	M[10]+=mat[10];
	M[11]+=mat[11];
	M[12]+=mat[12];
	M[13]+=mat[13];
	M[14]+=mat[14];
	M[15]+=mat[15];

	return *this;
}

inline Matrix4 Matrix4::operator-(const Matrix4& mat) const
{
	Matrix4 temp;

	temp[0] = M[0]-mat[0];
	temp[1] = M[1]-mat[1];
	temp[2] = M[2]-mat[2];
	temp[3] = M[3]-mat[3];
	temp[4] = M[4]-mat[4];
	temp[5] = M[5]-mat[5];
	temp[6] = M[6]-mat[6];
	temp[7] = M[7]-mat[7];
	temp[8] = M[8]-mat[8];
	temp[9] = M[9]-mat[9];
	temp[10] = M[10]-mat[10];
	temp[11] = M[11]-mat[11];
	temp[12] = M[12]-mat[12];
	temp[13] = M[13]-mat[13];
	temp[14] = M[14]-mat[14];
	temp[15] = M[15]-mat[15];

	return temp;
}

inline Matrix4& Matrix4::operator-=(const Matrix4& mat)
{
	M[0]-=mat[0];
	M[1]-=mat[1];
	M[2]-=mat[2];
	M[3]-=mat[3];
	M[4]-=mat[4];
	M[5]-=mat[5];
	M[6]-=mat[6];
	M[7]-=mat[7];
	M[8]-=mat[8];
	M[9]-=mat[9];
	M[10]-=mat[10];
	M[11]-=mat[11];
	M[12]-=mat[12];
	M[13]-=mat[13];
	M[14]-=mat[14];
	M[15]-=mat[15];

	return *this;
}

inline Matrix4& Matrix4::operator*=(const Matrix4& mat)
{
    Matrix4 curr = *this;
    *this = curr * mat;

    return *this;
}

inline Matrix4 Matrix4::operator*(const Matrix4& m2) const
{
    Matrix4 temp;

    temp[0] = M[0]*m2[0] + M[1]*m2[4] + M[2]*m2[8] + M[3]*m2[12];
    temp[1] = M[0]*m2[1] + M[1]*m2[5] + M[2]*m2[9] + M[3]*m2[13];
    temp[2] = M[0]*m2[2] + M[1]*m2[6] + M[2]*m2[10] + M[3]*m2[14];
    temp[3] = M[0]*m2[3] + M[1]*m2[7] + M[2]*m2[11] + M[3]*m2[15];

    temp[4] = M[4]*m2[0] + M[5]*m2[4] + M[6]*m2[8] + M[7]*m2[12];
    temp[5] = M[4]*m2[1] + M[5]*m2[5] + M[6]*m2[9] + M[7]*m2[13];
    temp[6] = M[4]*m2[2] + M[5]*m2[6] + M[6]*m2[10] + M[7]*m2[14];
    temp[7] = M[4]*m2[3] + M[5]*m2[7] + M[6]*m2[11] + M[7]*m2[15];

    temp[8] = M[8]*m2[0] + M[9]*m2[4] + M[10]*m2[8] + M[11]*m2[12];
    temp[9] = M[8]*m2[1] + M[9]*m2[5] + M[10]*m2[9] + M[11]*m2[13];
    temp[10] = M[8]*m2[2] + M[9]*m2[6] + M[10]*m2[10] + M[11]*m2[14];
    temp[11] = M[8]*m2[3] + M[9]*m2[7] + M[10]*m2[11] + M[11]*m2[15];

    temp[12] = M[12]*m2[0] + M[13]*m2[4] + M[14]*m2[8] + M[15]*m2[12];
    temp[13] = M[12]*m2[1] + M[13]*m2[5] + M[14]*m2[9] + M[15]*m2[13];
    temp[14] = M[12]*m2[2] + M[13]*m2[6] + M[14]*m2[10] + M[15]*m2[14];
    temp[15] = M[12]*m2[3] + M[13]*m2[7] + M[14]*m2[11] + M[15]*m2[15];

    return temp;
}

inline Matrix4 Matrix4::operator*(const float& scalar) const
{
	Matrix4 temp;

	temp[0] = M[0]*scalar;
	temp[1] = M[1]*scalar;
	temp[2] = M[2]*scalar;
	temp[3] = M[3]*scalar;
	temp[4] = M[4]*scalar;
	temp[5] = M[5]*scalar;
	temp[6] = M[6]*scalar;
	temp[7] = M[7]*scalar;
	temp[8] = M[8]*scalar;
	temp[9] = M[9]*scalar;
	temp[10] = M[10]*scalar;
	temp[11] = M[11]*scalar;
	temp[12] = M[12]*scalar;
	temp[13] = M[13]*scalar;
	temp[14] = M[14]*scalar;
	temp[15] = M[15]*scalar;

	return temp;
}

inline Matrix4& Matrix4::operator*=(const float& scalar)
{
	M[0]*=scalar;
	M[1]*=scalar;
	M[2]*=scalar;
	M[3]*=scalar;
	M[4]*=scalar;
	M[5]*=scalar;
	M[6]*=scalar;
	M[7]*=scalar;
	M[8]*=scalar;
	M[9]*=scalar;
	M[10]*=scalar;
	M[11]*=scalar;
	M[12]*=scalar;
	M[13]*=scalar;
	M[14]*=scalar;
	M[15]*=scalar;

	return *this;
}

inline void Matrix4::makeIdentity()
{
    memset(M, 0, 16*sizeof(float));
    M[0] = M[5] = M[10] = M[15] = 1.0;
}

inline bool Matrix4::isIdentity() const
{
    for(int i=0; i<4; i++)
        for (int j=0; j<4; j++)
            if (i != j && (*this)(i,j) != 0)
                return false;
    return true;
}

inline void Matrix4::setTranslation( const Vector3d& translation )
{
	M[12] = translation.x;
	M[13] = translation.y;
	M[14] = translation.z;
}

inline Vector3d Matrix4::getTranslation() const
{
	return Vector3d(M[12], M[13], M[14]);
}

inline void Matrix4::setRotationDegrees( const Vector3d& rotation )
{
	Vector3d r;
	
	r.x = degToRad(rotation.x);
	r.y = degToRad(rotation.y);
	r.z = degToRad(rotation.z);
	
	setRotationRadians(r);
}

inline void Matrix4::setRotationRadians( const Vector3d& rotation )
{
	double cr = cos( rotation.x );
	double sr = sin( rotation.x );
	double cp = cos( rotation.y );
	double sp = sin( rotation.y );
	double cy = cos( rotation.z );
	double sy = sin( rotation.z );

	M[0] = (float)( cp*cy );
	M[1] = (float)( cp*sy );
	M[2] = (float)( -sp );

	double srsp = sr*sp;
	double crsp = cr*sp;

	M[4] = (float)( srsp*cy-cr*sy );
	M[5] = (float)( srsp*sy+cr*cy );
	M[6] = (float)( sr*cp );

	M[8] = (float)( crsp*cy+sr*sy );
	M[9] = (float)( crsp*sy-sr*cy );
	M[10] = (float)( cr*cp );
}

inline void Matrix4::setScale( const Vector3d& scale )
{
	M[0] = scale.x;
	M[5] = scale.y;
	M[10] = scale.z;
}

void Matrix4::buildViewMatrix(const Vector3d& position, const Vector3d& target, const Vector3d& upVector)
{
    Vector3d forward = position-target;
    forward.normalize();

    Vector3d left = upVector.crossProduct(forward);
    left.normalize();

    Vector3d up = forward.crossProduct(left);

    M[0] = left.x;
    M[4] = left.y;
    M[8] = left.z;
    M[12] = -left.dotProduct(position);

    M[1] = up.x;
    M[5] = up.y;
    M[9] = up.z;
    M[13] = -up.dotProduct(position);

    M[2] = forward.x;
    M[6] = forward.y;
    M[10] = forward.z;
    M[14] = -forward.dotProduct(position);

    M[3] = 0;
    M[7] = 0;
    M[11] = 0;
    M[15] = 1;
}

void Matrix4::buildPerspectiveMatrix(double fov, double aspectRatio, double zNear, double zFar)
{
    double f = 1.0/tan(fov/2.0);

    M[0] = f / aspectRatio;
    M[1] = 0;
    M[2] = 0;
    M[3] = 0;

    M[4] = 0;
    M[5] = f;
    M[6] = 0;
    M[7] = 0;

    M[8] = 0;
    M[9] = 0;
    M[10] = zFar / (zNear-zFar);
    M[11] = -1;

    M[12] = 0;
    M[13] = 0;
    M[14] = (zFar*zNear) / (zNear-zFar);
    M[15] = 0;
}

void Matrix4::buildOrthogonalMatrix(double left, double right, double top, double bottom, double nnear, double ffar)
{
    M[0] = 2.0 / (right-left);
    M[1] = 0;
    M[2] = 0;
    M[3] = -(right+left)/(right-left);

    M[4] = 0;
    M[5] = 2.0 / (top-bottom);
    M[6] = 0;
    M[7] = -(top+bottom)/(top-bottom);

    M[8] = 0;
    M[9] = 0;
    M[10] = -2.0/(ffar-nnear);
    M[11] = -(ffar+nnear)/(ffar-nnear);

    M[12] = 0;
    M[13] = 0;
    M[14] = 0;
    M[15] = 1;
}

// this is a non-member function defined only here for inline cross-reference problem
inline Vector4d operator* (const Vector4d& v, const Matrix4& m)
{
    Vector4d temp;
    temp.x = v.x*m[0] + v.y*m[4] + v.z*m[8] + v.w*m[12];
    temp.y = v.x*m[1] + v.y*m[5] + v.z*m[9] + v.w*m[13];
    temp.z = v.x*m[2] + v.y*m[6] + v.z*m[10]+ v.w*m[14];
    temp.w = v.x*m[3] + v.y*m[7] + v.z*m[11]+ v.w*m[15];
    return temp;
}

}

#endif /* MATRIX4_H */
