/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TurbineBBox.h"

namespace Turbine
{

BBox::BBox(): _min(0.0, 0.0, 0.0), _max(0.0, 0.0, 0.0){}

BBox::BBox(const Vector3d& min, const Vector3d& max)
{
    _min = min;
    _max = max;
}

void BBox::init(const std::vector<Vector3d>& points)
{
    std::vector<Vector3d>::const_iterator point;
    point=points.begin();
    _min = (*point);
    _max = (*point);
    point++;
    for(; point!=points.end(); point++)
    {
        const Vector3d& p = (*point);
        if(p.x < _min.x)
            _min.x = p.x;
        if(p.y < _min.y)
            _min.y = p.y;
        if(p.z < _min.z)
            _min.z = p.z;
        if(p.x > _max.x)
            _max.x = p.x;
        if(p.y > _max.y)
            _max.y = p.y;
        if(p.z > _max.z)
            _max.z = p.z;
    }
}

std::vector<Vector3d> BBox::getCorners()
{
    std::vector<Vector3d> corners;

    corners.push_back(Vector3d(_min.x, _min.y, _max.z)); // bottom-front-left
    corners.push_back(Vector3d(_max.x, _min.y, _max.z)); // bottom-front-right
    corners.push_back(Vector3d(_max.x, _min.y, _min.z)); // bottom-back-right
    corners.push_back(Vector3d(_min.x, _min.y, _min.z)); // bottom-back-left
    corners.push_back(Vector3d(_min.x, _max.y, _max.z)); // top-front-left
    corners.push_back(Vector3d(_max.x, _max.y, _max.z)); // top-front-right
    corners.push_back(Vector3d(_max.x, _max.y, _min.z)); // top-back-right
    corners.push_back(Vector3d(_min.x, _max.y, _min.z)); // top-back-left

    return corners;
}

Vector3d BBox::getCenter()
{
    return Vector3d((_max.x+_min.x)/2.0, (_max.y+_min.y)/2.0, (_max.z+_min.z)/2.0);
}

float BBox::getHeight()
{
    return _max.y-_min.y;
}

float BBox::getWidth()
{
    return _max.x-_min.x;
}

float BBox::getDepth()
{
    return _max.z-_min.z;
}

}
