/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IWINDOW_H
#define IWINDOW_H

#include <string>
#include "TurbineIEventReceiver.h"

namespace Turbine
{

struct WindowSettings
{
	std::string title;
	int width;
	int height;
	int colorbits;
	int depthbits;
	int bitsperpixel;
	bool fullscreen;

	WindowSettings() : title(""), width(640), height(480), colorbits(32), depthbits(24), bitsperpixel(8), fullscreen(false) {}
};

class IWindow
{
protected:
    std::string _title;
    int _width;
    int _height;
    int _colorbits;
    int _depthbits;
    int _bitsperpixel;
    bool _fullscreen;

public:
    IWindow(const WindowSettings& settings)
    {
        _title = settings.title;
        _width = settings.width;
        _height = settings.height;
        _colorbits = settings.colorbits;
        _depthbits = settings.depthbits;
        _bitsperpixel = settings.bitsperpixel;
        _fullscreen = settings.fullscreen;
    }
    virtual ~IWindow(){}

    virtual WindowSettings getSettings()
    {
        WindowSettings settings;

        settings.title = _title;
        settings.width = _width;
        settings.height = _height;
        settings.colorbits = _colorbits;
        settings.depthbits = _depthbits;
        settings.bitsperpixel = _bitsperpixel;
        settings.fullscreen = _fullscreen;

        return settings;
    }

    virtual int getWidth(){ return _width; }
    virtual int getHeight(){ return _height; }
    virtual int getColorBits(){ return _colorbits; }
    virtual int getDepthBits(){ return _depthbits; }
    virtual int getBitsPerPixel(){ return _bitsperpixel; }

    virtual void toggleFullscreen() = 0;
    virtual bool isFullscreen(){ return _fullscreen; }

    virtual bool setTitle(const std::string& title) = 0;
    virtual const std::string& getTitle(){ return _title; }

    virtual bool create() = 0;
	virtual bool kill() = 0;
	virtual void show() = 0;
	virtual void hide() = 0;
	virtual void refresh() = 0;

	virtual bool eventQueued() = 0;
	virtual bool nextEvent(Event *event) = 0;
};

}

#endif /* IWINDOW_H */
