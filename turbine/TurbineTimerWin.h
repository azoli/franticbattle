#ifndef TURBINETIMERWIN_H
#define TURBINETIMERWIN_H

#include <Windows.h>
#include <WinBase.h>
#include "TurbineTimer.h"

namespace Turbine
{

class TimerWin : public Timer
{
	LARGE_INTEGER start;
	LARGE_INTEGER freq;

    public:

    TimerWin();
    ~TimerWin();

    void reset();
    float getElapsedSeconds();
};

}

#endif
