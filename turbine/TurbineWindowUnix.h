/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WINDOWUNIX_H
#define WINDOWUNIX_H

#include <map>
#include <string>
#include <X11/Xlib.h>

#include "TurbineIWindow.h"

namespace Turbine
{

/** WindowUnix ***************************************************************/
class WindowUnix : public IWindow
{
    bool _EWMHSupport;
protected:

	Display *dpy;
	Window win;
	int screen;
	Visual *visual;

	WindowSettings _settings;

	Event event;
	bool eventPresent;
	std::map<int ,Key> keyMap;

	void processXevent();
	void createKeyMap();
public:

    WindowUnix(const WindowSettings& settings) : IWindow(settings), dpy(0), win(0), screen(0), visual(0), eventPresent(0){ createKeyMap(); }
	~WindowUnix(){}

    bool create();
    void toggleFullscreen();

	bool kill();

	void show();
	void hide();
	void refresh();

	void getHandle(void *hPtr){ *((Window*)hPtr) = win; }

    bool setTitle(const std::string& title);

	Display *getDevice(){ return dpy; }
	Window getHandle(){ return win; }

	bool eventQueued();
	bool nextEvent(Event *event);
};

}

#endif /* WINDOWUNIX_H */
