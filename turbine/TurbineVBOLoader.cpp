/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>
#include <map>
#include "TurbineVBOLoader.h"
#include "TurbineTextureLoader.h"

using std::ifstream;
using std::map;
using std::string;
using std::cout;
using std::endl;

namespace Turbine
{

//#define DEBUG_VBOLOADER 1

string readString(ifstream& iss)
{
    int len;
    string retval;

    iss.read((char*)&len, sizeof(int));

    if(len != 0)
    {
        retval.resize(len);
        iss.read((char*)&retval[0], len);
    }

    return retval;
}

// Skeleton is stored in pre-order with numchilds for each bone
void loadSkeleton(ifstream& iss, map<string, int>& bones, Bone* curr, Bone* parent, int* index)
{
    curr->name = readString(iss);
    curr->index = *index;
    (*index)++;

    iss.read((char*)&curr->poseFrame, sizeof(float)*16);
    curr->frame.makeIdentity();
    iss.read((char*)&curr->offset, sizeof(float)*16);
    curr->parent = parent;
    int numchilds;
    iss.read((char*)&numchilds, sizeof(int));
    bones[curr->name] = curr->index;

#ifdef DEBUG_VBOLOADER
    cout << endl << curr->name << endl;
    cout << "index: " << curr->index << endl;
    cout << curr->poseFrame;
    cout << curr->offset;
    if(parent == NULL)
        cout << "parent node: NONE " << endl;
    else
        cout << "parent node: " << parent->name << endl;
#endif

    for(int i=0; i<numchilds; i++)
    {
        Bone* child = new Bone();
        curr->childs.push_back(child);
        loadSkeleton(iss, bones, child, curr, index);
    }
}

const string VBOLoader::fsVersion = "1.8\0";

Model* VBOLoader::load(const string& file)
{
    Model* model = new Model();

    // opening file
    ifstream iss;
    iss.open(file.c_str(), std::ios::in | std::ios::binary);
    if(!iss.is_open())
    {
#ifdef DEBUG_VBOLOADER
        std::cout << "cannot open the file " << file << std::endl;
#endif
        delete model;
        return NULL;
    }
    iss.exceptions(std::ios::failbit | std::ios::badbit | std::ios::eofbit);

    // reading header
    char magic[4];
    char version[4];

    iss.read(magic, 3);
    iss.read(version, 3);
    magic[3]=0;
    version[3]=0;

    if(fsVersion.compare(version))
    {
#ifdef DEBUG_VBOLOADER
        cout << "wrong fs version: " << file << " version is " << version << " expected " << fsVersion << endl;
#endif
        delete model;
        return NULL;
    }

    model->name = readString(iss);
    int vertCount, idxCount, materialCount, texCount, boneCount, actionCount;
    iss.read((char*)&vertCount, sizeof(int));
    if(vertCount <= 0)
    {
#ifdef DEBUG_VBOLOADER
        cout << "error while reading vertCount" << endl;
#endif

        delete model;
        return NULL;
    }
    iss.read((char*)&idxCount, sizeof(int));
    if(idxCount <= 0)
    {
#ifdef DEBUG_VBOLOADER
        cout << "error while reading idxCount" << endl;
#endif
        delete model;
        return NULL;
    }
    iss.read((char*)&materialCount, sizeof(int));
    if(materialCount <= 0)
    {
#ifdef DEBUG_VBOLOADER
        cout << "error while reading materialCount" << endl;
#endif
        delete model;
        return NULL;
    }
    iss.read((char*)&texCount, sizeof(int));
    iss.read((char*)&boneCount, sizeof(int));
    iss.read((char*)&actionCount, sizeof(int));

#ifdef DEBUG_VBOLOADER
    cout << "version: " << version << endl;
    cout << "vertices: " << vertCount << endl;
    cout << "indices: " << idxCount << endl;
    cout << "materials: " << materialCount << endl;
    cout << "textures: " << texCount << endl;
    cout << "bones: " << boneCount << endl;
    cout << "actions: " << actionCount << endl;
#endif

    // reading vertices
    for(int i=0; i<vertCount; i++)
    {
        Vertex v;

        iss.read((char*)&v.coord.x, sizeof(float)*3);
        model->vertices.push_back(v);
#ifdef DEBUG_VBOLOADER
        cout << "coord["<< i <<"]: " << v.coord << endl;
#endif

    }

    // reading normals
    for(int i=0; i<vertCount; i++)
    {
        Vertex* v = &model->vertices[i];
        iss.read((char*)&v->normal.x, sizeof(float)*3);
#ifdef DEBUG_VBOLOADER
        cout << "normal["<< i <<"]: " << v->normal << endl;
#endif
    }
    // reading texture coords
    if(texCount > 0)
    {
        for(int i=0; i<vertCount; i++)
        {
            Vertex* v = &model->vertices[i];
            iss.read((char*)&v->texCoord.x, sizeof(float)*2);

#ifdef DEBUG_VBOLOADER
        cout << "texCoord["<< i << "]: "<< v->texCoord << endl;
#endif
        }
    }

    // reading faces
    Face face;
    for(int i=0; i<idxCount; i+=3)
    {
        iss.read((char*)&face.index, sizeof(int)*3);
        model->faces.push_back(face);
#ifdef DEBUG_VBOLOADER
        cout << "index["<< i << "]: "<< face.index[0] << " " << face.index[1] << " " << face.index[2] << endl;
#endif
    }

    // reading material
    TextureLoader* tl = TextureLoader::getInstance();
    for(int i=0; i<materialCount; i++)
    {
        Material m;
        m.name = readString(iss);
        float alpha;
        iss.read((char*)&m.ambient, sizeof(float));
        iss.read((char*)&m.kd, sizeof(float)*3);
        iss.read((char*)&m.ks, sizeof(float)*3);
        iss.read((char*)&m.ns, sizeof(float));
        iss.read((char*)&alpha, sizeof(float));
        m.kd.a = alpha;
        m.ks.a = alpha;
#ifdef DEBUG_VBOLOADER
        cout << m.name << endl;
        cout << "ka: " << m.ka.r << " " << m.ka.g << " " << m.ka.b << endl;
        cout << "kd: " << m.kd.r << " " << m.kd.g << " " << m.kd.b << endl;
        cout << "ks: " << m.ks.r << " " << m.ks.g << " " << m.ks.b << endl;
#endif

        SmoothingGroup group;
        iss.read((char*)&group.start, sizeof(int));
        iss.read((char*)&group.end, sizeof(int));
        group.mtlIndex = model->materials.size();
        model->smoothGroups.push_back(group);

#ifdef DEBUG_VBOLOADER
        cout << "face index range: " << group.start << ":" << group.end << endl;
#endif

        // load the texture (from the same directory of the model)
        string texture = readString(iss);
        if(texture.compare("") != 0)
        {
            // the next line is OS dependent
            string::size_type slash = file.find_last_of('/');
            string dirname = file.substr(0, slash+1);
            m.texture = tl->loadPNG((dirname+texture).c_str());
#ifdef DEBUG_VBOLOADER
            cout << "texture: " << dirname+texture;
            if(m.texture != NULL)
                cout << " loaded" << endl;
            else
                cout << " not loaded!" << endl;
#endif
        }
        model->materials.push_back(m);
    }

    if(boneCount > 0)
    {
        // reading bone weights
        for(int i=0; i<vertCount; i++)
        {
            Vertex* v = &model->vertices[i];
            int indices[4];
            iss.read((char*)&indices[0], sizeof(int)*4);
            for(int j=0; j<4; j++)
                v->bone_indices[j] = indices[j];
            iss.read((char*)&v->influences[0], sizeof(float)*4);
#ifdef DEBUG_VBOLOADER
            cout << "[vertex " << i << "]" << endl;
            for(int j=0; j<4; j++)
                cout << v->bone_indices[j] << " ";
            cout << endl;
            for(int j=0; j<4; j++)
                cout << v->influences[j] << " ";
            cout << endl;
#endif
        }

        // loading Tbone pose skeleton
        model->skeleton = new Bone();
        map<string, int> bones;
        int index = 0;
        loadSkeleton(iss, bones, model->skeleton, NULL, &index);

        if(actionCount > 0)
        {
            // loading for each action
            for(int i=0; i<actionCount; i++)
            {
                Action action;
                action.name = readString(iss);
#ifdef DEBUG_VBOLOADER
                cout << "action " << action.name << " " << action.name.size() << endl;
#endif

                // the transformations
                int transfCount;
                iss.read((char*)&transfCount, sizeof(int));
                for(int j=0; j<transfCount; j++)
                {
                    Transformation transf;
                    string boneName = readString(iss);
                    transf.bone = bones[boneName];
                    int numkeys;
                    char type;
                    iss.read((char*)&numkeys, sizeof(int));
                    iss.read(&type, 1);
                    switch(type)
                    {
                        case 't':
                            transf.type = TR_TRANSLATION;
                            break;
                        case 'r':
                            transf.type = TR_ROTATION;
                            break;
                        case 's':
                            transf.type = TR_SCALE;
                            break;
                    }

#ifdef DEBUG_VBOLOADER
                    cout << "bone " << boneName << endl;
                    switch(transf.type)
                    {
                        case TR_TRANSLATION:
                            cout << "translation" << endl;
                            break;
                        case TR_ROTATION:
                            cout << "rotation" << endl;
                            break;
                        case TR_SCALE:
                            cout << "scale" << endl;
                        default:
                            cout << "wrong transformation type" << endl;
                    }
                    cout << "num keyframes: " << numkeys << endl;
#endif

                    // with keyframes
                    for(int k=0; k<numkeys; k++)
                    {
                        Keyframe key;
                        iss.read((char*)&key.time, sizeof(float));
                        iss.read((char*)key.coords, sizeof(float)*4);
                        transf.keyframes.push_back(key);
#ifdef DEBUG_VBOLOADER
                        cout << "time " << k.time;
                        cout << " " << k.coords[0] << " " << k.coords[1] << " " << k.coords[2] << " " << k.coords[3] << endl;
#endif
                    }

                    action.transformations.push_back(transf);
                }

                model->actions.push_back(action);
            }
        }
    }

    if(model->skeleton != NULL)
    {
        // adding _pose_ action
        Action poseAction;
        poseAction.name = "_pose_";
        model->actions.push_back(poseAction);

        model->isAnimated = true;
    }

    return model;
}

}
