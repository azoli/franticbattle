/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GUIHEALTHBAR_H
#define GUIHEALTHBAR_H

#include "TurbineGUIElement.h"
#include "TurbineColorf.h"

namespace Turbine
{

class GUIHealthBar : public GUIElement
{
    const static int COLOR_NUM = 7;
    Colorf colorArray[COLOR_NUM];
    public:

    GUIHealthBar(int id, int squareWidth, int squareHeight, int numSquares, Colorf colors[4], int borderSize, Colorf borderColor);
    ~GUIHealthBar();
    void update(float health);
};

}

#endif

