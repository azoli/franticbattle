/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef LIGHT_H
#define LIGHT_H

#include "TurbineIObject.h"
#include "TurbineColorf.h"

namespace Turbine
{

enum LIGHT_TYPE
{
    L_POINT,
    L_DIRECTIONAL,
    L_SPOT
};

class Light : public IObject
{

public:

/** Constructor/Destructor ***************************************************/
    Light( const std::string& name, unsigned int id, LIGHT_TYPE type = L_DIRECTIONAL,
          const Vector3d& position = Vector3d(0.0, 0.0, 0.0),
          Colorf color = Colorf(1.0,1.0,1.0), float radius=20.0)
             : IObject(name, id, position), _type(type),
               _ambientColor(1.0, 1.0, 1.0, 1.0), _diffuseColor(1.0, 1.0, 1.0, 1.0),
               _specularColor(1.0, 1.0, 1.0, 1.0), _spotDirection(0.0, -1.0, 0.0),
               _spotCutoff(30.0), _spotExponent(2.0)
    {
        _attenuation.set(1.0, 2.0/radius, 1.0/(radius*radius));
        if(_type == L_POINT)
            _spotCutoff = 180.0;
    }

    ~Light(){}

/** set/get ******************************************************************/
    void setType(LIGHT_TYPE type)
    {
        _type = type;
        if(_type == L_POINT)
            _spotCutoff = 180.0;
    }
    LIGHT_TYPE getType(){ return _type; }

    void setAmbientColor(const Colorf& ambientColor){ _ambientColor = ambientColor; }
    const Colorf& getAmbientColor(){ return _ambientColor; }

    void setDiffuseColor(const Colorf& diffuseColor){ _diffuseColor = diffuseColor; }
    const Colorf& getDiffuseColor(){ return _diffuseColor; }

    void setSpecularColor(const Colorf& specularColor){ _specularColor = specularColor; }
    const Colorf& getSpecularColor(){ return _specularColor; }

    void setAttenuation(float k0, float k1, float k2){ _attenuation.set(k0, k1, k2); }
    const Vector3d& getAttenuation(){ return _attenuation; }

    void setSpotDirection(const Vector3d& direction){ _spotDirection.set(direction); }
    const Vector3d& getSpotDirection(){ return _spotDirection; }

    void setSpotCutoff(float cutoff){ _spotCutoff = cutoff; }
    float getSpotCutoff(){ return _spotCutoff; }

    void setSpotExponent(float exponent){ _spotExponent=exponent; }
    float getSpotExponent(){ return _spotExponent; }

    void setRadius(int radius){ _radius = radius; }
    int getRadius(){ return _radius; }

private:

    LIGHT_TYPE _type;

    Colorf _ambientColor;
    Colorf _diffuseColor;
    Colorf _specularColor;
    Vector3d _attenuation;
    Vector3d _spotDirection;
    float _spotCutoff;
    float _spotExponent;
    int _radius;
};

}

#endif /* LIGHT_H */
