/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GL_H
#define GL_H

#ifdef _WIN32
#include "TurbineGLDeviceWin.h"
#elif __unix__
#include "TurbineGLDeviceUnix.h"
#endif


#include <vector>
#include "TurbineRenderer.h"
#include "TurbineModel.h"
#include "TurbineEntity.h"
#include "TurbineIWindow.h"
#include "TurbineTextureLoader.h"

#include "TurbineShader.h"

namespace Turbine
{

struct GLHardwareBuffers : public HardwareBuffers
{
    GLuint VBOID[3];   // 3 vertex buffer object id for: vertex/normals/texcoords
    GLuint IBOID;      // 1 index buffer object id
    GLuint VAOID;      // 1 vertex array object to hold states

    Model* modelPtr;   // used only for GUIElement

    GLHardwareBuffers(){}
    ~GLHardwareBuffers(){}
};

class GL : public Renderer
{

public:

/** Constructor/Destructor ***************************************************/
	GL();

	~GL();

/** Attach/Detach ************************************************************/
	bool attach(IWindow *window);

	bool detach();

/** Init *********************************************************************/
	bool init();

/** Resize *******************************************************************/
	void resizeScene(int width, int height);

/** Scene ********************************************************************/
	void beginScene();

	void endScene();

/** Transformation ***********************************************************/
    void setTransform(TRANSFORMATION_STATE state, const Matrix4& mat);
    const Matrix4& getTransform(TRANSFORMATION_STATE state);

/** Light ********************************************************************/
    void enableLighting( bool enable );
    bool isEnabledLighting();
    void drawLight(Light* light);

/** Draw *********************************************************************/
    bool createHardwareBuffers(Model* model);
    void destroyHardwareBuffers(Model* model);
    void drawEntity(Entity* entity);

    bool createHardwareBuffers(GUIElement* element);
    bool updateHardwareBuffers(GUIElement* element);
    void destroyHardwareBuffers(GUIElement* element);
    void drawGUIElement(GUIElement* element);

    void printCapabilities();

    // color picking
    unsigned int pickupColor(int x, int y);

    void enableDepthBuffer();
    void disableDepthBuffer();
private:

	IWindow *win;
	GLDevice *glDev;

	bool _lightingEnabled;

	Colorf _ambientLight;

	Matrix4 _matrix[TS_COUNT];

	GLuint vTex[MAX_TEXTURES];
    GLuint _notex;
    std::vector<int> texLoaded;

    void loadTexture(Image* image);
    void _createFBO();
    void _destroyFBO();
    bool _checkFBOCompleteness();
    void _blur(GLuint srcTex, float srcLevel, GLuint dstFBO, bool horizontal);

    // those shaders are equals except for bones transformations
    // that is only in the second one
    Shader* _shaderStatic;
    Shader* _shaderStaticFlat; // lighting disabled
    Shader* _shaderAnim;
    Shader* _shaderAnimFlat; // lighting disabled
    Shader* _shaderGUI;

    // FBO and related buffers for color-picking
    GLuint _fbo;
    GLuint _depthBuffer;
    GLuint _colorBuffers[3]; // 0-default rendering 1-color-picking 2-glow objects
    GLenum _fboBuffers[3];   // enum for drawbuffers

    // glow effect variables
    const static unsigned int KERNELSIZE = 5;
    float weights[KERNELSIZE];
    Vector2d hoffsets[KERNELSIZE];
    Vector2d voffsets[KERNELSIZE];

    const static unsigned int PASSES = 2;
    const static unsigned int NBUFFS = 4;
    Shader* _shaderBlur;
    Shader* _shaderCombine;
    GLuint _glowFBOs[PASSES][NBUFFS];
    GLuint _glowColorBuffers[PASSES];

    GLuint _fullscreenQuadVBO;

   int _lightNum;
};

}

#endif /*  GL_H */
