/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FONT_H
#define FONT_H

#include "TurbineImage.h"
#include "TurbineVector2d.h"

#define NUM_CHARS 256

namespace Turbine
{

struct CharacterMetrics
{
    int advances; // advance offsets
    int bearX;    // horizontal characters bearing
    int bearY;    // vertical characters bearing
    int width;    // characters width
    int height;   // character height
    Vector2d pos; // pen position at the character bottom left inside charmap
};

struct FontInstance
{
    std::string pathname;
    int size;

    Image* charmap;
    CharacterMetrics metrics[NUM_CHARS];

    ImageSet* render(const std::string& str);
};

}

#endif
