/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef OBJLOADER_H
#define OBJLOADER_H

#include <string>
#include <map>
#include "TurbineVector2d.h"
#include "TurbineVector3d.h"
#include "TurbineModel.h"
#include "TurbineIModelLoader.h"

namespace Turbine
{

struct ObjVertex
{
    int v;		// vertex index
    int n;		// normal index
    int t;		// texture index
    int m;		// material index

    ObjVertex() {}
    ObjVertex(int vIndex, int nIndex, int tIndex = 0, int mIndex = 0)
    {
        v = vIndex;
        n = nIndex;
        t = tIndex;
        m = mIndex;
    }

    ObjVertex operator=(const ObjVertex& vert)
    {
        v=vert.v;
        n=vert.n;
        t=vert.t;
        m=vert.m;
        return *this;
    }

    int operator==(const ObjVertex& b) const
    {
        if( (v==b.v) && (t==b.t) && (n==b.n) && (m==b.m) )
            return true;
        return false;
    }
};

struct ObjTriangle
{
    ObjVertex v[3];

    ObjTriangle() {}
    ObjTriangle(ObjVertex &v1, ObjVertex &v2, ObjVertex &v3)
    {
        v[0] = v1;
        v[1] = v2;
        v[2] = v3;
    }
    ~ObjTriangle() {}

    void set(const ObjVertex &v1, const ObjVertex &v2, const ObjVertex &v3)
    {
        v[0] = v1;
        v[1] = v2;
        v[2] = v3;
    }

    ObjVertex& operator[](int index)
    {
        return v[index];
    }
};

struct ObjQuad
{
    ObjVertex v[4];

    void set( ObjVertex& v1, ObjVertex& v2, ObjVertex& v3, ObjVertex& v4 )
    {
        v[0] = v1;
        v[1] = v2;
        v[2] = v3;
        v[3] = v4;
    }
};

class ObjLoader : public IModelLoader
{
    /** Vertex Face format *******************************************************/
    std::vector<Vector3d> vertexList;      // Vertex Position List
    std::vector<Vector3d> normalList;      // Normal List
    std::vector<Vector2d> texCoordList;    // Texture List
    std::vector<Material> materialList;    // Material List
    int vertexCount, normalCount, texCoordCount, materialCount;

    std::vector<ObjTriangle>	triangleList;		// 3 Vertex Face List
    std::list<SmoothingGroup> smoothGroups;
    int triangleCount;

    std::vector<Image> texImages;
    int textureCount;

    bool hasTexCoords;
    bool hasNormals;

    Model* toVBO();
    bool loadMtl(const std::string& mtlfile);

    std::string scenefile;
    std::map<std::string,int> matMap;

    void init();

public:
    ObjLoader()
    {
        init();
    }
    ~ObjLoader() {}
    Model* load(const std::string& file);
};

}

#endif /* OBJLOADER_H */
