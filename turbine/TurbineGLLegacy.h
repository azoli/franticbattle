/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GLLEGACY_H
#define GLLEGACY_H

#ifdef _WIN32
#include <windows.h>
#include <gl/gl.h>
#include <gl/glu.h>
#include "TurbineGLDeviceWin.h"
#elif __unix__
#include <gl.h>
#include <glu.h>
#include "TurbineGLDeviceUnix.h"
#endif


#include <vector>
#include "TurbineRenderer.h"
#include "TurbineModel.h"
#include "TurbineEntity.h"
#include "TurbineIWindow.h"
#include "TurbineGLDevice.h"

namespace Turbine
{

struct GLLegacyHardwareBuffers : public HardwareBuffers
{
    GLLegacyHardwareBuffers(){}
    ~GLLegacyHardwareBuffers(){}
};


class GLLegacy : public Renderer
{

public:

/** Constructor/Destructor ***************************************************/
	GLLegacy();

	~GLLegacy();

/** Attach/Detach ************************************************************/
	bool attach(IWindow *window);

	bool detach();

/** Init *********************************************************************/
	bool init();

/** Resize *******************************************************************/
	void resizeScene(int width, int height);

/** Scene ********************************************************************/
	void beginScene();

	void endScene();

/** Transformation ***********************************************************/
    void setTransform(TRANSFORMATION_STATE state, const Matrix4& mat);
    const Matrix4& getTransform(TRANSFORMATION_STATE state);

/** Light ********************************************************************/
    void enableLighting( bool enable );
    bool isEnabledLighting();
    void drawLight(Light* light);

/** Draw *********************************************************************/
    bool createHardwareBuffers(Model* model);
    void destroyHardwareBuffers(Model* model);
    void drawEntity(Entity* entity);

    bool createHardwareBuffers(GUIElement* element)
    {
        // TODO
        return false;
    }
    bool updateHardwareBuffers(GUIElement* element)
    {
        // TODO
        return true;
    }
    void destroyHardwareBuffers(GUIElement* element)
    {
        // TODO
    }
    void drawGUIElement(GUIElement* element)
    {
        // TODO
    }

    unsigned int pickupColor(int x, int y);

    void disableDepthBuffer()
    {
        glDisable(GL_DEPTH);
    }

    void enableDepthBuffer()
    {
        glEnable(GL_DEPTH);
    }

private:

	IWindow *win;
	GLDevice *glDev;

	bool _lightingEnabled;

	Colorf _ambientLight;

    void createGLMatrix(GLfloat* outMatrix, const Matrix4& sourceMatrix);

	Matrix4 _matrix[TS_COUNT];

	GLuint vTex[20];
    std::vector<int> texLoaded;

    void loadTexture(Image *image);
   int _index;
};

}

#endif /*  GLLEGACY_H */
