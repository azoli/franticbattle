/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TurbineGUIManager.h"

using namespace std;

namespace Turbine
{

void GUIManager::_freeTexture(GUIElement* element)
{
    Image* texture = element->getImageSet()->mat.texture;
    if(texture != NULL)
    {
        free(texture->data);
        TextureLoader::getInstance()->setFreeIndex(texture->index);
    }
}

GUIStaticImage* GUIManager::createStaticImage(Image* image)
{
    // create a quad with size width x height
    ImageSet* set = new ImageSet();
    Quad quad;
    quad.setCoords(0.0, 0.0, 0.0, image->width, 0.0, 0.0, \
                   image->width, image->height, 0.0, 0.0, image->height, 0.0);
    quad.setTexCoords(0.0, 0.0, 1.0, 0.0, 1.0, 1.0, 0.0, 1.0);
    set->regions.push_back(quad);
    set->width = image->width;
    set->height = image->height;
    set->mat.texture = image;

    GUIStaticImage* guiImage = new GUIStaticImage(_currID, set);
    _renderer->createHardwareBuffers(guiImage);
    _imgs.push_back(guiImage);
    _currID++;

    return guiImage;
}

GUIStaticImage* GUIManager::getStaticImage(int ID)
{
    list<GUIStaticImage*>::iterator it = _imgs.begin();
    while( (it != _imgs.end()) && ((*it)->getID() != ID) ) it++;
    if(it != _imgs.end())
        return (*it);

    return NULL;
}

void GUIManager::destroyStaticImage(GUIStaticImage* image)
{
    // free texture
    _freeTexture(image);

    _renderer->destroyHardwareBuffers(image);

    list<GUIStaticImage*>::iterator it = _imgs.begin();
    while( (it != _imgs.end()) && ((*it) != image) ) it++;
    if(it != _imgs.end())
    {
        delete (*it);
        _imgs.erase(it);
    }
}

GUIStaticText* GUIManager::createStaticText(const string& str, FontInstance* font)
{
    ImageSet* set = font->render(str);
    GUIStaticText* text = new GUIStaticText(_currID, set);

    _renderer->createHardwareBuffers(text);
    _texts.push_back(text);
    _currID++;

    return text;
}

GUIStaticText* GUIManager::getStaticText(int ID)
{
    list<GUIStaticText*>::iterator it = _texts.begin();
    while( (it != _texts.end()) && ((*it)->getID() != ID) ) it++;
    if(it != _texts.end())
        return (*it);

    return NULL;
}

void GUIManager::destroyStaticText(GUIStaticText* text)
{
    _renderer->destroyHardwareBuffers(text);

    list<GUIStaticText*>::iterator it = _texts.begin();
    while( (it != _texts.end()) && ((*it) != text) ) it++;
    if(it != _texts.end())
    {
        delete (*it);
        _texts.erase(it);
    }
}

GUIHealthBar* GUIManager::createHealthBar(int squareWidth, int squareHeight, int numSquares, Colorf colors[4], int borderWidth, Colorf borderColor)
{
    GUIHealthBar* bar = new GUIHealthBar(_currID, squareWidth, squareHeight, numSquares, colors, borderWidth, borderColor);

    _renderer->createHardwareBuffers(bar);
    _bars.push_back(bar);
    _currID++;

    return bar;
}

GUIHealthBar* GUIManager::getHealthBar(int ID)
{
    list<GUIHealthBar*>::iterator it = _bars.begin();
    while( (it != _bars.end()) && ((*it)->getID() != ID) ) it++;
    if(it != _bars.end())
        return (*it);

    return NULL;
}

void GUIManager::destroyHealthBar(GUIHealthBar* bar)
{
    _freeTexture(bar);

    _renderer->destroyHardwareBuffers(bar);

        _bars.remove(bar);
        delete bar;
}

void GUIManager::drawAll(int windowWidth, int windowHeight)
{
    if(!_renderer)
        return;

    _renderer->disableDepthBuffer();

    list<GUIStaticImage*>::iterator image;
    for(image = _imgs.begin(); image != _imgs.end(); image++)
    {
        if(!(*image)->isEnabled())
            continue;

        // draw static image
        Vector2d offset = (*image)->getOffset();
        Matrix4 model;
        model.setTranslation(Vector3d((int)(offset.x * windowWidth), (int)(offset.y * windowHeight), 0.0));
        _renderer->setTransform(TS_MODEL, model);
        _renderer->drawGUIElement((*image));
    }

    list<GUIStaticText*>::iterator text;
    for(text = _texts.begin(); text != _texts.end(); text++)
    {
        if(!(*text)->isEnabled())
            continue;

        // draw static text
        Vector2d offset = (*text)->getOffset();
        Matrix4 model;
        model.setTranslation(Vector3d((int)(offset.x * windowWidth), (int)(offset.y * windowHeight), 0.0));
        _renderer->setTransform(TS_MODEL, model);
        _renderer->drawGUIElement((*text));
    }

    list<GUIHealthBar*>::iterator bar;
    for(bar = _bars.begin(); bar != _bars.end(); bar++)
    {
        if(!(*bar)->isEnabled())
            continue;

        // update healthbar
        _renderer->updateHardwareBuffers((*bar));

        // draw health bar
        Vector2d offset = (*bar)->getOffset();
        Matrix4 model;
        model.setTranslation(Vector3d((int)(offset.x * windowWidth), (int)(offset.y * windowHeight), 0.0));
        _renderer->setTransform(TS_MODEL, model);
        _renderer->drawGUIElement((*bar));
    }

    _renderer->enableDepthBuffer();
}

}
