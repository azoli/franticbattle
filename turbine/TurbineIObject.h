/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IOBJECT_H
#define IOBJECT_H

#include <string>

#include "TurbineVector3d.h"
#include "TurbineMatrix4.h"

namespace Turbine
{

enum _OBJECT_TYPE
{
	O_UNKNOWN,
	O_CAMERA,
	O_LIGHT,
	O_MESH
};

class IObject
{

public:

/** Constructor/Destructor ***************************************************/
    IObject( const std::string& name, unsigned int id,
             const Vector3d& position = Vector3d(0,0,0),
             const Vector3d& rotation = Vector3d(0,0,0),
             const Vector3d& scale = Vector3d(1.0f,1.0f,1.0f) )
            : _name(name), _id(id), _translation(position),
              _rotation(rotation), _scale(scale), _enabled(true){}

    virtual ~IObject(){}

/** Name *********************************************************************/
    virtual void setName(const std::string& name)
    {
        _name = name;
    }

    virtual const std::string& getName()
    {
        return _name;
    }

/** ID ***********************************************************************/
    virtual void setID(unsigned int id){ _id = id; }
    virtual unsigned int getID(){ return _id; }

    virtual void enable(){ _enabled = true; }
    virtual void disable(){ _enabled = false; }
    virtual bool isEnabled(){ return _enabled; }

/** Type *********************************************************************/
	virtual _OBJECT_TYPE getType() const
	{
		return O_UNKNOWN;
	}

/** Transformation ***********************************************************/
    virtual void setScale(const Vector3d& scale)
    {
        _scale = scale;
    }

    virtual const Vector3d& getScale()
    {
        return _scale;
    }

    virtual void setRotation(const Vector3d& rotation)
    {
        _rotation = rotation;
    }

    virtual const Vector3d& getRotation()
    {
            return _rotation;
    }

    virtual void setPosition(const Vector3d& newpos)
    {
        _translation = newpos;
    }

    virtual const Vector3d& getPosition()
    {
        return _translation;
    }

	virtual Matrix4 getTransformation()
	{
		Matrix4 mat;
		mat.makeIdentity();

		mat.setRotationDegrees(_rotation);
		mat.setTranslation(_translation);

		if(_scale != Vector3d(1,1,1))
		{
			Matrix4 smat;
			smat.setScale(_scale);
			mat *= smat;
		}

		return mat;
	}

protected:

	std::string _name;
	int _id;

	Vector3d _translation;
	Vector3d _rotation;
	Vector3d _scale;

    bool _enabled;
};

}

#endif /* IOBJECT_H */
