/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TurbineGLDeviceWin.h"

namespace Turbine
{

/** init *******************************************************************/
bool GLDeviceWin::init(IWindow *window)
{
	win = (WindowWin*)window;

    // Create a fake context to load wgl extensions
	PIXELFORMATDESCRIPTOR pfd;
	memset (&pfd, 0, sizeof(pfd));
	pfd.nSize		= sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion	= 1;
	pfd.dwFlags		= PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.cColorBits	= win->getColorBits();
	pfd.cDepthBits	= win->getDepthBits();

	HDC hDC = win->getDevice();
    int pixelFormat = ChoosePixelFormat(hDC, &pfd);
    if(pixelFormat == 0)
    {
        MessageBox(win->getHandle(), "Error: Can't choose pixel format for the fake context", "ERROR", MB_OK | MB_ICONERROR);
        return false;
    }
    if(SetPixelFormat(hDC, pixelFormat, &pfd) == 0)
    {
        MessageBox(win->getHandle(), "Error: Can't set the pixel format for the fake context", "ERROR", MB_OK | MB_ICONERROR);
        return false;
    }
    // creating fake context
    glrc = wglCreateContext(hDC);
    if (glrc == NULL)
    {
        MessageBox(win->getHandle(), "Error: Can't create GL fake context", "ERROR", MB_OK | MB_ICONERROR);
        return false;
    }
    if (!wglMakeCurrent(hDC, glrc))
    {
        MessageBox(win->getHandle(), "Error: Can't make current GL fake context", "ERROR", MB_OK | MB_ICONERROR);
        wglDeleteContext(glrc);
        glrc = 0;
        return false;
    }

    // loading wgl extensions
    wglChoosePixelFormatARB = (PFNWGLCHOOSEPIXELFORMATARBPROC) getProcAddress("wglChoosePixelFormatARB");
    wglCreateContextAttribsARB = (PFNWGLCREATECONTEXTATTRIBSARBPROC) getProcAddress("wglCreateContextAttribsARB");

    // relasing fake context
    release();

    // Creating new context
    int attributeList[19];
    attributeList[0] = WGL_SUPPORT_OPENGL_ARB;
    attributeList[1] = TRUE;
    attributeList[2] = WGL_DRAW_TO_WINDOW_ARB;
    attributeList[3] = TRUE;
    attributeList[4] = WGL_ACCELERATION_ARB;
    attributeList[5] = WGL_FULL_ACCELERATION_ARB;
    attributeList[6] = WGL_COLOR_BITS_ARB;
    attributeList[7] = win->getColorBits();
    attributeList[8] = WGL_DEPTH_BITS_ARB;
    attributeList[9] = win->getDepthBits();
    attributeList[10] = WGL_DOUBLE_BUFFER_ARB;
    attributeList[11] = TRUE;
    attributeList[12] = WGL_SWAP_METHOD_ARB;
    attributeList[13] = WGL_SWAP_EXCHANGE_ARB;
    attributeList[14] = WGL_PIXEL_TYPE_ARB;
    attributeList[15] = WGL_TYPE_RGBA_ARB;
    attributeList[16] = WGL_STENCIL_BITS_ARB;
    attributeList[17] = 8;
    attributeList[18] = 0;

    unsigned int formatCount;
    if(!wglChoosePixelFormatARB(hDC, attributeList, NULL, 1, &pixelFormat, &formatCount))
    {
        MessageBox(win->getHandle(), "Error: Can't choose pixel format", "ERROR", MB_OK | MB_ICONERROR);
        return false;
    }
    if(SetPixelFormat(hDC, pixelFormat, &pfd) == 0)
    {
        MessageBox(win->getHandle(), "Error: Can't set the pixel format", "ERROR", MB_OK | MB_ICONERROR);
        return false;
    }
    // Creating 2.1 context
    attributeList[0] = WGL_CONTEXT_MAJOR_VERSION_ARB;
    attributeList[1] = 2;
    attributeList[2] = WGL_CONTEXT_MINOR_VERSION_ARB;
    attributeList[3] = 1;
    attributeList[4] = 0;
    glrc = wglCreateContextAttribsARB(hDC, 0, attributeList);
    if(glrc == NULL)
    {
        MessageBox(win->getHandle(), "Error: Can't create GL context", "ERROR", MB_OK | MB_ICONERROR);
        return false;
    }

    if(!wglMakeCurrent(hDC, glrc))
    {
        MessageBox(win->getHandle(), "Error: Can't Make current GL context", "ERROR", MB_OK | MB_ICONERROR);
        wglDeleteContext(glrc);
        glrc = 0;
        return false;
    }

	return true;
}

/** reset ********************************************************************/
bool GLDeviceWin::reset()
{
	//TODO
	return true;
}

/** release ******************************************************************/
bool GLDeviceWin::release()
{
	HDC hDC = win->getDevice();
	if(hDC == 0 && glrc == 0) return true;

	if( !wglMakeCurrent(0, 0) )
	{
		MessageBox(win->getHandle(), "Error: Release Of DC And RC Failed.", "Release Error", MB_OK | MB_ICONERROR);
		return false;
	}

	if( !wglDeleteContext(glrc) )
	{
		MessageBox(win->getHandle(), "Error: Release Rendering Context Failed.", "Release Error", MB_OK | MB_ICONERROR);
		return false;
	}
	glrc = 0;

	return true;
}

void* GLDeviceWin::getProcAddress(std::string funcName)
{
    return wglGetProcAddress(funcName.c_str());
}

/** swapBuffers **************************************************************/
void GLDeviceWin::swapBuffers()
{
	SwapBuffers(win->getDevice());
}

}