/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TurbineModel.h"

using namespace std;

namespace Turbine
{

Model::Model() : tloader(TextureLoader::getInstance()), scenefile(""), name(""), isAnimated(false), skeleton(NULL), buffs(NULL)
{
}

Model::~Model()
{
    // destroy mesh data
    for(unsigned int i=0; i<materials.size(); i++)
    {
        Image* texture = materials[i].texture;
        if(texture != NULL)
        {
            free(texture->data);
            tloader->setFreeIndex(texture->index);
        }
    }
    destroyMeshData();

    // destroy animation data
    if( skeleton != NULL)
    {
        skeleton->destroy_childs();
        delete skeleton;
        skeleton = NULL;
    }
    actions.resize(0);
}


float Action::length()
{
    float retlen = 0.0f;
    for(unsigned int i=0; i<transformations.size(); i++)
    {
        int lastindex = transformations[i].keyframes.size()-1;
        float len = transformations[i].keyframes[lastindex].time;
        if(len > retlen)
            retlen = len;
    }
    return retlen;
}

void Model::destroyMeshData()
{
    vertices.resize(0);
    faces.resize(0);
    materials.resize(0);
    smoothGroups.resize(0);
}

ostream& operator<<(ostream& os, const Model& m)
{
    os << endl << "Model " << m.scenefile << ". Stats:" << endl;
    os << " " << m.vertices.size() << " vertices" << endl;
    os << " " << m.faces.size()*3 << " indices" << endl;
    os << " " << m.materials.size() << " materials" << endl;

    os << "Vertices" << endl;
    vector<Vertex>::const_iterator vertex;
    for( vertex=m.vertices.begin(); vertex!=m.vertices.end(); vertex++)
        os << (*vertex);

    os << "Indices" << endl;
    vector<Face>::const_iterator face;
    for( face=m.faces.begin(); face!=m.faces.end(); face++)
        os << (*face);

    os << "Materials" << endl;
    vector<Material>::const_iterator material;
    for( material=m.materials.begin(); material!=m.materials.end(); material++)
        os << (*material);

    os << "Smoothing Groups" << endl;
    list<SmoothingGroup>::const_iterator group;
    for( group=m.smoothGroups.begin(); group!=m.smoothGroups.end(); group++)
        os << (*group);

    if(m.isAnimated)
    {
        os << endl << "Animation data" << endl;
        os << "Skeleton" << endl;
        os << m.skeleton;
        for(unsigned int i=0; i<m.actions.size(); i++)
            os << m.actions[i];
    }
    else
        os << "No animation data" << endl;

    return os;
}

Action* Model::getAction(const string& actionName)
{
    for(unsigned int i=0; i<actions.size(); i++)
        if(!actions[i].name.compare(actionName))
            return &actions[i];

    return NULL;
}

void Model::appendStaticMeshData(const vector<Quad>& quads, const Material& mat)
{

    int faceCounter = 0;
    int faceIndex = faces.size();

    for(unsigned int i=0; i<quads.size(); i++)
    {
        int vindex = vertices.size();
        const Quad& quad = quads[i];

        for(int vert=0; vert<4; vert++)
        {
            Vertex v;
            v.coord = quad.coords[vert];
            v.normal = quad.normals[vert];
            v.texCoord = quad.texCoords[vert];
            vertices.push_back(v);
        }

        faces.push_back(Face(vindex, vindex+1, vindex+2));
        faces.push_back(Face(vindex, vindex+2, vindex+3));

        faceCounter+=2;
    }

    SmoothingGroup group;
    group.start = faceIndex;
    group.end = faceIndex+faceCounter;
    group.mtlIndex = materials.size();
    smoothGroups.push_back(group);

    materials.push_back(mat);
}

void Model::initBBox()
{
    vector<Vector3d> points;

    vector<Vertex>::iterator vertex;
    for(vertex=vertices.begin(); vertex!=vertices.end(); vertex++)
        points.push_back((*vertex).coord);

    aabb.init(points);
}

const BBox& Model::getBBox()
{
    return aabb;
}

}
