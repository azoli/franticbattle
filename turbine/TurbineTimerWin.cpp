#include "TurbineTimerWin.h"

namespace Turbine
{

TimerWin::TimerWin()
{
	QueryPerformanceFrequency(&freq);
    reset();
}

TimerWin::~TimerWin()
{
}

void TimerWin::reset()
{
	QueryPerformanceCounter(&start);
}

float TimerWin::getElapsedSeconds()
{
	LARGE_INTEGER now;
	QueryPerformanceCounter(&now);
	return (double)(now.QuadPart-start.QuadPart) / (double)freq.QuadPart;
}

}
