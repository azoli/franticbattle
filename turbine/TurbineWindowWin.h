/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef WINDOWWIN_H
#define WINDOWWIN_H

#include <windows.h>
#include <string>
#include <queue>
#include <map>

#include "TurbineIWindow.h"
#include "TurbineIEventReceiver.h"

namespace Turbine
{

/** WindowWin ****************************************************************/
class WindowWin : public IWindow
{
	bool	fullscreen;

	HDC	hDC;
	HGLRC	hRC;

	HINSTANCE	hInstance;

	HWND	hWnd;

    std::map<int ,Key> _keyMap;

    std::queue<Event> eventQueue;

	static LRESULT CALLBACK WindowWin::wndProcRouter(HWND hWnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

	LRESULT CALLBACK wndProc(HWND hwnd, UINT uMsg, WPARAM wParam, LPARAM lParam);

public:

	WindowWin(const WindowSettings& settings);
	~WindowWin(){}

	void toggleFullscreen(){ /* TOOD */ }

    bool create();

	bool kill();

	void show();

    void hide(){ /* TODO */ }
	void refresh();

	HDC getDevice(){ return hDC; }
	HWND getHandle(){ return hWnd; }

    bool setTitle(const std::string& title);

	bool eventQueued();
	bool nextEvent(Event *event);
};

}

#endif /* WINDOWWIN_H */
