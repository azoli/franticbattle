/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GUIMANAGER_H
#define GUIMANAGER_H

#include <string>
#include <list>
#include "TurbineRenderer.h"
#include "TurbineModel.h"
#include "TurbineFont.h"
#include "TurbineGUIElement.h"
#include "TurbineGUIHealthBar.h"

namespace Turbine
{

typedef struct GUIElement GUIStaticText;
typedef struct GUIElement GUIStaticImage;

class GUIManager
{
    Renderer* _renderer;
    std::list<GUIStaticImage*> _imgs;
    std::list<GUIStaticText*> _texts;
    std::list<GUIHealthBar*> _bars;
    int _currID;

    void _freeTexture(GUIElement* element);

    public:

    GUIManager(Renderer* renderer) : _renderer(renderer), _currID(0)
    {}
    ~GUIManager(){}

    // Image
    GUIStaticImage* createStaticImage(Image* image);
    GUIStaticImage* getStaticImage(int ID);
    void destroyStaticImage(GUIStaticImage* image);

    // Static text
    GUIStaticText* createStaticText(const std::string& str, FontInstance* font);
    GUIStaticText* getStaticText(int ID);
    void destroyStaticText(GUIStaticText* text);

    // Healthbar
    GUIHealthBar* createHealthBar(int squareWidth, int squareHeight, int numSquares, Colorf colors[4], int borderWidth, Colorf borderColor);
    GUIHealthBar* getHealthBar(int ID);
    void destroyHealthBar(GUIHealthBar* bar);

    void drawAll(int windowWidth, int windowHeight);
};

}

#endif
