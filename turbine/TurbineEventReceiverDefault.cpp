/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "TurbineEngine.h"
#include "TurbineEventReceiverDefault.h"

namespace Turbine
{

bool EventReceiverDefault::onEvent(const Event &event)
{
	switch(event.type)
	{
		case EV_DRAW:
			break;

		case EV_QUIT:
		{
			std::cout << "ESC pressed: quit!" << std::endl;
			return false;
		}
		case EV_KEYBOARD:		//TODO
		{
//			cout << endl << endl << "keyboard! " << event.evKey.key << " " << event.evKey.pressed << endl;
			if(event.evKey.key == KEY_ESCAPE)		// ESC pressed
				if( !event.evKey.pressed )
					return false;
			
			if(event.evKey.key == KEY_RETURN)		// ALT+INVIO pressed
				if( event.evKey.pressed && event.evKey.alt )
					Engine::getInstance()->toggleFullscreen();
			break;
		}
		
		default:
		{
			break;
		}
	}

	return true;
}

}
