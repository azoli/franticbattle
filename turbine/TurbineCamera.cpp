/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <iomanip>
#include <cmath>
#include "TurbineMath.h"
#include "TurbineCamera.h"

using namespace std;
namespace Turbine
{

void Camera::setPosition(const Vector3d& newpos)
{
    _bUpdateMatrix = true;
}

void Camera::setLookAt(const Vector3d& lookat)
{
    _lookat = lookat;
    _bUpdateMatrix = true;
}

const Vector3d& Camera::getLookAt()
{
    return _lookat;
}

void Camera::setUpVector(const Vector3d& upVector)
{
    _upVector = upVector;
    _bUpdateMatrix = true;
}

const Vector3d& Camera::getUpVector()
{
    return _upVector;
}

void Camera::setLookatViewMatrix(const Vector3d& position, const Vector3d& lookat, const Vector3d& upVector)
{
    _translation = position;
    _lookat = lookat;
    _upVector = upVector;
    _bUpdateMatrix = true;
}

const Matrix4& Camera::getViewMatrix()
{
    if(_bUpdateMatrix)
    {
        _bUpdateMatrix = false;
        _viewMatrix.buildViewMatrix(_translation, _lookat, _upVector);
    }

    return _viewMatrix;
}

void Camera::zoom(float size)
{
    cartesianToSpherical();
    _ro-=size;    //decrement distance from center with positive value
    sphericalToCartesian();
    _bUpdateMatrix = true;
}

void Camera::rotateOriz(float degrees)
{
    cartesianToSpherical();
    _phi += degToRad(degrees);
    sphericalToCartesian();
    _bUpdateMatrix = true;
}

void Camera::rotateVert(float degrees)
{
    cartesianToSpherical();
    _theta -= degToRad(degrees);  // subtract because theta 0 is the zenith (0,1,0)
    sphericalToCartesian();
    _bUpdateMatrix = true;
}

float Camera::getZoom()
{
    return _ro;
}

float Camera::getOrizDegrees()
{
    return radToDeg(_phi);
}

float Camera::getVertDegrees()
{
    return radToDeg(_theta);
}

// TODO can be moved in math library
void Camera::cartesianToSpherical(void)
{
    _ro = std::sqrt( _translation.x*_translation.x + _translation.y*_translation.y + _translation.z*_translation.z );
    _theta = std::acos( _translation.y / _ro );
    _phi = std::atan2( _translation.x,  _translation.z );
}

// TODO can be moved in math library
void Camera::sphericalToCartesian(void)
{
    _translation.x = _ro * sin(_theta) * sin(_phi);
    _translation.y = _ro * cos(_theta);
    _translation.z = _ro * sin(_theta) * cos(_phi);
}

}
