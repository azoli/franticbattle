/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "TurbineQuaternion.h"
#include "TurbineEntity.h"

namespace Turbine
{

using namespace std;

//#define DEBUG_MODEL 1

Entity::Entity(const string& name, unsigned int id, Model* model,
              const Vector3d& position = Vector3d(0.0, 0.0, 0.0),
              const Vector3d& rotation = Vector3d(0.0, 0.0, 0.0),
              const Vector3d& scale = Vector3d(1.0, 1.0, 1.0))
          : IObject(name, id, position, rotation, scale), _model(model), _actionTime(0), _actionStart(0), _actionEnd(0), _actionLoop(true), _actionHasEnded(true), _forward(true), _actionVelocity(1.0)
{
    if(model->isAnimated)
    {
        _currSkeleton = model->skeleton->clone(NULL);
        _initIndexToBone(_currSkeleton);
        _initCombinedMatrices(_currSkeleton);
        setAction("_pose_");
    }
}

Entity::~Entity(){ }

Model* Entity::getModel()
{
    return _model;
}

void Entity::_initIndexToBone(Bone* bone)
{
    _indexToBone[bone->index] = bone;

    for(unsigned int i=0; i<bone->childs.size(); i++)
        _initIndexToBone(bone->childs[i]);
}

void Entity::_cleanFrameMatrices(Bone* bone)
{
    bone->frame.makeIdentity();

    for(unsigned int i=0; i<bone->childs.size(); i++)
        _cleanFrameMatrices(bone->childs[i]);
}

void Entity::_initCombinedMatrices(Bone* bone)
{
    Matrix4 curr = bone->poseFrame;
    Matrix4 father;

    if(bone->parent != NULL)
        father = combinedMatrices[bone->parent->index];
    else
        father.makeIdentity();

    Matrix4 final = curr * father;
    combinedMatrices.push_back(final);

    for(unsigned int i=0; i<bone->childs.size(); i++)
        _initCombinedMatrices(bone->childs[i]);
}

void Entity::_getCombinedMatrices(Bone* bone)
{
    Matrix4 curr = bone->frame * bone->poseFrame;
#ifdef DEBUG_MODEL
    cout << "calculate combined between: " << bone->name << " and father ";
#endif
    Matrix4 father;

    if(bone->parent != NULL)
    {
        father = combinedMatrices[bone->parent->index];
#ifdef DEBUG_MODEL
        cout << bone->parent->name << endl;
#endif
    }
    else
    {
        father.makeIdentity();
#ifdef DEBUG_MODEL
        cout << "identity" << endl;
#endif
    }

#ifdef DEBUG_MODEL
    cout << "pose" << endl;
    cout << bone->poseFrame;
    cout << "transformation" << endl;
    cout << bone->frame;
    cout << "pose x transf" << endl;
    cout << curr;
    cout << "father combined" << endl;
    cout << father;
    cout << endl;
#endif

    Matrix4 final = curr * father;
    combinedMatrices[bone->index] = final;

#ifdef DEBUG_MODEL
    cout << "combined final!" << endl;
    cout << combinedMatrices[bone->index];
    cout << "==================================================" << endl;
    cout << "COMBINED" << endl;
    for(unsigned int i=0; i<combinedMatrices.size(); i++)
        cout << combinedMatrices[i];
    cout << "==================================================" << endl;
#endif

    for(unsigned int i=0; i<bone->childs.size(); i++)
        _getCombinedMatrices(bone->childs[i]);
}

void Entity::_mulOffsetMatrices(Bone* bone)
{
    combinedMatrices[bone->index] = bone->offset * combinedMatrices[bone->index];
    for(unsigned int i=0; i<bone->childs.size(); i++)
        _mulOffsetMatrices(bone->childs[i]);
}

bool Entity::setAction(const string& actionName)
{
    if(!_model->isAnimated)
        return false;

    _cleanFrameMatrices(_currSkeleton);

    Action* action = _model->getAction(actionName);
    if(action)
    {
        _action = action;
        _actionTime = 0;
        _actionStart = 0;
        _actionLoop = true;
        _actionEnd = action->length();
        _actionHasEnded = false;
        _currKeyframes.resize(action->transformations.size());
        for(unsigned int i=0; i<action->transformations.size(); i++)
            _currKeyframes[i] = 0;

        updateTime(0);

#ifdef DEBUG_MODEL
        cout << "Action " << action->name << endl;
        cout << "time: " << _actionTime << endl;
        cout << "action start: " << _actionStart << endl;
        cout << "action end: " << _actionEnd << endl;
        cout << "action loop: ";
        (_actionLoop) ? cout << "true" : cout << "false";
        cout << endl;
#endif
        return true;
    }
    return false;
}

void Entity::updateTime(float time)
{
    if(_actionHasEnded || !_model->isAnimated)
        return;

    // update time
    if(_forward == true)
        _actionTime += time * _actionVelocity;
    else
        _actionTime -= time * _actionVelocity;

    float length = _actionEnd - _actionStart;

    // handle boundaries
    if( _forward && (_actionTime > length) ) // sup
    {
        if(_actionLoop)
        {
            int multiple = (int)(_actionTime / length);
            multiple *= length;
            _actionTime -= multiple;

            for(unsigned int i=0; i<_action->transformations.size(); i++)
                _currKeyframes[i] = 0;
        }
        else
            _actionHasEnded = true;
    }
    else if( !_forward && (_actionTime < 0) ) // inf
    {
        if(_actionLoop)
        {
            int multiple = (int)(_actionTime / length);
            multiple *= length;
            _actionTime -= multiple;
            _actionTime += length;

            for(unsigned int i=0; i<_action->transformations.size(); i++)
            {
                int numkeys = _action->transformations[i].keyframes.size();
                _currKeyframes[i] = numkeys-2;  // -1 for range [0, keyCount-1], -1 for next to last
            }
        }
        else
            _actionTime = 0;
    }

#ifdef DEBUG_MODEL
    cout << "update time " << time << "    action time" << _actionTime << endl;
#endif

    // find for each transformation the current keyframe (the inferior)
    if(_forward) // find forward
    {
        for(unsigned int i=0; i<_action->transformations.size(); i++)
        {
            vector<Keyframe> keys = _action->transformations[i].keyframes;

            while( (_currKeyframes[i] < keys.size()-1) && keys[_currKeyframes[i]+1].time < _actionTime ) // search the next keyframe
                _currKeyframes[i]++;

#ifdef DEBUG_MODEL
            cout << "key: " << _currKeyframes[i] << endl;
            cout << keys[_currKeyframes[i]];
            if(_currKeyframes[i] < keys.size()-1)
            {
                cout << "key next: " << _currKeyframes[i]+1 << endl;
                cout << keys[_currKeyframes[i]+1];

            }
            else
                cout << "key next: no" << endl;
#endif
        }
    }
    else // find backward
    {
        for(unsigned int i=0; i<_action->transformations.size(); i++)
        {
            vector<Keyframe> keys = _action->transformations[i].keyframes;
            while( keys[_currKeyframes[i]].time > _actionTime ) // search current keyframe
                _currKeyframes[i]--;
#ifdef DEBUG_MODEL
            cout << _currKeyframes[i] << " ";
#endif

        }
    }
#ifdef DEBUG_MODEL
    cout << endl;
#endif

    // without transformation is simple
    if(_action->transformations.size() == 0)
    {
        _getCombinedMatrices(_currSkeleton);
        _mulOffsetMatrices(_currSkeleton);
        return;
    }

    // apply transformations
    bool end = false;
    unsigned int trCounter = 0;
    do
    {
        Bone *bone, *boneNext;
        Matrix4 translation, rotation, scale;
        bone = _indexToBone[_action->transformations[trCounter].bone];

        translation.makeIdentity();
        rotation.makeIdentity();
        scale.makeIdentity();

        do
        {
            Transformation& tr = _action->transformations[trCounter];
            Keyframe& key = tr.keyframes[_currKeyframes[trCounter]];

            if(_currKeyframes[trCounter] == tr.keyframes.size()-1) // if is the last keyframe
            {
                switch(tr.type)
                {
                    case TR_TRANSLATION:
                    {
                        Vector3d t = *((Vector3d*)key.coords);
                        translation.setTranslation(t);
                        break;
                    }
                    case TR_SCALE:
                    {
                        Vector3d s = *((Vector3d*)key.coords);
                        scale.setScale(s);
                        break;
                    }
                    case TR_ROTATION:
                    {
                        float *co = key.coords;
                        Quaternion r = Quaternion(co[0], co[1], co[2], co[3]);
                        rotation = r.toMatrix();
                        break;
                    }
                }
            }
            else
            {
                Keyframe& keynext = tr.keyframes[_currKeyframes[trCounter]+1];

                float delta = (_actionTime - key.time) / (keynext.time - key.time);

                switch(tr.type)
                {
                    case TR_TRANSLATION:
                    {
                        Vector3d t1 = *((Vector3d*)key.coords);
                        Vector3d t2 = *((Vector3d*)keynext.coords);
                        Vector3d res;
                        res.lerpAt(t1, t2, delta);
                        translation.setTranslation(res);
                        break;
                    }
                    case TR_SCALE:
                    {
                        Vector3d s1 = *((Vector3d*)key.coords);
                        Vector3d s2 = *((Vector3d*)keynext.coords);
                        Vector3d res;
                        res.lerpAt(s1, s2, delta);
                        scale.setScale(res);
                        break;
                    }
                    case TR_ROTATION:
                    {
                        float *co1 = key.coords;
                        float *co2 = keynext.coords;
                        Quaternion r1 = Quaternion(co1[0], co1[1], co1[2], co1[3]);
                        Quaternion r2 = Quaternion(co2[0], co2[1], co2[2], co2[3]);

                        Quaternion res;
                        res = r1.slerp(r2, delta);
                        rotation = res.toMatrix();
                        break;
                    }
                }
            }

            trCounter++;

            // check transformation counter
            if(trCounter == _action->transformations.size())
            {
                end = true;
                break;
            }

            boneNext = _indexToBone[_action->transformations[trCounter].bone];
        }
        while(boneNext == bone);

        // change frame matrix
        bone->frame = rotation * scale * translation;
    }
    while(!end);

    _getCombinedMatrices(_currSkeleton);
    _mulOffsetMatrices(_currSkeleton);
#ifdef DEBUG_MODEL
    for(unsigned int i=0; i<combinedMatrices.size(); i++)
    {
        cout << "final transformations matrix[" << i << "]: " << _indexToBone[i]->name << endl;
        cout << combinedMatrices[i];
    }
#endif
}

}
