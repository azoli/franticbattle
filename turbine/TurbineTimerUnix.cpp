/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TurbineTimerUnix.h"

namespace Turbine
{

TimerUnix::TimerUnix()
{
    reset();
}

TimerUnix::~TimerUnix()
{
}

void TimerUnix::reset()
{
    gettimeofday(&start, 0);
}

float TimerUnix::getElapsedSeconds()
{
    struct timeval now;

    gettimeofday(&now, 0);
    return (now.tv_sec-start.tv_sec)*1.0f+(now.tv_usec-start.tv_usec)/1000000.0f;
}

}
