/* This file is a part of the Turbine game engine.
  This is a modified version of vector2d.h Irrlicht source.

  Copyright (C) 2002-2010 Nikolaus Gebhardt
  Copyright (C) 2012 Andrea Zoli

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef vector2D_H
#define vector2D_H

#include <iostream>
#include <iomanip>

namespace Turbine
{

struct Vector2d
{

/** Member yariables *********************************************************/
	float x, y;

/** Constrxctors *************************************************************/
	Vector2d() : x(0), y(0){}

	Vector2d(float nx, float ny) : x(nx), y(ny){}

	Vector2d(const Vector2d& tc) : x(tc.x), y(tc.y){}

    bool operator==(const Vector2d& v) const
    {
        return (x==v.x && y==v.y);
    }

    bool operator!=(const Vector2d& v) const
    {
        return (x!=v.x || y!=v.y);
    }
    friend std::ostream& operator<<(std::ostream& os, const Vector2d& v)
    {
        os.precision(3);
        os.setf(std::ios::right | std::ios::showpoint);
        os << "{ ";
        os << std::fixed << v.x;
        os << ", ";
        os << std::fixed << v.y;
        os << " }" << std::endl;

        return os;
    }

/** Set **********************************************************************/
	void set(const float nx, const float ny) { x=nx; y=ny; }

    void set(const Vector2d& tc) { x=tc.x; y=tc.y; }
};

}

#endif /* vector2D_H */
