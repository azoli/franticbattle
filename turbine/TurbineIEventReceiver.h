/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IEVENTRECEIVER_H
#define IEVENTRECEIVER_H

#include "TurbineKeyDefs.h"

namespace Turbine
{

enum EventType
{
	EV_KEYBOARD,
	EV_MOUSE,
	EV_DRAW,
	EV_RESIZE,
	EV_QUIT
};

struct KeyboardEvent
{
	EventType type;

	char  asciiKey;	// 0 if not a charater
	Key key;

	bool pressed;		// if not pressed is released

	bool alt;			// if alt is also pressed
	bool shift;			// if shift is also pressed
	bool control;		// if control is also pressed
};

struct MouseEvent
{
	EventType type;
	int x,y;
	Button button;
	bool pressed;		// if not pressed is released
};

union Event
{
	EventType type;

	KeyboardEvent evKey;
	MouseEvent evMouse;
};


class IEventReceiver
{

public:
	IEventReceiver(){}
	virtual ~IEventReceiver(){}

	virtual bool onEvent(const Event &event) = 0;
};

}

#endif /* IEVENTRECEIVER_H */
