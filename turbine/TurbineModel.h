/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MODEL_H
#define MODEL_H

#include "TurbineVector2d.h"
#include "TurbineVector3d.h"
#include "TurbineMatrix4.h"
#include "TurbineMaterial.h"
#include "TurbineTextureLoader.h"
#include "TurbineQuad.h"
#include "TurbineRenderer.h"
#include "TurbineBBox.h"

#include <vector>
#include <list>
#include <string>
#include <iostream>

namespace Turbine
{

struct SmoothingGroup
{
    int start;
    int end;
    int mtlIndex;
	
    SmoothingGroup() : start(0), end(0), mtlIndex(0) {}

    friend std::ostream& operator<<(std::ostream& os, const SmoothingGroup& sg)
    {
        os << "start:  " << sg.start << std::endl;
        os << "end:    " << sg.end << std::endl;
        os << "mindex: " << sg.mtlIndex << std::endl;

        return os;
    }
};

struct Vertex
{
    Vector3d coord;
    Vector3d normal;
    Vector2d texCoord;
    float bone_indices[4];
    float influences[4];

    Vertex()
    {
        for(int i=0; i<4; i++)
        {
            bone_indices[i] = 0.0;
            influences[i] = 0;
        }
    }

    friend std::ostream& operator<<(std::ostream& os, const Vertex& v)
    {
        os << "coord    " << v.coord;
        os << "normal   " << v.normal;
        os << "texcoord " << v.texCoord;
        os << "bone_indices ";
        for(int i=0; i<4; i++)
            os << v.bone_indices[i] << " ";
        os << std::endl;
        os << "influences ";
        for(int i=0; i<4; i++)
            os << v.influences[i] << " ";
        os << std::endl;

        return os;
    }
};

struct Face
{
    int index[3];

    Face(){ index[0] = index[1] = index[2] = 0; }

    Face(int i, int j, int k)
    {
        index[0] = i;
        index[1] = j;
        index[2] = k;
    }

    friend std::ostream& operator<<(std::ostream& os, const Face& f)
    {
        return os << f.index[0] << " " << f.index[1] << " " << f.index[2] << std::endl;
    }
};

struct Bone
{
    std::string name;
    int index;
    Matrix4 poseFrame;
    Matrix4 frame;
    Matrix4 offset;
    Bone* parent;
    std::vector<Bone*> childs;

    Bone() : name("None"), parent(NULL){}

    friend std::ostream& operator<<(std::ostream& os, const Bone& b)
    {
        os << b.name << std::endl;
        os << "index " << b.index << std::endl;
        os << b.poseFrame << b.offset;
        if(b.parent != NULL)
            os << "parent: " << b.parent->name << std::endl << std::endl;
        for(unsigned int i=0; i<b.childs.size(); i++)
            os << b.childs[i];

        return os;
    }

    void destroy_childs()
    {
        for(unsigned int i=0; i<childs.size(); i++)
        {
            childs[i]->destroy_childs();
            delete childs[i];
        }
    }

    Bone* clone(Bone* parent)
    {
        Bone* newbone = new Bone();
        newbone->name = name;
        newbone->index = index;
        newbone->frame = frame;
        newbone->poseFrame = poseFrame;
        newbone->offset = offset;
        newbone->parent = parent;

        for(unsigned int i=0; i<childs.size(); i++)
        {
            Bone* child = childs[i]->clone(newbone);
            newbone->childs.push_back(child);
        }
        return newbone;
    }
};
typedef Bone* Skeleton;

enum TRANSFORMATION_TYPE
{
    TR_TRANSLATION=0,
    TR_ROTATION,
    TR_SCALE,
};

struct Keyframe
{
    float time;
    float coords[4];

    friend std::ostream& operator<<(std::ostream& os, const Keyframe k)
    {
        return os << "time: " << k.time << " " << k.coords[0] << " " << k.coords[1] << " " << k.coords[2] << " " << k.coords[3] << std::endl;
    }
};

struct Transformation
{
    TRANSFORMATION_TYPE type;
    int bone;
    std::vector<Keyframe> keyframes;
    int active;

    friend std::ostream& operator<<(std::ostream& os, const Transformation t)
    {
        os << "bone index " << t.bone << std::endl;
        switch(t.type)
        {
            case TR_TRANSLATION:
                os << "translation" << std::endl;
                break;
            case TR_ROTATION:
                os << "rotation" << std::endl;
                break;
            case TR_SCALE:
                os << "scale" << std::endl;
                break;
            default:
                os << "wrong transformation type" << std::endl;
        }
        for(unsigned int i=0; i<t.keyframes.size(); i++)
            os << t.keyframes[i];

        return os;
    }
};

struct Action
{
    std::string name;
    std::vector<Transformation> transformations;

    float length();         // return action length in seconds

    friend std::ostream& operator<<(std::ostream& os, const Action& a)
    {
        os << "Action " << a.name << std::endl;
        for(unsigned int i=0; i<a.transformations.size(); i++)
            os << a.transformations[i];

        return os;
    }
};

struct HardwareBuffers;
struct Model
{
   TextureLoader* tloader;
    void init();
    BBox aabb;

public:

    std::string scenefile;
    std::string name;

    // Mesh data
    std::vector<Vertex> vertices;
    std::vector<Face> faces;
    std::vector<Material> materials;
    std::list<SmoothingGroup> smoothGroups;

    // Animation data
    bool isAnimated;
    Skeleton skeleton;          // tree-like skeleton
    std::vector<Action> actions;     // animations (forward-kinematics only)

    // internal buffers. Handled by the renderer
    HardwareBuffers* buffs;

    Model();
    ~Model();
    void destroyMeshData();

    friend std::ostream& operator<<(std::ostream& os, const Model& m);
    Action* getAction(const std::string& actionName);

    // internal function to add data to mesh. Use sceneManager->createQuadsModel() to create new models from a quad list
    void appendStaticMeshData(const std::vector<Quad>& quads, const Material& mat);

    void initBBox();
    const BBox& getBBox();
};

}

#endif /* MODEL.H */
