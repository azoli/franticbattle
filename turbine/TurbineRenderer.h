/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef RENDERER_H
#define RENDERER_H
#include <vector>

#include "TurbineIWindow.h"
#include "TurbineMatrix4.h"
#include "TurbineColorf.h"
#include "TurbineLight.h"
#include "TurbineGUIElement.h"

namespace Turbine
{

enum TRANSFORMATION_STATE
{
	TS_VIEW,
	TS_MODEL,
	TS_PROJECTION,
/*	TS_TEXTURE_0,
	TS_TEXTURE_1,
	TS_TEXTURE_2,
	TS_TEXTURE_3,*/

	TS_COUNT
};

// Abstraction on renderer buffers used for rendering
struct HardwareBuffers
{
    HardwareBuffers(){}
    virtual ~HardwareBuffers(){}
};

struct Model;
class Entity;
class Renderer
{

public:

	virtual ~Renderer() {}

/** Attach/Detach ************************************************************/
	virtual bool attach(IWindow *window) = 0;

	virtual bool detach() = 0;

/** Init *********************************************************************/
	virtual bool init() = 0;

/** Resize *******************************************************************/
	virtual void resizeScene(int width, int height) = 0;

/** Scene ********************************************************************/
	virtual void beginScene() = 0;

	virtual void endScene() = 0;

/** Transformation ***********************************************************/
    virtual void setTransform(TRANSFORMATION_STATE state, const Matrix4& mat) = 0;
    virtual const Matrix4& getTransform(TRANSFORMATION_STATE state) = 0;

/** Light ********************************************************************/
    virtual void enableLighting( bool enable ) = 0;
    virtual bool isEnabledLighting() = 0;
    virtual void drawLight(Light* light) = 0;

/** Draw *********************************************************************/
    virtual bool createHardwareBuffers(Model* model) = 0;
    virtual void destroyHardwareBuffers(Model* model) = 0;
    virtual void drawEntity(Entity* entity) = 0;

    virtual bool createHardwareBuffers(GUIElement* element) = 0;
    virtual bool updateHardwareBuffers(GUIElement* element) = 0;
    virtual void destroyHardwareBuffers(GUIElement* element) = 0;
    virtual void drawGUIElement(GUIElement* element) = 0;

    // return color (RGBA 32bit) from color-picking buffer from the given coordinates
    virtual unsigned int pickupColor(int x, int y) = 0;

/** Depth buffer *************************************************************/
    virtual void enableDepthBuffer() = 0;
    virtual void disableDepthBuffer() = 0;
};

}

#endif /*  RENDERER_H */
