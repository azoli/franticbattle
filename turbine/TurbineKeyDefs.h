/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef KEYDEFS_H
#define KEYDEFS_H

namespace Turbine
{

enum Key
{
	KEY_BACK				= 0x08,  // BACKSPACE key
	KEY_TAB					= 0x09,  // TAB key
	KEY_CLEAR				= 0x0C,  // CLEAR key
	KEY_RETURN				= 0x0D,  // ENTER key
	KEY_PAUSE				= 0x13,  // PAUSE key
	KEY_CAPITAL				= 0x14,	// SHIFT or CAPS LOCK key
	KEY_ESCAPE				= 0x1B,  // ESC key
	KEY_SPACE				= 0x20,  // SPACEBAR
	KEY_PAGEUP				= 0x21,  // PAGE UP key
	KEY_PAGEDOWN			= 0x22,  // PAGE DOWN key
	KEY_END					= 0x23,  // END key
	KEY_HOME				= 0x24,  // HOME key
	KEY_LEFT				= 0x25,  // LEFT ARROW key
	KEY_UP					= 0x26,  // UP ARROW key
	KEY_RIGHT				= 0x27,  // RIGHT ARROW key
	KEY_DOWN				= 0x28,  // DOWN ARROW key
	KEY_INSERT				= 0x2D,  // INS key
	KEY_DELETE				= 0x2E,  // DEL key
	KEY_KEY_0				= 0x30,  // 0 key
	KEY_KEY_1				= 0x31,  // 1 key
	KEY_KEY_2				= 0x32,  // 2 key
	KEY_KEY_3				= 0x33,  // 3 key
	KEY_KEY_4				= 0x34,  // 4 key
	KEY_KEY_5				= 0x35,  // 5 key
	KEY_KEY_6				= 0x36,  // 6 key
	KEY_KEY_7				= 0x37,  // 7 key
	KEY_KEY_8				= 0x38,  // 8 key
	KEY_KEY_9				= 0x39,  // 9 key
	KEY_KEY_A				= 0x41,  // A key
	KEY_KEY_B				= 0x42,  // B key
	KEY_KEY_C				= 0x43,  // C key
	KEY_KEY_D				= 0x44,  // D key
	KEY_KEY_E				= 0x45,  // E key
	KEY_KEY_F				= 0x46,  // F key
	KEY_KEY_G				= 0x47,  // G key
	KEY_KEY_H				= 0x48,  // H key
	KEY_KEY_I				= 0x49,  // I key
	KEY_KEY_J				= 0x4A,  // J key
	KEY_KEY_K				= 0x4B,  // K key
	KEY_KEY_L				= 0x4C,  // L key
	KEY_KEY_M				= 0x4D,  // M key
	KEY_KEY_N				= 0x4E,  // N key
	KEY_KEY_O				= 0x4F,  // O key
	KEY_KEY_P				= 0x50,  // P key
	KEY_KEY_Q				= 0x51,  // Q key
	KEY_KEY_R				= 0x52,  // R key
	KEY_KEY_S				= 0x53,  // S key
	KEY_KEY_T				= 0x54,  // T key
	KEY_KEY_U				= 0x55,  // U key
	KEY_KEY_V				= 0x56,  // V key
	KEY_KEY_W				= 0x57,  // W key
	KEY_KEY_X				= 0x58,  // X key
	KEY_KEY_Y				= 0x59,  // Y key
	KEY_KEY_Z				= 0x5A,  // Z key
	KEY_LWIN				= 0x5B,  // Left Windows key (Microsoft® Natural® keyboard)
	KEY_RWIN				= 0x5C,  // Right Windows key (Natural keyboard)
	KEY_MULTIPLY			= 0x6A,  // Multiply key
	KEY_ADD					= 0x6B,  // Add key
	KEY_SEPARATOR			= 0x6C,  // Separator key
	KEY_SUBTRACT			= 0x6D,  // Subtract key
	KEY_DECIMAL				= 0x6E,  // Decimal key
	KEY_DIVIDE				= 0x6F,  // Divide key
	KEY_F1					= 0x70,  // F1 key
	KEY_F2					= 0x71,  // F2 key
	KEY_F3					= 0x72,  // F3 key
	KEY_F4					= 0x73,  // F4 key
	KEY_F5					= 0x74,  // F5 key
	KEY_F6					= 0x75,  // F6 key
	KEY_F7					= 0x76,  // F7 key
	KEY_F8					= 0x77,  // F8 key
	KEY_F9					= 0x78,  // F9 key
	KEY_F10					= 0x79,  // F10 key
	KEY_F11					= 0x7A,  // F11 key
	KEY_F12					= 0x7B,  // F12 key
	KEY_NUMLOCK				= 0x90,  // NUM LOCK key
	KEY_SCROLL				= 0x91,  // SCROLL LOCK key
	KEY_LSHIFT				= 0xA0,  // Left SHIFT key
	KEY_RSHIFT				= 0xA1,  // Right SHIFT key
	KEY_LCONTROL			= 0xA2,  // Left CONTROL key
	KEY_RCONTROL			= 0xA3,  // Right CONTROL key
	KEY_LALT				= 0xA4,  // Left MENU key
	KEY_RALT				= 0xA5,  // Right MENU key
	KEY_PLUS				= 0xBB,  // Plus Key   (+)
	KEY_COMMA				= 0xBC,  // Comma Key  (,)
	KEY_MINUS				= 0xBD,  // Minus Key  (-)
	KEY_PERIOD				= 0xBE  // Period Key (.)
};

enum Button
{
	BTN_LEFT					= 0x01,  // Left mouse button
	BTN_RIGHT				= 0x02,  // Right mouse button
	BTN_MOVE					= 0x03,  // Button Move
	BTN_MIDDLE				= 0x04,  // Middle mouse button (three-button mouse)
	BTN_WHEELUP				= 0x05,  // Windows 2000/XP: X1 mouse button
	BTN_WHEELDOWN			= 0x06  // Windows 2000/XP: X2 mouse button
};

}

#endif	/* KEYDEFS_H */
