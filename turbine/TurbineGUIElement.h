/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GUIELEMENT_H
#define GUIELEMENT_H

#include<string>
#include "TurbineVector2d.h"
#include "TurbineImage.h"
#include "TurbineTextureLoader.h"

namespace Turbine
{

struct HardwareBuffers;
class GUIElement
{
    int _id;
    Vector2d _offset;
    bool _enabled;

    protected:

    ImageSet* _set;

    public:

    GUIElement(int id, ImageSet* set) : _id(id), _offset(0.0, 0.0), _enabled(true), _set(set) {}
    virtual ~GUIElement()
    {
    }

    virtual void enable(){ _enabled = true; }
    virtual void disable(){ _enabled = false; }
    virtual bool isEnabled(){ return _enabled; }

    virtual void setID(int id){ _id = id; }
    virtual int getID(){ return _id; }

    virtual void setOffset(const Vector2d& offset){ _offset = offset; }
    virtual const Vector2d& getOffset(){ return _offset; }

    virtual ImageSet* getImageSet(){ return _set; }

    // internal buffers used by the renderer
    HardwareBuffers* buffs;
};

}

#endif

