/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef QUAD_H
#define QUAD_H

#include "TurbineVector2d.h"
#include "TurbineVector3d.h"

namespace Turbine
{

struct Quad
{
    Vector3d coords[4];
    Vector3d normals[4];
    Vector2d texCoords[4];

    Quad()
    {
        for(int i=0; i<4; i++)
        {
            coords[i] = Vector3d(0,0,0);
            normals[i] = Vector3d(0,0,0);
            texCoords[i] = Vector2d(0,0);
        }
    }

    bool operator==(const Quad& q) const
    {
        for(int i=0; i<4; i++)
            if((coords[i]!=q.coords[i]) || (normals[i]!=q.normals[i]) || (texCoords[i]!=q.texCoords[i]))
                return false;

        return true;
    }

    void setCoords(float x0, float y0, float z0, float x1, float y1, float z1, \
              float x2, float y2, float z2, float x3, float y3, float z3)
    {
        coords[0] = Vector3d(x0, y0, z0);
        coords[1] = Vector3d(x1, y1, z1);
        coords[2] = Vector3d(x2, y2, z2);
        coords[3] = Vector3d(x3, y3, z3);
    }

    void setNormals(float x, float y, float z)
    {
        for(int i=0; i<3; i++)
            normals[i] = Vector3d(x, y, z);
    }

    void setNormals(float x0, float y0, float z0, float x1, float y1, float z1, \
              float x2, float y2, float z2, float x3, float y3, float z3)
    {
        normals[0] = Vector3d(x0, y0, z0);
        normals[1] = Vector3d(x1, y1, z1);
        normals[2] = Vector3d(x2, y2, z2);
        normals[3] = Vector3d(x3, y3, z3);
    }

    void setTexCoords(float u0, float v0, float u1, float v1, \
                 float u2, float v2, float u3, float v3)
    {
        texCoords[0] = Vector2d(u0, v0);
        texCoords[1] = Vector2d(u1, v1);
        texCoords[2] = Vector2d(u2, v2);
        texCoords[3] = Vector2d(u3, v3);
    }
};

}

#endif /* QUAD_H */
