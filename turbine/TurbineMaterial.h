/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MATERIAL_H
#define MATERIAL_H

#include <iostream>
#include <string>
#include "TurbineColorf.h"

namespace Turbine
{

struct Image;
struct Material
{
    std::string name;
    float ambient;
	Colorf kd;
	Colorf ks;
	float ns;		// shiness
    bool useGlow;
    Image* texture;

    Material() : name(""), ambient(0.0), kd(1.0, 1.0, 1.0), ks(1.0, 1.0, 1.0), ns(50), useGlow(false), texture(NULL)
    {}

    Material& operator=(const Material& mat)
    {
        name = mat.name;
        ambient = mat.ambient;
        kd = mat.kd;
        ks = mat.ks;
        ns = mat.ns;
        texture = mat.texture;

        return *this;
    }

    bool operator==(const Material& mat) const
    {
        return (name.compare(mat.name)==0 && ambient == mat.ambient && kd==mat.kd && ks==mat.ks && ns==mat.ns && texture==mat.texture);
    }
    bool operator!=(const Material& mat) const
    {
        return !((*this)==mat);
    }
    friend std::ostream& operator<<(std::ostream& os, const Material& m)
    {
        os << "name: " << m.name;
        os << "ambient: " << m.ambient;
        os << "diffuse: " << m.kd;
        os << "specular: " << m.ks;
        os << "shininess: " << m.ns << std::endl;
        os << "texture: ";
        (m.texture != NULL) ? os << "yes" : os << "no";
        os << std::endl;

        return os;
    }
};

}

#endif /*  MATERIAL_H */
