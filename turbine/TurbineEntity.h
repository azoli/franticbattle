/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef ENTITY_H
#define ENTITY_H

#include <map>
#include "TurbineIObject.h"
#include "TurbineModel.h"

namespace Turbine
{

class Entity : public IObject
{
    Model* _model;

    Action* _action;        // current action
    float   _actionTime;
    float   _actionStart;   // start delimiter (sec)
    float   _actionEnd;     // end delimiter (sec)
    float   _actionLoop;    // loop action?
    bool    _actionHasEnded;  // true if animation is active
    bool    _forward;       // whenever the action is going forward or backward
    float   _actionVelocity; // velocity multiplier (default 1.0)

    Skeleton _currSkeleton;               // current skeleton
    std::map<int, Bone*> _indexToBone;          // get current skeleton bone from index (needed for transformations)
    std::vector<unsigned int> _currKeyframes;           // current lower keyframe index for each transformation
    std::vector<Matrix4> combinedMatrices;     // current per-bone frames

    void _initIndexToBone(Bone* bone);
    void _initCombinedMatrices(Bone* bone);
    void _getCombinedMatrices(Bone* bone);
    void _mulOffsetMatrices(Bone* bone);
    void _cleanFrameMatrices(Bone* bone);

public:

/** Constructor/Destructor ***************************************************/
    Entity(const std::string& name, unsigned int id, Model* model, const Vector3d& position,
           const Vector3d& rotation, const Vector3d& scale );

    ~Entity();

    Model* getModel();

/** Animation ****************************************************************/
    bool setAction(const std::string& actionName);
    const std::string& getActionName(){ return _action->name; }

//TODO check boundaries
    void setActionStart(float start){ _actionStart = start; }
    float getActionStart(){ return _actionStart; }
    void setActionEnd(float end){ _actionEnd = end; }
    float getActionEnd(){ return _actionEnd; }
    void setActionLoop(bool loop){ _actionLoop = loop; }
    bool getActionLoop(){ return _actionLoop; }

    void setActionVelocity(float velocity){ _actionVelocity = velocity; }
    float getActionVelocity(){ return _actionVelocity; }

    void setForwardAction(){ _forward = true; }
    void setBackwardAction(){ _forward = false; }

    void updateTime(float time);

    bool hasEnded(){ return _actionHasEnded; }

    const std::vector<Matrix4>& getBonesMatrices(){ return combinedMatrices; }
};

}

#endif /* ENTITY_H */
