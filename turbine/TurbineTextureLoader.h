/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TEXTURELOADER_H
#define TEXTURELOADER_H

#include <stack>
#include "TurbineImage.h"

namespace Turbine
{

const int MAX_TEXTURES = 100;

class TextureLoader
{
	
public:

    static TextureLoader* getInstance();

    int getFreeIndex()
    {
        int retval = indexes.top();
        indexes.pop();
	return retval;
    }

    void setFreeIndex(int index)
    {
        indexes.push(index);
    }

    /* Load a bmp file to Image memory struct with dynamically allocation. Assume depth of image 24 bit.
     * The Image struct is useful for opengl. On error return NULL. Remember to free Image pointer */
    Image* loadBMP(const std::string& filename);

    Image* loadPNG(const std::string& filename);
    void writePNG(Image* image, const std::string& filename);
private:
	
	TextureLoader();
	
	static TextureLoader* tloader;
		
    std::stack<int> indexes;

	struct bmp_header
	{
		unsigned short type;		/* Magic identifier            */
		unsigned int size;		/* File size in bytes          */
		unsigned short reserved1, reserved2;
		unsigned int offset;		/* Offset to image data, bytes */
	};

	struct bmp_info
	{
		unsigned int size;		/* Header size in bytes      */
		int width, height;		/* Width and height of image */
		unsigned short planes;	/* Number of colour planes   */
		unsigned short bits;		/* Bits per pixel            */
		unsigned int compression;	/* Compression type          */
		unsigned int imagesize;	/* Image size in bytes       */
		int xresolution, yresolution;	/* Pixels per meter          */
		unsigned int ncolours;	/* Number of colours         */
		unsigned int importantcolours;	/* Important colours         */
	};
};

}

#endif /* TEXTURELOADER_H */
