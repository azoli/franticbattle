/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TurbineGUIHealthBar.h"
#include "TurbineTextureLoader.h"

using namespace std;

namespace Turbine
{

GUIHealthBar::GUIHealthBar(int id, int squareWidth, int squareHeight, int numSquares, Colorf colors[4], int borderSize, Colorf borderColor) : GUIElement(id, NULL)
{
    ImageSet* set = new ImageSet();

    float bottom = 0;
    float top = squareHeight;
    for(int square=0; square<numSquares; square++)
    {
        Quad quad;
        float left = (squareWidth-borderSize)*square;
        float right = left+squareWidth;
        quad.coords[0] = Vector3d(left, bottom, 0.0);
        quad.coords[1] = Vector3d(right, bottom, 0.0);
        quad.coords[2] = Vector3d(right, top, 0.0);
        quad.coords[3] = Vector3d(left, top, 0.0);

        int index = 0;
        quad.texCoords[0] = Vector2d(0.0, (float)index/COLOR_NUM);
        quad.texCoords[1] = Vector2d(1.0, (float)index/COLOR_NUM);
        quad.texCoords[2] = Vector2d(1.0, (float)(index+1.0)/COLOR_NUM);
        quad.texCoords[3] = Vector2d(0.0, (float)(index+1.0)/COLOR_NUM);

        set->regions.push_back(quad);
    }

    set->mat.texture = new Image();
    Image* tex = set->mat.texture;
    tex->index = TextureLoader::getInstance()->getFreeIndex();
    tex->width = squareWidth;
    tex->height = squareHeight*COLOR_NUM;
    tex->type = RGBA_32BIT_U;

    // allocate texture atlas for COLOR_NUM squares
    int squareSize = squareWidth*squareHeight*4;
    unsigned char* ptr = new unsigned char[squareSize*COLOR_NUM];
    tex->data = ptr;

    //generate intermediate colors
    colorArray[0] = colors[0];
    colorArray[1] = colors[0].getInterpolated(colors[3], 0.5);
    colorArray[2] = colors[1];
    colorArray[3] = colors[1].getInterpolated(colors[3], 0.5);
    colorArray[4] = colors[2];
    colorArray[5] = colors[2].getInterpolated(colors[3], 0.5);
    colorArray[6] = colors[3];

    // create COLOR_NUM squares with the same border and different internal color
    // the up border lower pixel
    int upBorder = squareHeight-1-borderSize;
    // the right border most left pixel
    int rightBorder = squareWidth-1-borderSize;
    Colorf* c;
    for(int square=0; square<COLOR_NUM; square++)
    {
        for(int y=0; y<squareHeight; y++)
        {
            for(int x=0; x<squareWidth; x++)
            {
                // if the pixel is on the border
                if(x<borderSize || y<borderSize || x>rightBorder || y>upBorder)
                    c = &borderColor;
                else // is inside the square
                    c = &colorArray[square];

                int offset = square*squareSize + (y*squareWidth + x)*4;
                ptr[offset] = (unsigned char)(c->r * 255.0);
                ptr[offset+1] = (unsigned char)(c->g * 255.0);
                ptr[offset+2] = (unsigned char)(c->b * 255.0);
                ptr[offset+3] = (unsigned char)(c->a * 255.0);
            }
        }
    }

    set->width = (squareWidth-borderSize)*numSquares + borderSize*2;
    set->height = squareHeight;

    _set = set;
}

GUIHealthBar::~GUIHealthBar()
{
}

void GUIHealthBar::update(float health)
{
    if(health > 1.0)
        health = 1.0;

    if(health < 0.0)
        health = 0.0;

    std::vector<Quad>& quads = _set->regions;
    int squareNum = quads.size();

    float indexF = health * squareNum;
    int index = ((int)(indexF)); // [1, squareNum]

    int color;
    if(health < 1/3.0)
        color = 4;
    else if(health < 2/3.0)
        color = 2;
    else
        color = 0;

    // full color squares
    for(int square=0; square<index; square++)
    {
        Quad& quad = quads[square];
        quad.texCoords[0] = Vector2d(0.0, (float)color/COLOR_NUM);
        quad.texCoords[1] = Vector2d(1.0, (float)color/COLOR_NUM);
        quad.texCoords[2] = Vector2d(1.0, (float)(color+1.0)/COLOR_NUM);
        quad.texCoords[3] = Vector2d(0.0, (float)(color+1.0)/COLOR_NUM);
    }

    // possible half color square
    if(indexF - (float)index > 0.0)
    {
        Quad& quad = quads[index];
        color++;
        quad.texCoords[0] = Vector2d(0.0, (float)color/COLOR_NUM);
        quad.texCoords[1] = Vector2d(1.0, (float)color/COLOR_NUM);
        quad.texCoords[2] = Vector2d(1.0, (float)(color+1.0)/COLOR_NUM);
        quad.texCoords[3] = Vector2d(0.0, (float)(color+1.0)/COLOR_NUM);
        index++;
    }

    // empty color squares
    for(int square=index; square<squareNum; square++)
    {
        Quad& quad = quads[square];
        quad.texCoords[0] = Vector2d(0.0, (float)6/COLOR_NUM);
        quad.texCoords[1] = Vector2d(1.0, (float)6/COLOR_NUM);
        quad.texCoords[2] = Vector2d(1.0, (float)7/COLOR_NUM);
        quad.texCoords[3] = Vector2d(0.0, (float)7/COLOR_NUM);
    }
}

}
