/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef MATH_H
#define MATH_H

namespace Turbine
{

const float PI = 3.14159265359f;

const double PI64 = 3.1415926535897932384626433832795028841971693993751;

inline float radToDeg(float x){ return x * 180.0f / PI; }
inline float degToRad(float x){ return x * PI / 180.0f; }

inline double radToDeg64(double x){ return x * 180.0f / PI64; }
inline double degToRad64(double x){ return x * PI64 / 180.0f; }

inline float round(float x)
{
    if(x >= 0.0)
        return (int)(x + 0.5);
    else if((x - (double)(int)x) <= -0.5)
        return (int)x;
    return (int)(x - 0.5);
}

inline double round64(double x)
{
    if(x >= 0.0)
        return (int)(x + 0.5);
    else if((x - (double)(int)x) <= -0.5)
        return (int)x;
    return (int)(x - 0.5);
}

}

#endif /* MATH_H */
