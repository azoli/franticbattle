/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SCENEMANAGER_H
#define SCENEMANAGER_H

#include <list>

#include "TurbineIObject.h"
#include "TurbineRenderer.h"

#include "TurbineVector3d.h"
#include "TurbineColorf.h"
#include "TurbineModel.h"
#include "TurbineCamera.h"
#include "TurbineLight.h"
#include "TurbineEntity.h"

#include "TurbineObjLoader.h"
#include "TurbineVBOLoader.h"

namespace Turbine
{

class Entity;

class SceneManager
{
    Renderer* _renderer;

    std::list<Camera*> cameraList;
    std::list<Light*> lightList;
    std::list<Entity*> entityList;

    Camera* _activeCamera;

    ObjLoader objLoader;
    VBOLoader vboLoader;

    unsigned int _newID;

public:

    SceneManager(Renderer* renderer);
    ~SceneManager();

    // load a model from file
    Model* loadModel(const std::string& modelFilename);
    void destroyModel(Model* model);

    // create a model from the quads list
    Model* createQuadsModel(const std::vector<Quad>& quads, const Material& mat);

    // handle cameras
    Camera* createCamera(const std::string& name, bool active = true,
                         const Vector3d& position = Vector3d(0.0, 0.0, 0.0),
                         const Vector3d& lookat   = Vector3d(0.0, 0.0, -1.0),
                         const Vector3d& upVector = Vector3d(0.0, 1.0, 0.0));
    Camera* getCamera(const std::string& name);
    bool hasCamera(const std::string& name);
    void destroyCamera(Camera *camera);
    void setActiveCamera(Camera* camera);
    Camera* getActiveCamera();

    // handle lights
    Light* createLight(const std::string& name, LIGHT_TYPE type = L_DIRECTIONAL,
                    const Vector3d& position = Vector3d(0.0, 0.0, 0.0),
                    Colorf color = Colorf(1.0, 1.0, 1.0), float radius=100.0);
    Light* getLight(const std::string& name);
    bool hasLight(const std::string& name);
    void destroyLight(Light* light);

    // handle entities (model instances)
    Entity* createEntity(const std::string& name, Model* model,
                     const Vector3d& position = Vector3d(0.0, 0.0, 0.0),
                     const Vector3d& rotation = Vector3d(0.0, 0.0, 0.0),
                     const Vector3d& scale    = Vector3d(1.0, 1.0, 1.0));
    Entity* getEntity(const std::string& name);
    bool hasEntity(const std::string& name);
    void destroyEntity(Entity* model);

    // select an Entity from 2d coordinates (by color-picking)
    Entity* selectEntity(int x, int y);

    // update animations
    void updateTime(float delta);

    // internal function called by the renderer
    void drawAll();

};

}

#endif /* SCENEMANAGER_H */
