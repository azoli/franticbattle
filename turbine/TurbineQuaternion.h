/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.

    This class is using code present in this gamasutra article:
    http://www.gamasutra.com/view/feature/131686/rotating_objects_using_quaternions.php
*/

#ifndef QUATERNION_H
#define QUATERNION_H

#include "TurbineMatrix4.h"
#include "TurbineVector3d.h"

namespace Turbine
{

class Quaternion
{

public:

    float x;
    float y;
    float z;
    float w;

/** Constructor/Destructor ***************************************************/
    Quaternion() : x(0.0f), y(0.0f), z(0.0f), w(1.0f) {}
    Quaternion(float x, float y, float z, float w) : x(x), y(y), z(z), w(w) {}

/** Operator *****************************************************************/
    inline Quaternion& operator=(const Quaternion& other);

    bool operator==(const Quaternion& other) const;
    bool operator!=(const Quaternion& other) const;

    Quaternion operator+(const Quaternion& other) const;

    inline Quaternion operator*(const float& s) const;
    inline Quaternion& operator*=(const float& s);

    inline Quaternion operator*(const Quaternion& other) const;
    inline Quaternion& operator*=(const Quaternion& other);

    inline Quaternion& set(float x, float y, float z, float w);
    const Quaternion& makeIdentity();
    const Quaternion& makeInverse();

    inline float dotProduct(const Quaternion& other) const;

    Quaternion slerp(Quaternion to, float time); // the from is the current quaternion

    inline Matrix4 toMatrix();

};

inline Quaternion& Quaternion::operator=(const Quaternion& other)
{
    x = other.x;
    y = other.y;
    z = other.z;
    w = other.w;
    return *this;
}

inline bool Quaternion::operator==(const Quaternion& other) const
{
    return ((x == other.x) &&
            (y == other.y) &&
            (z == other.z) &&
            (w == other.w));
}

inline bool Quaternion::operator!=(const Quaternion& other) const
{
    return !(*this == other);
}

inline Quaternion Quaternion::operator+(const Quaternion& b) const
{
    return Quaternion(x+b.x, y+b.y, z+b.z, w+b.w);
}

inline Quaternion Quaternion::operator*(const float& s) const
{
    return Quaternion(s*x, s*y, s*z, s*w);
}

inline Quaternion& Quaternion::operator*=(const float& s)
{
    x*=s;
    y*=s;
    z*=s;
    w*=s;
    return *this;
}

inline Quaternion Quaternion::operator*(const Quaternion& other) const
{
    float A, B, C, D, E, F, G, H;
    A = (w + x)*(other.w + other.x);
    B = (z - y)*(other.y - other.z);
    C = (w - x)*(other.y + other.z); 
    D = (y + z)*(other.w - other.x);
    E = (x + z)*(other.x + other.y);
    F = (x - z)*(other.x - other.y);
    G = (w + y)*(other.w - other.z);
    H = (w - y)*(other.w + other.z);

    float xx = A - (E + F + G + H)/2;
    float yy = C + (E - F + G - H)/2;
    float zz = D + (E - F - G + H)/2;
    float ww = B + (-E - F + G + H)/2;

    return Quaternion(xx, yy, zz, ww);
}

inline Quaternion& Quaternion::operator*=(const Quaternion& other)
{
    return (*this = other * (*this));
}

inline Quaternion& Quaternion::set(float x, float y, float z, float w)
{
    x = x;
    y = y;
    z = z;
    w = w;
    return *this;
}

inline const Quaternion& Quaternion::makeIdentity()
{
    x = 0.0f;
    y = 0.0f;
    z = 0.0f;
    w = 1.0f;
    return *this;
}

inline const Quaternion& Quaternion::makeInverse()
{
    x = -x;
    y = -y;
    z = -z;
    return *this;
}

inline float Quaternion::dotProduct(const Quaternion& other) const
{
    return (x * other.x) + (y * other.y) + (z * other.z) + (w * other.w);
}

inline Quaternion Quaternion::slerp(Quaternion to, float time)
{
    Quaternion& from = (*this);

    float to1[4];
    double omega, cosom, sinom, scale0, scale1;

    // calc cosine
    cosom = from.dotProduct(to);

    // adjust signs (if necessary)
    if ( cosom <0.0 )
    {
        cosom = -cosom; to1[0] = - to.x;
        to1[1] = - to.y;
        to1[2] = - to.z;
        to1[3] = - to.w;
    } else
    {
        to1[0] = to.x;
        to1[1] = to.y;
        to1[2] = to.z;
        to1[3] = to.w;
    }

    // calculate coefficients
    if ( (1.0 - cosom) > 0.05f )
    {
        // standard case (slerp)
        omega = acos(cosom);
        sinom = sin(omega);
        scale0 = sin((1.0f - time) * omega) / sinom;
        scale1 = sin(time * omega) / sinom;
    } else
    {
        // "from" and "to" quaternions are very close 
        //  ... so we can do a linear interpolation
        scale0 = 1.0 - time;
        scale1 = time;
    }
    // calculate final values
    float xx = scale0 * from.x + scale1 * to1[0];
    float yy = scale0 * from.y + scale1 * to1[1];
    float zz = scale0 * from.z + scale1 * to1[2];
    float ww = scale0 * from.w + scale1 * to1[3];

    return Quaternion(xx, yy, zz ,ww);
}

inline Matrix4 Quaternion::toMatrix()
{
    Matrix4 m;

    float wx, wy, wz, xx, yy, yz, xy, xz, zz, x2, y2, z2;

    // calculate coefficients
    x2 = x + x; y2 = y + y;
    z2 = z + z;
    xx = x * x2; xy = x * y2; xz = x * z2;
    yy = y * y2; yz = y * z2; zz = z * z2;
    wx = w * x2; wy = w * y2; wz = w * z2;

    m[0] = 1.0 - (yy + zz);
    m[4] = xy - wz;
    m[8] = xz + wy;
    m[12] = 0.0;

    m[1] = xy + wz;
    m[5] = 1.0 - (xx + zz);
    m[9] = yz - wx;
    m[13] = 0.0;

    m[2] = xz - wy;
    m[6] = yz + wx;
    m[10] = 1.0 - (xx + yy);
    m[14] = 0.0;

    m[3] = 0;
    m[7] = 0;
    m[11] = 0;
    m[15] = 1;

    return m;
}

}

#endif
