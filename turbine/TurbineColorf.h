/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef COLORF_H
#define COLORF_H

#include <iostream>

namespace Turbine
{

class Colorf
{

public:

	Colorf() : r(1.0f), g(1.0f), b(1.0f), a(1.0f) {}

	Colorf(float r, float g, float b) : r(r), g(g), b(b), a(1.0f) {}
	Colorf(float r, float g, float b, float a) : r(r), g(g), b(b), a(a) {}

	float r;
	float g;
	float b;
	float a;

	void set(float rr, float gg, float bb)
	{
		r = rr;
		g = gg;
		b = bb;
	}
	
	void set(float rr, float gg, float bb, float aa)
	{
		r = rr;
		g = gg;
		b = bb;
		a = aa;
	}
	
	Colorf& operator=(const Colorf& c) { this->r=c.r; this->g=c.g; this->b=c.b; a=c.a; return *this; }

    bool operator==(const Colorf& c) const
    {
        return (r==c.r && g==c.g && b==c.b && a==c.a);
    }

    friend std::ostream& operator<<(std::ostream& os, const Colorf& c)
    {
        return os << c.r << " " << c.g << " " << c.b << " " << c.a << std::endl;
    }
	Colorf getInterpolated(const Colorf &other, float d) const
	{
		const float d_inv = 1.0f - d;
		
		return Colorf(	r*d + other.r*d_inv,
							g*d + other.g*d_inv,
							b*d + other.b*d_inv,
							a*d + other.a*d_inv);
	}
};

}

#endif /* COLORF_H */
