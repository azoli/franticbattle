/* This file is a part of the Turbine game engine.
  This is a modified version of vector3d.h Irrlicht source.

  Copyright (C) 2002-2010 Nikolaus Gebhardt
  Copyright (C) 2012 Andrea Zoli

  This software is provided 'as-is', without any express or implied
  warranty.  In no event will the authors be held liable for any damages
  arising from the use of this software.

  Permission is granted to anyone to use this software for any purpose,
  including commercial applications, and to alter it and redistribute it
  freely, subject to the following restrictions:

  1. The origin of this software must not be misrepresented; you must not
     claim that you wrote the original software. If you use this software
     in a product, an acknowledgment in the product documentation would be
     appreciated but is not required.
  2. Altered source versions must be plainly marked as such, and must not be
     misrepresented as being the original software.
  3. This notice may not be removed or altered from any source distribution.
*/

#ifndef VECTOR3D_H
#define VECTOR3D_H

#include <iostream>
#include <iomanip>
#include <cmath>
#include "TurbineMath.h"

namespace Turbine
{

struct Vector3d
{

/** Member variables *********************************************************/
	float x, y, z;

/** Constructors *************************************************************/
	Vector3d() : x(0), y(0), z(0) {}

	Vector3d(float nx, float ny, float nz) : x(nx), y(ny), z(nz) {}

	Vector3d(const Vector3d& v) : x(v.x), y(v.y), z(v.z) {}

/** Operators ****************************************************************/

	Vector3d operator-() const { return Vector3d(-x, -y, -z); }

	Vector3d operator=(const Vector3d& v) { x=v.x; y=v.y; z=v.z; return *this; }

	Vector3d operator+(const Vector3d& v) const { return Vector3d(x + v.x, y + v.y, z + v.z); }
	Vector3d operator+=(const Vector3d& v) { x+=v.x; y+=v.y; z+=v.z; return *this; }

	Vector3d operator-(const Vector3d& v) const { return Vector3d(x - v.x, y - v.y, z - v.z); }
	Vector3d operator-=(const Vector3d& v) { x-=v.x; y-=v.y; z-=v.z; return *this; }

	Vector3d operator*(const Vector3d& v) const { return Vector3d(x * v.x, y * v.y, z * v.z); }
	Vector3d operator*=(const Vector3d& v) { x*=v.x; y*=v.y; z*=v.z; return *this; }
	Vector3d operator*(const float v) const { return Vector3d(x * v, y * v, z * v); }
	Vector3d operator*=(const float v) { x*=v; y*=v; z*=v; return *this; }

	Vector3d operator/(const Vector3d& v) const { return Vector3d(x / v.x, y / v.y, z / v.z); }
	Vector3d operator/=(const Vector3d& v) { x/=v.x; y/=v.y; z/=v.z; return *this; }
	Vector3d operator/(const float v) const { float i=(float)1.0/v; return Vector3d(x * i, y * i, z * i); }
	Vector3d operator/=(const float v) { float i=(float)1.0/v; x*=i; y*=i; z*=i; return *this; }

	bool operator<=(const Vector3d& v) const { return x<=v.x && y<=v.y && z<=v.z; }
	bool operator>=(const Vector3d& v) const { return x>=v.x && y>=v.y && z>=v.z; }
	bool operator<(const Vector3d& v) const { return x<v.x && y<v.y && z<v.z; }
	bool operator>(const Vector3d& v) const { return x>v.x && y>v.y && z>v.z; }

	bool operator==(const Vector3d& v) const { return x==v.x && y==v.y && z==v.z; }

	bool operator!=(const Vector3d& v) const { return x!=v.x || y!=v.y || z!=v.z; }

    friend std::ostream& operator<<(std::ostream& os, const Vector3d& v)
    {
        os.precision(3);
        os.setf(std::ios::right | std::ios::showpoint);
        os << "{ ";
        os << std::fixed << v.x;
        os << ", ";
        os << std::fixed << v.y;
        os << ", ";
        os << std::fixed << v.z;
        os << " }" << std::endl;

        return os;
    }

/** Set **********************************************************************/
	void set(const float nx, const float ny, const float nz) {x=nx; y=ny; z=nz; }

    void set(const Vector3d& v) { x=v.x; y=v.y; z=v.z;}

/** Product ******************************************************************/
	float dotProduct(const Vector3d v) const { return x*v.x + y*v.y + z*v.z; }

	Vector3d crossProduct(const Vector3d v) const { return Vector3d(y*v.z-z*v.y, z*v.x-x*v.z, x*v.y-y*v.x); }

/** Normalize ****************************************************************/
	Vector3d normalize()
	{
		float l = x*x + y*y + z*z;
		if (l == 0)
			return *this;

		l = (float) (1.0f/sqrt(l));
		x *= l;
		y *= l;
		z *= l;
		return *this;
	}

/** Length *******************************************************************/
	void setLength(float newlength)
	{
		normalize();
		*this *= newlength;
	}

	float getLength() const { return (float) sqrt(x*x + y*y + z*z); }

	float getLengthSQ() const { return x*x + y*y + z*z; }

/** Point related ************************************************************/
	float getDistanceFromPoint(const Vector3d p) const{ return Vector3d(x-p.x, y-p.y, z-p.z).getLength(); }

	float getDistanceFromPointSQ(const Vector3d p) const { return Vector3d(x-p.x, y-p.y, z-p.z).getLengthSQ(); }

	bool isBetweenPoints(const Vector3d begin, const Vector3d end) const
	{
		float f = (end - begin).getLengthSQ();
		return getDistanceFromPointSQ(begin) < f && getDistanceFromPointSQ(end) < f;
	}

    void lerpAt( const Vector3d a, const Vector3d b, const float t )
    {
        x = a.x + t * (b.x - a.x);
        y = a.y + t * (b.y - a.y);
        z = a.z + t * (b.z - a.z);
    }

/** Invert *******************************************************************/
	void invert()
	{
		x *= -1.0f;
		y *= -1.0f;
		z *= -1.0f;
	}
	
/** PolarCoordinates ***************************************************/
	void makeCartesianFromPolar(float theta, float phi, float ro)
	{
		theta = theta * PI / 180.0f;
		phi = phi * PI / 180.0f;
		
      x = ro * std::sin(theta) * std::cos(phi);
      y = ro * std::sin(theta) * std::sin(phi);
      z = ro * std::cos(theta);
	}
	
	float getRoFromCartesian()
	{
		if(x==0 && y==0 && z==0)
			return 0;
		else
			return sqrtf(x*x + y*y + z*z);
	}
	
	float getPhiFromCartesian()
	{
		if(x!=0)
			return std::atan(y/x);
		else
			return 0;
	}
	
	float getThetaFromCartesian()
	{
		if(getRoFromCartesian()!=0)
			return std::acos(z/getRoFromCartesian());
		else
			return 0;
	}
};

}

#endif /* VECTOR3D_H */
