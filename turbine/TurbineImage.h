/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef IMAGE_H
#define IMAGE_H

#include <vector>
#include "TurbineQuad.h"
#include "TurbineMaterial.h"

namespace Turbine
{

enum ImageType
{
    RGB_24BIT_U,
    RGBA_24BIT_U,
    RGBA_32BIT_U,
};

struct Image
{
    int index;
    ImageType type;
    unsigned long width;
    unsigned long height;
    unsigned char *data;
};

struct ImageSet
{
    std::vector<Quad> regions;
    Material mat;           // contains charmap texture
    int width;
    int height;
};

}

#endif
