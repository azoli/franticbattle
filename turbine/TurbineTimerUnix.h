/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef TURBINETIMERUNIX_H
#define TURBINETIMERUNIX_H

#include <sys/time.h>

#include "TurbineTimer.h"

namespace Turbine
{

class TimerUnix : public Timer
{
    struct timeval start;

    public:

    TimerUnix();
    ~TimerUnix();

    void reset();
    float getElapsedSeconds();
};

}

#endif
