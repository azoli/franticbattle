/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef CAMERA_H
#define CAMERA_H

#include "TurbineIObject.h"
#include "TurbineRenderer.h"

namespace Turbine
{

class Camera : public IObject
{

public:

/** Constructor/Destructor ***************************************************/
    Camera( const std::string& name, unsigned int id, Renderer *renderer,
            const Vector3d& position = Vector3d(0,0,-10.0f),
            const Vector3d& lookat = Vector3d(0,0,0),
            const Vector3d& upVector = Vector3d(0,1.0f,0) )
          : IObject(name, id, position), _renderer(renderer),
            _lookat(lookat), _upVector(upVector), _bUpdateMatrix(true)
    {
        cartesianToSpherical();
    }

    ~Camera() {}

/** View *********************************************************************/
    void setLookatViewMatrix(const Vector3d& position, const Vector3d& lookat, const Vector3d& upVector);
    const Matrix4& getViewMatrix();

    void setPosition(const Vector3d& newpos);

    void setLookAt(const Vector3d& lookat);
    const Vector3d& getLookAt();

    void setUpVector(const Vector3d& upVector);
    const Vector3d& getUpVector();

/** Spheric system ***********************************************************/
    void zoom(float size);
    float getZoom();

    void rotateOriz(float degrees);
    float getOrizDegrees();

    void rotateVert(float degrees);
    float getVertDegrees();

private:

	Renderer* _renderer;

	Matrix4 _viewMatrix;

	Vector3d _pos;
	Vector3d _lookat;
	Vector3d _upVector;

	void cartesianToSpherical(void);
	void sphericalToCartesian(void);

	float _theta;
	float _phi;
	float _ro;
	
    bool _bUpdateMatrix;
};

}

#endif /* CAMERA_H */
