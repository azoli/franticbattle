/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "TurbineFontBuilder.h"

#define DEBUG_FONT 0

using namespace std;

namespace Turbine
{

bool FontBuilder::init()
{
    if(!FT_Init_FreeType(&library));
        return false;
    return true;
}

FontInstance* FontBuilder::build(const string& pathname, int size)
{
    // check loaded font/size
    for(unsigned int i=0; i<_loadedFonts.size(); i++)
        if((_loadedFonts[i]->pathname.compare(pathname)==0) && (_loadedFonts[i]->size==size)) return _loadedFonts[i];

    FontInstance* font = new FontInstance();
    font->pathname = pathname;
    font->size = size;
    font->charmap = new Image();

    // create new face
    int error = FT_New_Face(library, pathname.c_str(), 0, &face);
    if(error == FT_Err_Unknown_File_Format)
    {
#ifdef DEBUG_FONT
        cout << "font format not supported" << endl;
#endif
        return NULL;
    }
    else if(error)
    {
        cout << "the font file could not be opened, read, or simply that it is broken" << endl;
        return NULL;
    }

    // set pixel size
    if(FT_Set_Pixel_Sizes(face, 0, size) != 0)
    {
#ifdef DEBUG_FONT
        cout << "cannot set pixel size" << endl;
#endif
        return false;
    }

    FT_GlyphSlot slot = face->glyph;

    int width = 0;
    int height = 0;

    int rowWidth = 0;
    int rowHeight = 0;
    int rowBeginIndex = 0;

    font->metrics[0].pos.x = 0;
    // for each character
    for (int n = 0; n < NUM_CHARS; n++)
    {
        // load character
        FT_Load_Char(face, _charmapStr[n], FT_LOAD_RENDER);
        FT_Glyph_Metrics sm = slot->metrics;

        // set glyph metrics
        CharacterMetrics& m = font->metrics[n];
        m.advances = sm.horiAdvance >> 6;
        m.bearX = sm.horiBearingX >> 6;
        m.bearY = -((sm.height-sm.horiBearingY) >> 6);
        m.width = sm.width >> 6;
        m.height = sm.height >> 6;

        // if newline
        if(m.width+rowWidth > 400)
        {
            m.pos.x = 0;

            // update charmap size
            height += rowHeight;
            if(rowWidth > width)
                width = rowWidth;

            // set the previous row pos.y
            for(int i=rowBeginIndex; i<n; i++)
            {
                font->metrics[i].pos.y = height;
            }
            rowBeginIndex = n;

            rowHeight = 0;
            rowWidth = 0;
        }
        else if(n > 0)
        {
            CharacterMetrics& mprec = font->metrics[n-1];
            m.pos.x = mprec.pos.x + mprec.width;
        }

        // update charmap width and height
        rowWidth += sm.width >> 6;
        if(rowHeight < (sm.height >> 6))
            rowHeight = sm.height >> 6;
    }

    // update charmap size
    height += rowHeight;
    if(rowWidth > width)
        width = rowWidth;

    // set the last row pos.y
    for(int i=rowBeginIndex; i<NUM_CHARS; i++)
    {
        font->metrics[i].pos.y = height;
    }

    // allocate bitmap
    font->charmap->data = new unsigned char[width*height*4*NUM_CHARS];
    memset(font->charmap->data, 0, width*height*4*NUM_CHARS);

    // convert freetype bitmap into Image* bitmap
    for(int n = 0; n < NUM_CHARS; n++)
    {
        FT_Load_Char(face, _charmapStr[n], FT_LOAD_RENDER);
        CharacterMetrics& m = font->metrics[n];
        for(int y = 0; y < slot->bitmap.rows; ++y)
        {
            for(int x = 0; x < slot->bitmap.width; ++x)
            {
                int y_inv = slot->bitmap.rows-1 - y;
                int inOffset = slot->bitmap.width*y_inv + x;
                int outOffset = (height-m.pos.y + y)*width + m.pos.x + x;

                // freetype font bitmap is 8 bit grayscale
                unsigned char c = face->glyph->bitmap.buffer[inOffset];

                // font->data is stored as rgba (8_8_8_8) so it must be copied 4 times
                font->charmap->data[4 * outOffset] = c;
                font->charmap->data[4 * outOffset + 1] = c;
                font->charmap->data[4 * outOffset + 2] = c;
                font->charmap->data[4 * outOffset + 3] = c;
            }
        }
    }

    TextureLoader* tloader = TextureLoader::getInstance();
    font->charmap->index = tloader->getFreeIndex();
    font->charmap->width = width;
    font->charmap->height = height;
    font->charmap->type = RGBA_32BIT_U;

    return font;
}

}
