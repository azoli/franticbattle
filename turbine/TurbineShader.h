/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef SHADER_H
#define SHADER_H

#ifdef _WIN32
#include <windows.h>
#include <gl/gl.h>
#elif __unix__
#include <gl.h>
#endif
#include "TurbineGLFunctions.h"

#include<string.h>
#include <map>

namespace Turbine
{

const int MAX_LIGHTS = 8;
const int MAX_BONES = 80;

void printErrors(const std::string& glfunc);

class Shader
{
    const std::string _name;

    std::string _vsSrc;
    std::string _fsSrc;

    GLuint _handle;
    GLuint _fs;
    GLuint _vs;

    void _throwCompileErrors(GLuint handle);
    void _throwLinkingErrors();

    std::map<std::string, GLint> uniforms;

    public:

    Shader(const std::string name, std::string fragShaderSrc, std::string vertShaderSrc);
    const std::string getName(){ return _name; }
    unsigned int getHandle(){ return _handle; }
    int getUniform(std::string name);
    void addUniformName(std::string name); //workaround function for driver bug on mat4 finalMatrix[80]

    bool build();
    void use();
};

}

#endif

