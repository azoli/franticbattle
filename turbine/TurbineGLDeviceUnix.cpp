/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <glx.h>
#include <X11/Xlib.h>
#include <iostream>

#include "TurbineGLDeviceUnix.h"

using namespace std;

namespace Turbine
{

bool GLDeviceUnix::init(IWindow *window)
{
	win = (WindowUnix*)window;

	dev = win->getDevice();
	handle = win->getHandle();

	XWindowAttributes attributes;
	XGetWindowAttributes(dev, handle, &attributes);

	int screen = XScreenNumberOfScreen(attributes.screen);

	int colorbits = win->getBitsPerPixel();

	int attrib[] = { GLX_RGBA,
                     GLX_RED_SIZE, colorbits, GLX_GREEN_SIZE, colorbits, GLX_BLUE_SIZE, colorbits,
                     GLX_DEPTH_SIZE, win->getDepthBits(), GLX_DOUBLEBUFFER, None };

	XVisualInfo* vinfo;
	vinfo = glXChooseVisual( dev, screen, attrib );
    if( !vinfo )
	{
		cerr << "Error matching visual info" << endl;
		return false;
	}

	ctx = glXCreateContext(dev, vinfo, 0, GL_TRUE);
	if(!ctx)
	{
		cerr << "Error glXCreateContext" << endl;
		return false;
	}

	glXMakeCurrent(dev, handle, ctx);

	int major,minor;
	glXQueryVersion(dev, &major, &minor);
	cout << "GLX version " << major << "." << minor << endl;

	if (glXIsDirect(dev, ctx))
		cout << "Direct Rendering: on" << endl;
	else
		cout << "Direct Rendering: off" << endl;

	return true;
}

bool GLDeviceUnix::reset()
{
	// TODO
	return true;
}

bool GLDeviceUnix::release()
{
	bool correct = true;

	if(ctx)
	{
		if( !glXMakeCurrent(dev, None, NULL) )
		{
			correct = false;
			cerr << "Could not release drawing context" << endl;
		}

		glXDestroyContext(dev, ctx);
		ctx = 0;
	}

	return correct;
}

void* GLDeviceUnix::getProcAddress(std::string funcName)
{
    return (void*)glXGetProcAddress((const GLubyte*)funcName.c_str());
}

void GLDeviceUnix::swapBuffers()
{
	glXSwapBuffers(dev, handle);
}

}
