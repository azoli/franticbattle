/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <string>
#include "TurbineSceneManager.h"

using namespace std;

namespace Turbine
{

SceneManager::SceneManager(Renderer* renderer) : _renderer(renderer), _activeCamera(NULL), _newID(1)
{}

SceneManager::~SceneManager()
{
    list<Camera*>::iterator camera;
    for(camera = cameraList.begin(); camera != cameraList.end(); camera++)
        delete (*camera);
    cameraList.clear();

    list<Light*>::iterator light;
    for(light = lightList.begin(); light != lightList.end(); light++)
        delete (*light);
    lightList.clear();

    list<Entity*>::iterator entity;
    for(entity = entityList.begin(); entity != entityList.end(); entity++)
        delete (*entity);
    entityList.clear();
}

Model* SceneManager::loadModel(const string& filename)
{
    Model* model = NULL;
    string file(filename);
    size_t pos = file.find_last_of(".");

    string extension = file.substr(pos);

    if(extension.compare(".obj")==0)
        model = objLoader.load(filename);

    else if(extension.compare(".vbo")==0)
        model = vboLoader.load(filename);

    if(model != NULL)
    {
        model->initBBox();
        _renderer->createHardwareBuffers(model);
    }

    return model;
}

void SceneManager::destroyModel(Model* model)
{
    if(!model)
        return;

    _renderer->destroyHardwareBuffers(model);
    delete model;
}

Model* SceneManager::createQuadsModel(const vector<Quad>& quads, const Material& mat)
{
    Model* model = new Model();
    model->appendStaticMeshData(quads, mat);
    _renderer->createHardwareBuffers(model);

    return model;
}

Camera* SceneManager::createCamera(const string& name, bool active,
                                   const Vector3d& position,
                                   const Vector3d& lookat,
                                   const Vector3d& upVector)
{
    Camera* camera = new Camera(name, _newID, _renderer, position, lookat, upVector);
    _newID++;

    if(active)
        setActiveCamera(camera);

    cameraList.push_back(camera);

    return camera;
}

Camera* SceneManager::getCamera(const string& name)
{
    list<Camera*>::iterator it = cameraList.begin();
    while( (it != cameraList.end()) && ((*it)->getName().compare(name)) ) it++;
    if(it != cameraList.end())
        return (*it);

    return NULL;
}

bool SceneManager::hasCamera(const string& name)
{
    list<Camera*>::iterator it = cameraList.begin();
    while( (it != cameraList.end()) && ((*it)->getName().compare(name)) ) it++;
    if(it != cameraList.end())
        return true;

    return false;
}

void SceneManager::destroyCamera(Camera* camera)
{
    cameraList.remove(camera);
    delete camera;
}


void SceneManager::setActiveCamera(Camera* camera)
{
    _activeCamera = camera;
}

Camera* SceneManager::getActiveCamera()
{
    return _activeCamera;
}

Light* SceneManager::createLight(const string& name, LIGHT_TYPE type,
                                 const Vector3d& position,
                                 Colorf color, float radius)
{
    Light* light = new Light(name, _newID, type, position, color, radius);
    _newID++;
    lightList.push_back(light);

    return light;
}

Light* SceneManager::getLight(const string& name)
{
    list<Light*>::iterator it = lightList.begin();
    while( (it != lightList.end()) && ((*it)->getName().compare(name)) ) it++;
    if(it != lightList.end())
        return (*it);

    return NULL;
}

bool SceneManager::hasLight(const string& name)
{
    list<Light*>::iterator it = lightList.begin();
    while( (it != lightList.end()) && ((*it)->getName().compare(name)) ) it++;
    if(it != lightList.end())
        return true;

    return false;
}

void SceneManager::destroyLight(Light* light)
{
    lightList.remove(light);
    delete light;
}

Entity* SceneManager::createEntity(const string& name, Model* model,
                                  const Vector3d& position,
                                  const Vector3d& rotation,
                                  const Vector3d& scale)
{
    Entity* entity = new Entity(name, _newID, model, position, rotation, scale);
    _newID++;
    entityList.push_back(entity);

    return entity;
}

Entity* SceneManager::getEntity(const string& name)
{
    list<Entity*>::iterator it = entityList.begin();
    while( (it != entityList.end()) && (name.compare((*it)->getName())) ) it++;
    if(it != entityList.end())
        return (*it);

    return NULL;
}

bool SceneManager::hasEntity(const string& name)
{
    list<Entity*>::iterator it = entityList.begin();
    while( (it != entityList.end()) && ((*it)->getName().compare(name)) ) it++;
    if(it != entityList.end())
        return true;

    return false;
}

void SceneManager::destroyEntity(Entity* entity)
{
    entityList.remove(entity);
    delete entity;
}

Entity* SceneManager::selectEntity(int x, int y)
{
    unsigned int id = _renderer->pickupColor(x, y);

    list<Entity*>::iterator entity = entityList.begin();
    while( (entity != entityList.end()) && ((*entity)->getID() != id) ) entity++;
    if(entity != entityList.end())
        return (*entity);

    return NULL;
}

void SceneManager::updateTime(float delta)
{
    std::list<Entity*>::iterator entity = entityList.begin();
    for(; entity != entityList.end(); entity++)
        (*entity)->updateTime(delta);
}

void SceneManager::drawAll()
{
    if (!_renderer)
        return;

    Matrix4 identity;
    _renderer->setTransform ( TS_VIEW, identity );
    _renderer->setTransform ( TS_MODEL, identity );

    // render camera
    if(_activeCamera && _activeCamera->isEnabled())
        _renderer->setTransform(TS_VIEW, _activeCamera->getViewMatrix());

    // render lights
    list<Light*>::iterator light;
    for(light = lightList.begin(); light!=lightList.end(); light++)
    {
        if((*light)->isEnabled())
            _renderer->drawLight((*light));
    }

    // render entities
    list<Entity*>::iterator entity;
    for(entity = entityList.begin(); entity!=entityList.end(); entity++)
    {
        if((*entity)->isEnabled())
        {
            // update model matrix
            Matrix4 mat = (*entity)->getTransformation();
            _renderer->setTransform(TS_MODEL, mat);

           // draw model
            _renderer->drawEntity((*entity));
        }
    }
}

}
