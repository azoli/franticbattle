/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef GLDEVICEUNIX_H
#define GLDEVICEUNIX_H

#ifdef _WIN32
#include <gl/glx.h>
#elif __unix__
#include <glx.h>
#endif

#include "TurbineIWindow.h"
#include "TurbineWindowUnix.h"
#include "TurbineGLDevice.h"

namespace Turbine
{

/** GLDeviceUnix *************************************************************/
class GLDeviceUnix : public GLDevice
{
	GLXContext ctx;

	WindowUnix *win;
	Window handle;
	Display *dev;

public:
	GLDeviceUnix(){}
	~GLDeviceUnix(){}

	bool init(IWindow *window);
	bool reset();
	bool release();

    void* getProcAddress(std::string funcName);

	void swapBuffers();
};

}

#endif /* GLDEVICEUNIX_H */
