/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <fstream>
#include <string.h>
#include <stdlib.h>
#ifdef _WIN32
#elif __unix__
#include <libgen.h>
#endif
#include "TurbineObjLoader.h"

using namespace std;

namespace Turbine
{

void ObjLoader::init()
{
    vertexList.clear();
    normalList.clear();
    texCoordList.clear();
    materialList.clear();

    // default material
    Material mat;
    materialList.push_back(mat);

    triangleList.clear();
    smoothGroups.clear();
    texImages.clear();
    vertexCount = normalCount = texCoordCount = materialCount = triangleCount = textureCount = 0;
    hasTexCoords = false;
    hasNormals = true;
    matMap.clear();
}

Model* ObjLoader::load(const string& sfile)
{
    init();

    int i, num, t;
    char buf[256], *pbuf;
    char tmp[256], mtlFilename[256], materialName[256];
    int currMaterial;

    Vector3d tmp3;
    Vector2d tmp2;

    ObjTriangle triangle;
    ObjQuad quad;

    SmoothingGroup group;

    ifstream scene(sfile.c_str(), ifstream::in);
    if(!scene.is_open())
    {
        cout << "Cannot open file " << sfile << endl;
        return NULL;
    }

    scenefile = sfile.c_str();

    currMaterial = 0;

    /** First pass: test file *************************************************/
    while (!scene.eof())
    {
        scene.getline(buf,256);

        switch (buf[0])
        {
        case 'v':
        {
            //test 3d vertex
            if (buf[1] == ' ')
            {
                vertexCount++;

                if (sscanf (buf + 2, "%f %f %f", &tmp3.x, &tmp3.y, &tmp3.z) != 3)
                {
                    cerr << "only 3d vertex data supported (xyz)!" << endl;
                    return NULL;
                }
            }
            //test 2d texture coordinates
            else if (buf[1] == 't')
            {
                texCoordCount++;

                if (sscanf (buf + 2, "%f %f", &tmp2.x, &tmp2.y) != 2)
                {
                    cerr << "only 2d texture data supported (uv)!" << endl;
                    return NULL;
                }
            }
            //test 3d normal
            else if (buf[1] == 'n')
            {
                normalCount++;

                if (sscanf (buf + 2, "%f %f %f", &tmp3.x, &tmp3.y, &tmp3.z) != 3)
                {
                    cerr << "only 3d normal data supported (ijk)!" << endl;
                    return NULL;
                }
            }
            else
                cout << "warning: unknown token " << buf << "! (ignoring)" << endl;

            break;
        }

        case 'f':
        {
            pbuf = buf;
            num = 0;

            // count number of vertices for this face
            while (*pbuf)
            {
                if( ((*pbuf) == ' ') && ((*(pbuf+1)) != ' ') && ((*(pbuf+1)) != '\r') )
                    num++;

                pbuf++;
            }

            if (num == 4)
            {
                triangleCount+=2;
            }
            else if (num == 3)
            {
                triangleCount++;
            }
            else
            {
                cerr << "error: geometry not supported, supported only quad and triangle" << endl;
                cout << num << " vertices in " << buf << endl;
                return NULL;
            }

            //test vertex indices
            if (sscanf (buf + 2, "%d/%d/%d", &t, &t, &t) == 3)
            {
                hasTexCoords = 1;
                hasNormals = 1;
            }
            else if (sscanf (buf + 2, "%d//%d", &t, &t) == 2)
            {
                hasNormals = 1;
            }
            else if (sscanf (buf + 2, "%d/%d", &t, &t) == 2)
            {
                hasTexCoords = 1;
            }
            else if (sscanf (buf + 2, "%d", &t) != 1)
            {
                cerr << "error: found face with no vertex!" << endl;
                return NULL;
            }
            break;
        }

        case 'm':
        {
            if( sscanf(buf, "%s %s", tmp, mtlFilename) == 2 && strcmp(tmp, "mtllib") == 0 )
            {
                char *dir = new char [scenefile.size()+1];
                strcpy (dir, scenefile.c_str());
                string file = dir;
                file.append("/");
#ifdef _WIN32
                /* TODO compute dirname and basename */
#elif __unix__
                dir = dirname(dir);
                file.append(basename(mtlFilename));
#endif
                delete[] dir;

                if(!loadMtl(file.c_str()))
                {
                    cout << "error: cannot load mtl file " << file << endl;
                    return NULL;
                }
            }
            else
                cout << "warning: unknown token " << buf << "! (ignoring)" << endl;

            break;
        }

        case 'u':
        {
            if( sscanf(buf, "%s %s", tmp, materialName) == 2 && strcmp(tmp, "usemtl") == 0 )
            {
                map<string,int>::iterator it = matMap.find(materialName);
                if( it == matMap.end() )
                {
                    cout << "error: material " << materialName << " not defined" << endl;
                    return NULL;
                }
            }
            else
                cout << "warning: unknown token " << buf << "! (ignoring)" << endl;

            break;
        }

        default:
            break;
        }
    }

    if ((hasTexCoords && !texCoordCount) ||	(hasNormals && !normalCount))
    {
        cerr << "error: contradiction between collected info!" << endl;
        return NULL;
    }

    if (!vertexCount)
    {
        cerr << "error: no vertex found!\n" << endl;
        return NULL;
    }

    /** Second pass: loading model ********************************************/
    scene.clear();
    scene.seekg(0, ios::beg);

    currMaterial = 0;

    vertexList.push_back(tmp3);
    texCoordList.push_back(tmp2);
    normalList.push_back(tmp3);

    while (!scene.eof())
    {
        scene.getline(buf,256);

        switch (buf[0])
        {
        case 'v':
        {
            if (buf[1] == ' ')
            {
                pbuf = strchr(buf, ' ');
                while(pbuf[0] == ' ') pbuf++;

                sscanf (pbuf, "%f %f %f", &tmp3.x, &tmp3.y, &tmp3.z);
                vertexList.push_back(tmp3);
            }

            else if (buf[1] == 't')
            {
                sscanf (buf + 2, "%f %f", &tmp2.x, &tmp2.y);
                texCoordList.push_back(tmp2);
            }

            else if (buf[1] == 'n')
            {
                sscanf (buf + 2, "%f %f %f", &tmp3.x, &tmp3.y, &tmp3.z);
                normalList.push_back(tmp3);
            }

            break;
        }

        case 'f':
        {
            pbuf = buf;
            num = 0;

            // count number of vertices for this face
            while (*pbuf)
            {
                if( ((*pbuf) == ' ') && ((*(pbuf+1)) != ' ') && ((*(pbuf+1)) != '\r') )
                    num++;

                pbuf++;
            }

            // read face data
            pbuf = buf;

            if(num == 4)
            {
                i=0;
                while(i<4)
                {
                    pbuf = strchr(pbuf, ' ');
                    pbuf++;
                    if(pbuf[0] == ' ') continue;		//additional spaces

                    //test vertex indices
                    if (sscanf (pbuf, "%d/%d/%d", &quad.v[i].v, &quad.v[i].n, &quad.v[i].t) != 3)
                        if (sscanf (pbuf, "%d//%d", &quad.v[i].v, &quad.v[i].n) != 2)
                            if (sscanf (pbuf, "%d/%d", &quad.v[i].v, &quad.v[i].t) != 2)
                                sscanf (pbuf, "%d", &quad.v[i].v);
                    i++;
                }

                group.end+=2;
                ObjTriangle t1 = ObjTriangle( quad.v[0], quad.v[1], quad.v[2] );
                ObjTriangle t2 = ObjTriangle( quad.v[0], quad.v[2], quad.v[3] );
                triangleList.push_back(t1);
                triangleList.push_back(t2);
            }
            else if(num == 3)
            {
                i=0;
                while(i<3)
                {
                    pbuf = strchr(pbuf, ' ');
                    pbuf++;
                    if(pbuf[0] == ' ') continue;		//additional spaces

                    //test vertex indices
                    if (sscanf (pbuf, "%d/%d/%d", &triangle.v[i].v, &triangle.v[i].n, &triangle.v[i].t) != 3)
                        if (sscanf (pbuf, "%d//%d", &triangle.v[i].v, &triangle.v[i].n) != 2)
                            if (sscanf (pbuf, "%d/%d", &triangle.v[i].v, &triangle.v[i].t) != 2)
                                sscanf (pbuf, "%d", &triangle.v[i].v);


                    i++;
                }

                group.end++;
                triangleList.push_back(triangle);
            }
            break;
        }

        case 'u':
        {
            if(group.start != group.end)
            {
                smoothGroups.push_back(group);
                group.start = group.end;
            }

            if( sscanf(buf, "%s %s", tmp, materialName) == 2 && strcmp(tmp, "usemtl") == 0 )
                group.mtlIndex = matMap[materialName];

            break;
        }

        default:
            break;
        }
    }

    if(group.start != group.end)
        smoothGroups.push_back(group);

    scene.close();

    return toVBO();
}

bool ObjLoader::loadMtl(const string& mtlfile)
{
    char token[256], buf[256];
    int currMaterial;

    ifstream mtl(mtlfile, ifstream::in);
    if(!mtl.is_open())
        return false;

    while (!mtl.eof())
    {
        mtl >> token;

        if(strcmp(token,"newmtl") == 0)
        {
            Material newMat;

            currMaterial = materialCount;

            mtl >> buf;

            matMap[string(buf)] = currMaterial;
            materialList.push_back(newMat);

            materialCount++;
        }
        else if(strcmp(token,"Ka") == 0)
        {
            Colorf ka;

//            mtl >> ka.r >> ka.g >> ka.b;
//            ka.a = 1;

//            materialList[currMaterial].ka = ka;
        }
        else if (strcmp(token,"Kd") == 0)
        {
            Colorf kd;

            mtl >> kd.r >> kd.g >> kd.b;
            kd.a = 1;

            materialList[currMaterial].kd = kd;
        }
        else if (strcmp(token,"Ks") == 0)
        {
            Colorf ks;

            mtl >> ks.r >> ks.g >> ks.b;
            ks.a = 1;

            materialList[currMaterial].ks = ks;
        }
        else if (strcmp(token,"Ns") == 0)
        {
            mtl >> materialList[currMaterial].ns;
        }
        else
            mtl.getline(buf, 256);
    }

    mtl.close();

    return true;
}

Model* ObjLoader::toVBO()
{
    Model* model = new Model();
    int newIdx;
    Vector2d tex;
    Vector3d norm, vert;
    vector<ObjVertex> vertexVector;
    list<SmoothingGroup>::iterator group;
    vector<Material>::iterator itm;

    for(itm = materialList.begin(); itm != materialList.end(); itm++)
    {
        model->materials.push_back((*itm));
    }

    model->scenefile = scenefile;
    model->isAnimated = false;

    // calculate VBO size (counter)
    int counter = 0;
    for(group = smoothGroups.begin(); group != smoothGroups.end(); group++)
    {
        for(int i = (*group).start; i<(*group).end; i++)
        {
            for (int j=0; j<3; j++)
            {
                ObjVertex idx = triangleList[i].v[j];

                bool found = false;
                for(unsigned int k=0; k<vertexVector.size(); k++)
                {
                    if(vertexVector[k] == idx)
                    {
                        found = true;
                        break;
                    }
                }

                if(!found)
                {
                    vertexVector.push_back(idx);
                    counter++;
                }
            }
        }
    }

    vertexVector.clear();

    // init VBO data
    int vCount = 0;
    int iCount = 0;
    Face face;

    // fill VBO structure
    // for each material
    for(group = smoothGroups.begin(); group != smoothGroups.end(); group++)
    {
        model->smoothGroups.push_back((*group));
        // iterate triangles with the same material
        for(int i = (*group).start; i<(*group).end; i++)
        {
            // for each vertex
            for (int j=0; j<3; j++)
            {
                ObjVertex idx = triangleList[i].v[j];

                // add the vertex if it is a new vertex
                newIdx=-1;
                for(unsigned int k=0; k<vertexVector.size(); k++)
                {
                    if(vertexVector[k] == idx)
                    {
                        newIdx = k;
                        break;
                    }
                }

                if(newIdx == -1)
                {
                    vertexVector.push_back(idx);

                    Vertex v;

                    v.coord = vertexList[idx.v];
                    if(hasNormals)
                    {
                        v.normal = normalList[idx.n];
                    }
                    else
                    {
                        //TODO generate normals if they doesn't exist..
                        cout << "cannot generate normals, not present in the .obj file" << endl;
                    }
                    if(hasTexCoords)
                        v.texCoord = texCoordList[idx.t];
                    else
                        v.texCoord = Vector2d(0.0, 0.0);

                    model->vertices.push_back(v);

                    newIdx = vCount;
                    vCount++;
                }

                // update indexes
                face.index[j] = newIdx;
                iCount++;
            }
            model->faces.push_back(face);
        }
    }

    return model;
}

}
