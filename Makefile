# general options
TURBINE_INCLUDEDIR = turbine/libturbine/include/
TURBINE_LIBDIR = turbine/libturbine/lib/
APP_SYSTEM_BIN = /usr/bin
APP_SYSTEM_DATA = /usr/share/FranticBattle
APP_LOCAL_BIN = build/
APP_LOCAL_DATA = build/data/

CXX = g++
OBJS = FBEventReceiver.o FBLevel.o FBMap.o FBUnit.o FBSimulator.o FBAttackNearest.o FranticBattle.o

# compiler options
HEADERS_DIR = $(TURBINE_INCLUDEDIR)
WARNINGS = -Wall
CXXFLAGS = -std=c++0x $(WARININGS) $(addprefix -I, $(HEADERS_DIR)) $(shell freetype-config --cflags)
DEBUG = yes
ifdef DEBUG
    CXXFLAGS += -O0 -g -ggdb
else
    CXXFLAGS += -O3
endif
MAKEDEPEND = $(CXX) -M $(CXXFLAGS) -o $*.d $<

# linker options
LIBS = X11 GL turbine freetype png
LIBS_DIR = $(TURBINE_LIBDIR)
LDLIBS = $(addprefix -l, $(LIBS))
LDFLAGS = $(addprefix -L, $(LIBS_DIR))

# explicit rules
all: FranticBattle_build vboviewer_build
	@echo FranticBattle has been successfully built

turbine_build:
	@cd turbine && $(MAKE)

vboviewer_build:
	@cd tools/vboviewer && $(MAKE)

local_init:
	@mkdir -p $(APP_LOCAL_BIN) $(APP_LOCAL_DATA)

local_install:
	@echo Installing FranticBattle files locally..
	@cp -ur models/* $(APP_LOCAL_DATA)
	@cp -ur fonts/* $(APP_LOCAL_DATA)
	@mv FranticBattle $(APP_LOCAL_BIN)FranticBattle
	@echo FranticBattle local installation complete

install:
	@echo Installing turbine files in the system (root permission required)
	@mkdir -p $(APP_SYSTEM_BIN) $(APP_SYSTEM_DATA)
	@cp -u $(APP_LOCAL_BIN)FranticBattle $(APP_SYSTEM_BIN)
	@cp -ur models/* $(APP_SYSTEM_DATA)
	@cp -ur fonts/* $(APP_SYSTEM_DATA)
	@echo turbine system installation complete

clean:
	rm -f *.o *.P *.*~ *~
	cd turbine && $(MAKE) clean
	cd tools/vboviewer && $(MAKE) clean

FranticBattle_build: turbine_build local_init FranticBattle local_install

FranticBattle: $(OBJS)

.PHONY: all local_init local_install install clean turbine_build FranticBattle_build vboviewer_build

# compile implicit rule
%.o : %.cpp
	@$(MAKEDEPEND) && \
        cp $*.d $*.P && \
        sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
            -e '/^$$/ d' -e 's/$$/ :/' < $*.d >> $*.P && \
        rm -rf $*.d
	$(CXX) -c $(CXXFLAGS) -o $@ $<
-include $(OBJS:.o=.P)

# linking implicit rule
% : %.o $(TURBINE_LIBDIR)libturbine.a
	$(CXX) $(LDFLAGS) -o$@ $(OBJS) $(LDLIBS)
