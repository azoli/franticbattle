/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include "FBAttackNearest.h"

//#define DEBUG 1

namespace FB
{

AttackNearest::AttackNearest(Map* map) : UnitStrategy(map)
{
    int row = _map->getRow();
    int col = _map->getColumn();

    // build coordinate map
    _coordMap.resize(row);
    for(int y=0; y<row; y++)
    {
        _coordMap[y].resize(col);
        for(int x=0; x<col; x++)
        {
            _coordMap[y][x].first = x;
            _coordMap[y][x].second = y;
        }
    }

    // setup ranges to hit at most row+1, col+i
    _ranges.push_back(0.0);
    for(int i=1; i<std::max(row, col)*1.5; i++)
        _ranges.push_back(std::sqrt(1.0 + i*i));
}

AttackNearest::~AttackNearest()
{
}

Event* AttackNearest::getNextAction(std::shared_ptr<Unit> unit, float time)
{
    int row = _map->getRow();
    int col = _map->getColumn();
    MapPosition pos = _map->findUnit(unit->getName());
    int width = std::max(row, col);

    // build a distance map
    std::vector<std::vector<int> > distMap;
    distMap.resize(row);
    for(int y=0; y<row; y++)
    {
        distMap[y].resize(col);
        for(int x=0; x<col; x++)
        {
            float dx = _coordMap[y][x].first - pos.x;
            float dy = _coordMap[y][x].second - pos.y;
            float dist = std::sqrt(std::pow(dx, 2) + std::pow(dy, 2));

            int i=0;
            while(_ranges[i] < dist)
                i++;

            distMap[y][x] = i;
        }
    }

#ifdef DEBUG
    std::cout << "distance map" << std::endl;
    for(int y=row-1; y>=0; y--)
    {
        for(int x=0; x<col; x++)
        {
            std::cout << distMap[y][x] << " ";
        }
        std::cout << std::endl;
    }
#endif

    // locate nearest enemy
    std::shared_ptr<Unit> enemy;
    int range = 0;
    while(range < width)
    {
        range++;

        for(unsigned int y=0; y<row; y++)
        {
            for(unsigned int x=0; x<col; x++)
            {
                if(distMap[y][x] == range)
                {
                    std::shared_ptr<Unit> other = _map->getUnit(MapPosition(x,y));
                    if(other && other->getTeamNum() != unit->getTeamNum() && other->getHP() > 0)
                    {
                        enemy = other;
                        goto Found;
                    }
                }
            }
        }
    }

    // if not found you have won!
    return new Event(EV_WINNER, unit, time);

    Found:
    // if the enemy is on range and it is not reloading, then attack it
    MapPosition enemypos = _map->findUnit(enemy->getName());
    if(range <= unit->getRange() )
    {
        if(unit->isReloading())
            return new Event(EV_IDLE, unit, time+0.1f);

        // before attack test unit orientation
        float rotation = _map->getRotation(unit->getName(), enemypos);
        if(Turbine::round(rotation) != 0.0)
            return new RotationEvent(unit, time, rotation);

        return new AttackEvent(unit, time, enemy);
    }

    // otherwise move toward him
    std::list<MapPosition> path = _map->findPath(pos, enemypos, true);

    // if i'm stuck use an invalid step so the simulator test step validity
    if(path.size() == 0)
    {
        MapPosition nearPos = pos;
        nearPos.x += (pos.x > 0) ? -1 : 1;
        return new MoveEvent(unit, time, nearPos);
    }

    // but before move test unit orientation
    std::list<MapPosition>::iterator startPos = path.begin();
    MapPosition step = *(++startPos);
    float rotation = _map->getRotation(unit->getName(), step);
    if(Turbine::round(rotation) != 0.0)
        return new RotationEvent(unit, time, rotation);


    // remove from the path the goal (the enemy position)
    path.pop_back();

    std::list<MapPosition>::iterator it = path.begin();
    return new MoveEvent(unit, time, *(++it));
}

}

