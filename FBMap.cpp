/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <cassert>
#include <queue>
#include <set>
#include <map>
#include <math.h>
#include <TurbineEngine.h>
#include <TurbineMath.h>
#include "FBMap.h"

using std::string;
using Turbine::Vector2d;
using Turbine::Vector3d;
using Turbine::degToRad;

//#define DEBUG 1

namespace FB
{

bool nodeMoreThen(const Node* left, const Node* right)
{
    return (left->F > right->F);
}

Node::Node(MapPosition pos, Node* par) : position(pos), parent(par), F(0), G(0), H(0)
{
}

Node::~Node()
{
}

Map::Map(Turbine::SceneManager* smgr) : _smgr(smgr)
{
    for(int y=0; y<rownum; y++)
        for(int x=0; x<colnum; x++)
            cells[y][x] = CELL_FREE;

    srand(time(NULL));
    buildTerrainModel();
}

Map::~Map()
{
    clear();
}

void Map::clear()
{
    for(int y=0; y<rownum; y++)
    {
        for(int x=0; x<colnum; x++)
        {
            if(cells[y][x] == CELL_OCCUPIED)
                units[y][x].reset();
            cells[y][x] = CELL_FREE;
        }
    }
}

bool Map::addUnit(std::shared_ptr<Unit> unit, MapPosition pos)
{
    if( (pos.x < 0) && (pos.x >= colnum) && (pos.y < 0) && (pos.y >= rownum) )
        return false;

    if(cells[pos.y][pos.x] == CELL_FREE)
    {
        units[pos.y][pos.x] = unit;
        cells[pos.y][pos.x] = CELL_OCCUPIED;
        return true;

    }

    return false;
}

MapPosition Map::addUnitRandomMapPosition(std::shared_ptr<Unit> unit)
{
    MapPosition pos;

    bool fullMap = true;
    for(int y=0; y<rownum; y++)
    {
        for(int x=0; x<colnum; x++)
        {
            if(cells[y][x] == CELL_FREE)
            {
                fullMap = false;
                break;
            }
        }
    }

    if(!fullMap)
    {
        do
        {
            pos.x = (int) rand() % colnum;
            pos.y = (int) rand() % rownum;
        } while(cells[pos.y][pos.x] != CELL_FREE);

        if(addUnit(unit, pos))
            return pos;
    }

    pos.setInvalid();
    return pos;
}

std::shared_ptr<Unit> Map::getUnit(MapPosition pos)
{
    if( (pos.x >= 0) && (pos.x < colnum) && (pos.y >= 0) && (pos.y < rownum) )
        if(cells[pos.y][pos.x] == CELL_OCCUPIED)
            return units[pos.y][pos.x];

    return std::shared_ptr<Unit>();
}

bool Map::removeUnit(std::shared_ptr<Unit> unit)
{
    MapPosition pos = findUnit(unit->getName());
    if(cells[pos.y][pos.x] == CELL_OCCUPIED)
    {
        cells[pos.y][pos.x] = CELL_FREE;
        units[pos.y][pos.x].reset();

        return true;
    }

    return false;
}

MapPosition Map::findUnit(std::string name)
{
    MapPosition pos;
    for(int y=0; y<rownum; y++)
    {
        for(int x=0; x<colnum; x++)
        {
            std::shared_ptr<Unit> unit = units[y][x];
            if((cells[y][x] == CELL_OCCUPIED) && (unit) && (name.compare(unit->getName()) == 0))
                return MapPosition(x, y);
        }
    }

    pos.setInvalid();
    return pos;
}

Vector3d Map::getCellCenter(MapPosition pos)
{
    float transx = -(int)(colnum / 2.0) * cellsize;
    float transz = (int)(rownum / 2.0) * cellsize;
    return Vector3d(transx + pos.x*cellsize, 0, transz - pos.y*cellsize);
}

void Map::buildTerrainModel()
{
    std::shared_ptr<Turbine::Engine> engine = Turbine::Engine::getInstance();
    Turbine::TextureLoader* tloader = Turbine::TextureLoader::getInstance();

    // Create tile model
    Turbine::Quad quad;

    quad.coords[0] = Vector3d(0.0f, 0.0f, 0.0f);
    quad.coords[1] = Vector3d(cellsize, 0.0f, 0.0f);
    quad.coords[2] = Vector3d(cellsize, 0.0f, -cellsize);
    quad.coords[3] = Vector3d(0.0f, 0.0f, -cellsize);

    for(int i=0; i<4; i++)
        quad.normals[i] = Vector3d(0.0f, 1.0f, 0.0f);

    quad.texCoords[0] = Vector2d(0.0f, 0.0f);
    quad.texCoords[1] = Vector2d(1.0f, 0.0f);
    quad.texCoords[2] = Vector2d(1.0f, 1.0f);
    quad.texCoords[3] = Vector2d(0.0f, 1.0f);

    Turbine::Material mat;
    mat.ks = Turbine::Colorf(0.0, 0.0, 0.0);
    mat.texture = tloader->loadPNG(engine->getAbsolutePath("terrain.png"));
    std::vector<Turbine::Quad> quads;
    quads.push_back(quad);
    Turbine::Model* terrainModel = _smgr->createQuadsModel(quads, mat);

    // Create col x row tiles entities
    float transx = -(colnum/2.0f)*cellsize;
    float transz = (rownum/2.0f)*cellsize;
    Vector3d position = Vector3d(transx, 0, transz);
    for(int row=0; row<rownum; row++)
    {
        for(int col=0; col<colnum; col++)
        {
            Vector3d offset = Vector3d(row*cellsize, 0, -col*cellsize);

            char name[256];
            sprintf(name, "!cell %d %d", row, col);
           _smgr->createEntity( name, terrainModel, position+offset );
        }
    }
}

bool Map::lockCell(MapPosition pos)
{
    assert( (pos.x >= 0) && (pos.x < colnum) && (pos.y >= 0) && (pos.y < rownum) );

    if(cells[pos.y][pos.x] == CELL_FREE)
    {
        cells[pos.y][pos.x] = CELL_LOCKED;
        return true;
    }

    return false;
}

void Map::unlockCell(MapPosition pos)
{
    assert( (pos.x >= 0) && (pos.x < colnum) && (pos.y >= 0) && (pos.y < rownum) );

    if(cells[pos.y][pos.x] == CELL_LOCKED)
        cells[pos.y][pos.x] = CELL_FREE;
}

std::list<MapPosition> Map::findPath(MapPosition start, MapPosition goal, bool skipLast)
{
#ifdef DEBUG
    std::cout << "Find path from " << start.x << " " << start.y << " and " << goal.x << " " << goal.y << std::endl;

#endif

    std::priority_queue<Node*, std::vector<Node*>, NodeMoreThen> openQueue; // nodes to be evaluated
    std::map<MapPosition, Node*> openMap;   // table lookup for nodes
    std::map<MapPosition, Node*> closedMap; // already evaluated positions
    std::list<MapPosition> path;            // the final path

    Node* node = new Node(start, 0);
    openQueue.push(node);
    openMap[start] = node;

    while( openQueue.size() > 0 )
    {
        // get the current square (lower F value)
        Node* current = openQueue.top();
#ifdef DEBUG
        std::cout << "current " << current->position.x << " " << current->position.y << std::endl;
#endif

        // if it is the goal
        if(current->position == goal)
        {
            // get path walking back from node parents
            while(current != 0)
            {
                path.push_front(current->position);
                current = current->parent;
            }
            break;
        }

        // move current to the closeList
        openQueue.pop();
        closedMap[current->position] = current;
        openMap.erase(current->position);

        // get the up-to-8 neighbor
        MapPosition square;
        std::vector<MapPosition> neighbors;

        // left square
        square = current->position + MapPosition(-1, 0);
        if(square.x >= 0)
            neighbors.push_back(square);

        // right square
        square = current->position + MapPosition(1, 0);
        if(square.x < colnum)
            neighbors.push_back(square);

        // top square
        square = current->position + MapPosition(0, 1);
        if(square.y < rownum)
            neighbors.push_back(square);

        // bottom square
        square = current->position + MapPosition(0, -1);
        if(square.y >= 0)
            neighbors.push_back(square);

        // bottom-left square
        square = current->position + MapPosition(-1, -1);
        if(square.x >= 0 && square.y >= 0)
            neighbors.push_back(square);

        // bottom-right square
        square = current->position + MapPosition(1, -1);
        if(square.x < colnum && square.y >= 0)
            neighbors.push_back(square);

        // top-left square
        square = current->position + MapPosition(-1, 1);
        if(square.x >= 0 && square.y < rownum)
            neighbors.push_back(square);

        // top-right square
        square = current->position + MapPosition(1, 1);
        if(square.x < colnum && square.y < rownum)
            neighbors.push_back(square);

        // for each neighbor
        for(unsigned int i=0; i<neighbors.size(); i++)
        {
            // if it is already evaluated skip
            if(closedMap.count(neighbors[i]))
                continue;

            // if it is not empty (or it is the goal neighbors and skipLast is true)
            if(!skipLast || (skipLast && neighbors[i] != goal))
                if(!isEmptyCell(neighbors[i]))
                    continue;

            float deltax = neighbors[i].x-current->position.x;
            float deltay = neighbors[i].y-current->position.y;
            float temptative_G = current->G + std::sqrt(deltax*deltax + deltay*deltay);
            std::map<MapPosition, Node*>::iterator neighbor = openMap.find(neighbors[i]);

            // if not in the openQueue
            if(neighbor == openMap.end())
            {
                // add the neighbor
                Node* node = new Node(neighbors[i], current);
                node->G = temptative_G;
                float destx = goal.x-neighbors[i].x;
                float desty = goal.y-neighbors[i].y;
                node->H = std::sqrt(deltax*deltax + deltay*deltay);
                node->F = node->G + node->H;
                openQueue.push(node);
                openMap[node->position] = node;
#ifdef DEBUG
                std::cout << "added free neighbor " << neighbors[i].x << " " << neighbors[i].y << " F=" << node->F << std::endl;
#endif
            }
            // if is in the open queue with a minor G value
            else if(temptative_G < (*neighbor).second->G)
            {
#ifdef DEBUG
                std::cout << "updated free neighbor " << neighbors[i].x << " " << neighbors[i].y << std::endl;
#endif
                // update the neighbor
                Node * node = (*neighbor).second;
                node->G = temptative_G;
                node->F = node->G + node->H;
                std::make_heap(const_cast<Node**>(&openQueue.top()),
                const_cast<Node**>(&openQueue.top()) + openQueue.size(),
                nodeMoreThen);
            }
        }
    }

    // cleanup
    for(std::map<MapPosition, Node*>::iterator elem=openMap.begin(); elem!=openMap.end(); elem++)
        delete (*elem).second;

    for(std::map<MapPosition, Node*>::iterator elem=closedMap.begin(); elem!=closedMap.end(); elem++)
        delete (*elem).second;

#ifdef DEBUG
    std::cout << *this << std::endl;
    std::cout << "start " << start.x << " " << start.y << std::endl;
    std::cout << "goal " << goal.x << " " << goal.y << std::endl;
    std::cout << "Path: ";
    std::list<MapPosition>::iterator pos;
    for(pos = path.begin(); pos != path.end(); pos++)
        std::cout << pos->x << " " << pos->y << ", ";
    std::cout << std::endl;
#endif

    return path;
}

float Map::getRotation(const string& unitName, MapPosition other)
{
    MapPosition unitPos = findUnit(unitName);
    if(!unitPos.isValid())
        return 0;

    std::shared_ptr<Unit> unit = getUnit(unitPos);
    if(!unit)
        return 0;

    float theta = unit->getEntity()->getRotation().y;
    theta = degToRad(theta);
    float theta2 = atan2((float)(other.y - unitPos.y), (float)(other.x - unitPos.x));
    theta = Turbine::radToDeg(theta);
    theta2 = Turbine::radToDeg(theta2);

    // ray attack has different orientation then other attacks (must be a %45)
    if(unit->getAttackType() == AT_RAY && ((int)Turbine::round(theta2) % 45) != 0)
    {
        int n = Turbine::round(theta2) / 90;
        int mod = ((int)Turbine::round(theta2)) % 90;
        if(mod != 0)
        {
            if(mod > 0)
                theta2 = n*90.0+45.0;
            else
                theta2 = n*90.0-45.0;
        }
    }

    // rotation 0 is looking toward -z, so add 90 degrees
    float rotation = theta2 - theta + 90.0;

    // test border cases
    if(rotation > 180)
        rotation -= 360;

    if(rotation < -180)
        rotation += 360;

    return rotation;
}

std::ostream& operator<<(std::ostream& os, const Map& map)
{
    for(int y=map.rownum-1; y>=0; y--)
    {
        for(int x=0; x<map.colnum; x++)
        {
            if(map.cells[y][x] == CELL_FREE)
                os << "-";
            else if(map.cells[y][x] == CELL_LOCKED)
                os << "\\";
            else if(map.cells[y][x] == CELL_OCCUPIED)
                os << "X";
            else
                os << "e";
        }
        os << std::endl;
    }

    int counter=0;
    for(int y=map.rownum-1; y>=0; y--)
    {
        for(int x=0; x<map.colnum; x++)
        {
            if(map.cells[y][x] == CELL_OCCUPIED)
            {
                os << "unit " << counter << " " << map.units[y][x]->getName() << " in " << x << " " << y << " has hp " << map.units[y][x]->getHP() << "/" << map.units[y][x]->getHealth() << " team: " << map.units[y][x]->getTeamNum() << std::endl;
                counter++;
            }
        }
    }

    return os;
}

bool Map::isEmptyCell(MapPosition pos)
{
    assert( (pos.x >= 0) && (pos.x < colnum) && (pos.y >= 0) && (pos.y < rownum) );

    if(cells[pos.y][pos.x] == CELL_FREE)
        return true;

    return false;
}

}
