/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FBUNIT_H
#define FBUNIT_H

#include <TurbineEntity.h>
#include <TurbineGUIHealthBar.h>

#include "FBMap.h"
#include "FBEvent.h"
#include "FBUnitStrategy.h"

namespace FB
{

enum ATTACK_TYPE
{
    AT_PHASER,
    AT_RAY,
};

enum UNIT_STRATEGY
{
    STRAT_ATTACK_NEAREST,
};

struct UnitProperties
{
    int health;
    int armor;
    ATTACK_TYPE attackType;
    float attackDamage;
    float attackPrepTime;
    float reloadSpeed;
    float mobility;
    int range;
};

struct UnitParameters
{
    std::string name;
    Turbine::Model* model;
    Turbine::Model* bullet;
    Turbine::Model* selIndicator;
    UnitProperties prop;
    int teamNum;
    Turbine::Vector3d barOffset;
};

class Map;
class UnitStrategy;
class Unit : public std::enable_shared_from_this<Unit>
{
    Map* _map;

    Turbine::Entity* _entity;
    Turbine::Model* _bullet;
    Turbine::Entity* _selIndicator;

    UnitProperties _prop;
    UnitStrategy* _strategy;

    MapPosition _destination;         // if the player is moving the unit is stored here
    std::list<MapPosition> _userPath; // and a path is generated when a previous action has ended

    int _teamNum;
    int _HP;
    float _reloading;

    Turbine::GUIHealthBar* _bar;
    Turbine::Vector3d _barPlacement;
    Turbine::Vector3d _barOffset;

    bool _selected;

public:

    Unit(Turbine::Entity* entity, Turbine::Model* bullet, Turbine::Entity* selectIndicator, const UnitProperties& prop, int teamNum, Map* map);
    ~Unit();

    Turbine::Entity* getEntity(){ return _entity; }

    Turbine::Model* getBullet(){ return _bullet; }

    void setPosition(const Turbine::Vector3d& newpos);

    void setHealthBarOffset(const Turbine::Vector3d& offset){ _barOffset = offset; }
    const Turbine::Vector3d& getHealthBarOffset(){ return _barOffset; }

    void setName(const std::string& name){ _entity->setName(name); }
    std::string getName(){ return _entity->getName(); }

    void setHealth(int health){ _prop.health = health; }
    int getHealth(){ return _prop.health; }

    void select()
    {
        _selected = true;
        _selIndicator->enable();
    }

    void deselect()
    {
        _selected = false;
        _selIndicator->disable();
    }

    bool isSelected()
    {
        return _selected;
    }

    void setHP(int HP)
    {
        _HP = HP;
        _bar->update((float)_HP/_prop.health);
    }

    void decreaseHP(int amount)
    {
        _HP -= amount;
        _bar->update((float)_HP/_prop.health);
    }

    int getHP(){ return _HP; }

    void setReload(bool val){ _reloading = val; }
    bool isReloading(){ return _reloading; }

    void setArmor(int armor){ _prop.armor = armor; }
    int getArmor(){ return _prop.armor; }
    void incArmor(int amount){ _prop.armor += amount; }

    void setTeamNum(int teamNum){ _teamNum = teamNum; }
    int getTeamNum(){ return _teamNum; }

    void setRange(int range){ _prop.range = range; }
    int getRange(){ return _prop.range; }

    ATTACK_TYPE getAttackType(){ return _prop.attackType; }

    void setAttackDamage(float damage){ _prop.attackDamage = damage; }
    float getAttackDamage(){ return _prop.attackDamage; }

    void setAttackPrepTime(float attackPrepTime){ _prop.attackPrepTime = attackPrepTime; }
    float getAttackPrepTime(){ return _prop.attackPrepTime; }

    void setReloadSpeed(float reloadSpeed){ _prop.reloadSpeed = reloadSpeed; }
    float getReloadSpeed(){ return _prop.reloadSpeed; }

    void setMobility(float mobility){ _prop.mobility = mobility; }
    float getMobility(){ return _prop.mobility; }

    void setUserMove(MapPosition destination){ _destination = destination; }
    MapPosition getUserMove(){ return _destination; }
    void startUserMove();

    // the following funtions start new animations and set the currentAction
    void idle();
    void move();
    void rotate(float degrees);
    void attack();
    void die();
    void celebrate();
    Event* getNextAction(float time);

    void changeUnitStrategy(UNIT_STRATEGY strategy);

    void updateHealthBar();
    void updateSelectIndicator();

    void show();
    void hide();
};

}

#endif /* FBUNIT_H */
