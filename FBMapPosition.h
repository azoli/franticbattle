/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FBMAPPOSITION_H
#define FBMAPPOSITION_H

namespace FB
{

struct MapPosition
{
    int x;
    int y;

    MapPosition() : x(-1), y(-1)
    {
    }

    MapPosition(int posx, int posy) : x(posx), y(posy)
    {
    }

    inline void setInvalid();
    inline bool isValid();

    inline bool operator==(const MapPosition& other) const;
    inline bool operator!=(const MapPosition& other) const;
    inline bool operator<(const MapPosition& other) const;
    inline const MapPosition operator+(const MapPosition& pos1) const;
};

void MapPosition::setInvalid()
{
    x = -1;
    y = -1;
}

bool MapPosition::isValid()
{
    if(x == -1 || y == -1)
        return false;
    return true;
}

bool MapPosition::operator==(const MapPosition& other) const
{
    if(x == other.x && y == other.y)
        return true;
    return false;
}

bool MapPosition::operator!=(const MapPosition& other) const
{
    if(x != other.x || y != other.y)
        return true;
    return false;
}

bool MapPosition::operator<(const MapPosition& other) const
{
    if(x < other.x || (x == other.x && y < other.y))
        return true;

    return false;
}

const MapPosition MapPosition::operator+(const MapPosition& other) const
{
    return MapPosition(x + other.x, y + other.y);
}


}

#endif
