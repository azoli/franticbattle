/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FBUNITSTRATEGY_H
#define FBUNITSTRATEGY_H

#include "FBMap.h"
#include "FBEvent.h"

namespace FB
{

class Map;
class Event;
class UnitStrategy
{
    protected:

    Map* _map;

    public:

    UnitStrategy(Map* map) : _map(map)
    {
    }

    virtual ~UnitStrategy()
    {
    }

    virtual Event* getNextAction(std::shared_ptr<Unit> unit, float time) = 0;
};

}

#endif

