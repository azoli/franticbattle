/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FBEVENTRECEIVER_H
#define FBEVENTRECEIVER_H

#include <memory>
#include <TurbineIEventReceiver.h>
#include <TurbineEngine.h>
#include <TurbineTimer.h>

#include "FBMap.h"
#include "FBSimulator.h"
#include "FBLevel.h"

namespace FB
{

class EventReceiver : public Turbine::IEventReceiver
{
    std::shared_ptr<Turbine::Engine> e;
    Turbine::Camera *camera;
    Turbine::Light *light;

    Map* _map;
    Simulator* _sim;
    bool* _resetTime;
    bool* _printFps;
    bool moveCamera;
    int lastx, lasty;

    std::shared_ptr<Unit> _selUnit;

    Level* _level;

    void _updateHealthBars();

public:

    EventReceiver(Map* map, Simulator* sim, bool* resetTime, bool* printFps, Level* level);
    ~EventReceiver();

    bool onEvent(const Turbine::Event& event);
    void setLevel(Level* level){ _level = level; }
};

}

#endif /* FBEVENTRECEIVER_H */

