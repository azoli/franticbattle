/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <sstream>

#include "FBEventReceiver.h"

using Turbine::Vector3d;
using Turbine::Event;
using Turbine::Engine;
using Turbine::Entity;
using Turbine::degToRad;

//#define DEBUG 1

namespace FB
{

EventReceiver::EventReceiver(Map* map, Simulator* sim, bool* resetTime, bool* printFps, Level* level) : Turbine::IEventReceiver(), _map(map), _sim(sim), _resetTime(resetTime), _printFps(printFps), moveCamera(false), _selUnit(std::shared_ptr<Unit>()), _level(level)
{
    e = Turbine::Engine::getInstance();
    camera = e->getSceneManager()->getCamera("Camera");
    light = e->getSceneManager()->getLight("Light");
}

EventReceiver::~EventReceiver()
{
}

bool EventReceiver::onEvent(const Turbine::Event& event)
{
    switch(event.type)
    {
        case Turbine::EV_DRAW:
            break;

        case Turbine::EV_RESIZE:
        {
            _updateHealthBars();
            break;
        }

        case Turbine::EV_QUIT:
        {
            std::cout << "See you next time! ;)" << std::endl;
            return false;
        }

        case Turbine::EV_KEYBOARD:
        {
            Vector3d v;
            if(event.evKey.key == Turbine::KEY_ESCAPE)
                if( !event.evKey.pressed )
                    return false;

            if(event.evKey.pressed && camera != 0)
            {
                switch(event.evKey.key)
                {
                    case Turbine::KEY_RETURN:
                        if(event.evKey.alt)
                        {
                            e->toggleFullscreen();
                            _updateHealthBars();
                        }
                        break;

                    case Turbine::KEY_ADD:
                        camera->zoom(15.0);
                        _updateHealthBars();
                        break;

                    case Turbine::KEY_SUBTRACT:
                        camera->zoom(-15.0);
                        _updateHealthBars();
                        break;

                    case Turbine::KEY_RIGHT:
                        camera->rotateOriz(1.0);
                        _updateHealthBars();
                        break;

                    case Turbine::KEY_LEFT:
                        camera->rotateOriz(-1.0);
                        _updateHealthBars();
                        break;

                    case Turbine::KEY_UP:
                        if(camera->getVertDegrees()-1.0 > 10.0)
                        {
                            camera->rotateVert(1.0);
                            _updateHealthBars();
                        }
                        break;

                    case Turbine::KEY_DOWN:
                        if(camera->getVertDegrees()+1.0 < 80.0)
                        {
                            camera->rotateVert(-1.0);
                            _updateHealthBars();
                        }
                        break;

                    case Turbine::KEY_KEY_E:
                        Engine::getInstance()->toggleLighting();
                        break;

                    case Turbine::KEY_KEY_U:
                        v=light->getPosition();
                        v.x += 10.0;
                        light->setPosition(v);
                        break;

                    case Turbine::KEY_KEY_J:
                        v=light->getPosition();
                        v.x -= 10.0;
                        light->setPosition(v);
                        break;

                    case Turbine::KEY_KEY_I:
                        v=light->getPosition();
                        v.y += 10.0;
                        light->setPosition(v);
                        break;

                    case Turbine::KEY_KEY_K:
                        v=light->getPosition();
                        v.y -= 10.0;
                        light->setPosition(v);
                        break;

                    case Turbine::KEY_KEY_O:
                        v=light->getPosition();
                        v.z += 10.0;
                        light->setPosition(v);
                        break;

                    case Turbine::KEY_KEY_L:
                        v=light->getPosition();
                        v.z -= 10.0;
                        light->setPosition(v);
                        break;

                    case Turbine::KEY_KEY_F:
                        if(event.evKey.alt && event.evKey.control)
                            *_printFps = !*_printFps;
                        break;

                    case Turbine::KEY_KEY_R:
                        if(_selUnit)
                            _selUnit->deselect();
                        _selUnit = std::shared_ptr<Unit>();
                        _level->restart();
                        *_resetTime = true;
                        break;

                    default:
                        break;
                }
            }
            break;
        }

        case Turbine::EV_MOUSE:
        {
            switch(event.evMouse.button)
            {
                case Turbine::BTN_MOVE:
                {
                    if(moveCamera)
                    {
                        int oriz = (event.evMouse.x - lastx);
                        int vert = (event.evMouse.y - lasty);

                        if(oriz)
                            camera->rotateOriz(-oriz);
                        if((vert) && (camera->getVertDegrees()-vert < 80.0) && (camera->getVertDegrees()-vert > 10.0))
                            camera->rotateVert(vert);

                        lastx = event.evMouse.x;
                        lasty = event.evMouse.y;

                        _updateHealthBars();
                    }
                    break;
                }
                case Turbine::BTN_WHEELUP:
                {
                    if(event.evMouse.pressed)
                    {
                        camera->zoom(15.0);
                        _updateHealthBars();
                    }
                    break;
                }
                case Turbine::BTN_WHEELDOWN:
                {
                    if(event.evMouse.pressed)
                    {
                        camera->zoom(-15.0);
                        _updateHealthBars();
                    }
                    break;
                }
                case Turbine::BTN_LEFT:
                {
                    // on button release
                    if(!event.evMouse.pressed)
                    {
                        Turbine::SceneManager* smgr = e->getSceneManager();
                        Entity* entity=smgr->selectEntity(event.evMouse.x, event.evMouse.y);
                        // if something is being selected
                        if(entity != NULL)
                        {
                            std::string name = entity->getName();

                            // if it is a cell
                            if(name.find("!cell") != std::string::npos)
                            {
                                MapPosition clickedCell;
                                std::stringstream ss(name);
                                ss.seekg(5);
                                ss >> clickedCell.x >> clickedCell.y;

                                // and there is a unit over it, select the unit
                                std::shared_ptr<Unit> unit = _map->getUnit(clickedCell);
                                if(unit)
                                {
                                    if(unit->getTeamNum() == 1)
                                    {
                                        if(_selUnit)
                                            _selUnit->deselect();
                                        unit->select();
                                        _selUnit = unit;
                                    }
                                    else // if clicking on a enemy skip action
                                        break;
                                }

                                // otherwise if the cell is empty and we had a previous selected unit
                                else if(_map->isEmptyCell(clickedCell) && _selUnit)
                                {
                                    _selUnit->deselect();
                                    _selUnit->setUserMove(clickedCell);
                                    _selUnit = std::shared_ptr<Unit>();
                                }

                                break;
                            }

                            // if it is a unit
                            MapPosition unitPos = _map->findUnit(name);
                            std::shared_ptr<Unit> unit = _map->getUnit(unitPos);
                            if(unitPos.isValid() && unit->getTeamNum() == 1)
                            {
                                if(_selUnit)
                                    _selUnit->deselect();
                                unit->select();
                                _selUnit = unit;
                                 break;
                            }
                        }
                        else
                            if(_selUnit)
                            {
                                _selUnit->deselect();
                                _selUnit = std::shared_ptr<Unit>();
                            }
                    }
                    break;
                }
                case Turbine::BTN_RIGHT:
                {
                    if(event.evMouse.pressed)
                    {
                        moveCamera = true;
                        lastx = event.evMouse.x;
                        lasty = event.evMouse.y;
                    }
                    else
                        moveCamera = false;
                    break;
                }
                default:
                    break;
            }
        }

        default:
            break;
    }

    return true;
}

void EventReceiver::_updateHealthBars()
{
    for(int x=0; x<_map->getRow(); x++)
    {
        for(int y=0; y<_map->getColumn(); y++)
        {
            std::shared_ptr<Unit> unit = _map->getUnit(FB::MapPosition(x, y));
            if(unit)
            {
                Vector3d pos = unit->getEntity()->getPosition();
                unit->setPosition(pos);
            }
        }
    }
}

}
