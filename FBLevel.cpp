/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <sstream>
#include <TurbineEngine.h>
#include "FBLevel.h"

namespace FB
{

    Level::Level() : _smgr(Turbine::Engine::getInstance()->getSceneManager()),
                     _map(_smgr), _sim(&_map), _index(0)
    {
    }

    void Level::_addUnits(std::pair<int, UnitParameters> params)
    {
        UnitParameters& p = params.second;
        for(unsigned int i=0; i<params.first; i++)
        {
            std::stringstream ss;
            ss << p.name << "_" << _index;
            _index++;
            Turbine::Entity* entity = _smgr->createEntity(ss.str(), p.model);
            entity->setAction("Idle");

            Turbine::Entity* selEntity = _smgr->createEntity(ss.str()+"_selIndicator", p.selIndicator);

            std::shared_ptr<FB::Unit> unit(new FB::Unit(entity, p.bullet, selEntity, p.prop, p.teamNum, &_map));
            unit->setHealthBarOffset(p.barOffset);
            FB::MapPosition squarepos = _map.addUnitRandomMapPosition(unit);
            Turbine::Vector3d pos = _map.getCellCenter(squarepos);
            unit->setPosition(pos);
        }
    }

    void Level::addUnits(unsigned int num, const UnitParameters& params)
    {
        std::pair<int, UnitParameters> p(num, params);
        unitsParams.push_back(p);
        _addUnits(p);
    }

    void Level::start()
    {
        // init simulator
        for(int y=0; y<_map.getColumn(); y++)
        {
            for(int x=0; x<_map.getRow(); x++)
            {
                std::shared_ptr<FB::Unit> unit = _map.getUnit(FB::MapPosition(x, y));
                if(unit)
                {
                    FB::Event* event = unit->getNextAction(0);
                    _sim.schedule(event);
                }
            }
        }
    }

    void Level::restart()
    {
        // reset simulator
        _sim.reset();

        // clean the map
        _map.clear();

        _index = 0;

        // reload units
        std::vector<std::pair<int, UnitParameters>>::iterator param;
        for(param=unitsParams.begin(); param!=unitsParams.end(); param++)
            _addUnits(*param);

        start();
    }
};
