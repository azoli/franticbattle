/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FBSIMULATOR_H
#define FBSIMULATOR_H

#include <queue>
#include <memory>
#include "FBEvent.h"
#include "FBMap.h"

namespace FB
{

class Simulator
{
    class EventComp
    {
        public:

        bool operator() (const Event* left, const Event* right)
        {
            if( left->time > right->time )
                return true;

            if( left->time == right->time )
                if( left->type == EV_MOVE && right->type == EV_ROTATE )
                    return true;

            return false;
        }
    };

    Map* _map;
    float _time;
    int _winnerTeam;
    std::priority_queue<Event*, std::vector<Event*>, EventComp> queue;

    void _hit(std::shared_ptr<Unit> enemy, float damage, float time);

public:

    Simulator(Map* map);
    virtual ~Simulator();
    virtual float getTime();
    virtual void schedule(Event* event);
    virtual int run(float time);
    virtual void reset();
};

}

#endif /* FBSIMULATOR_H */
