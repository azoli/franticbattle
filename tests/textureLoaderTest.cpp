/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <sys/time.h>
#include <string.h>

#include <TurbineEngine.h>
#include <TurbineEventReceiverDefault.h>

using namespace Turbine;

struct timeval now, start;

float getTicks()
{
    gettimeofday(&now, NULL);
    return (now.tv_sec-start.tv_sec)*1.0f+(now.tv_usec-start.tv_usec)/1000000.0f;
}

void initTicks()
{
    gettimeofday(&start, NULL);
}

#ifdef _WIN32
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
#elif __unix__
int main(){
#endif

    //Create window
    WindowSettings sett;

    sett.title = "Frantic Battle!";
    sett.width = 1280;
    sett.height = 1024;
    sett.colorbits = 32;
    sett.depthbits = 32;
    sett.fullscreen = false;

    std::shared_ptr<Engine> engine = Engine::getInstance();
    engine->create(OPENGL, sett, NULL);

    TextureLoader* tloader = TextureLoader::getInstance();
    GUIManager* guimgr = engine->getGUIManager();

    Image* image = tloader->loadPNG("under_construction.png");
    if(image == NULL)
    {
        std::cout << "Error on loading png file" << std::endl;
        return 0;
    }
    GUIStaticImage* guiImage = guimgr->createStaticImage(image);
    guiImage->setOffset(Vector2d(0.5, 0.5));

    initTicks();
    unsigned int FPS = 0;
    float ticks = getTicks();
    float lastTicks, delta;

    while( engine->run() )
    {
        lastTicks = ticks;
        ticks = getTicks();
        delta += ticks - lastTicks;

        if( delta >= 1.0f ) // when a second has passed
        {
            std::cout << "fps " << FPS << std::endl;

            delta = 0.0f;
            FPS = 0;
        }

        FPS++;

        engine->draw();
    }

    return 1;
}

