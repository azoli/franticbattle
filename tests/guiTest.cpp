/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <sys/time.h>
#include <string.h>

#include <TurbineEngine.h>
#include <TurbineEventReceiverDefault.h>
#include <TurbineFontBuilder.h>

using namespace std;
using namespace Turbine;

struct timeval now, start;

float getTicks()
{
    gettimeofday(&now, NULL);
    return (now.tv_sec-start.tv_sec)*1.0f+(now.tv_usec-start.tv_usec)/1000000.0f;
}

void initTicks()
{
    gettimeofday(&start, NULL);
}

#ifdef _WIN32
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
#elif __unix__
int main(){
#endif

    //Create window
    WindowSettings sett;

    sett.title = "Frantic Battle!";
    sett.width = 1280;
    sett.height = 1024;
    sett.colorbits = 32;
    sett.depthbits = 32;
    sett.fullscreen = false;

    //Create engine with FranticBattle eventReceiver
    std::shared_ptr<Engine> engine = Engine::getInstance();
    engine->create(OPENGL, sett, NULL);

    GUIManager* guimgr = engine->getGUIManager();

    FontBuilder fb;
    fb.init();
    FontInstance* font = fb.build("/usr/share/fonts/corefonts/arial.ttf", 25);

    GUIStaticText* text;
    // test phrase 1
    text = guimgr->createStaticText("the ~ quick! brown, fox: jumps over the lazy dog.", font);
    text->setOffset(Vector2d(0.4, 0.5));
    text = guimgr->createStaticText("The ~ Quick! Brown, Fox: Jumps Over The Lazy Dog.", font);
    text->setOffset(Vector2d(0.4, 0.4));

    text = guimgr->createStaticText("THE ~ QUICK! BROWN, FOX: JUMPS OVER THE LAZY DOG.", font);
    text->setOffset(Vector2d(0.4, 0.3));

    Colorf colors[4] = { Colorf(0.0, 1.0, 0.0, 1.0), Colorf(1.0, 1.0, 0.0, 1.0), Colorf(1.0, 0.0, 0.0, 1.0), Colorf(0.6, 0.6, 0.6, 1.0) };
    GUIHealthBar* bar = guimgr->createHealthBar(13, 13, 10, colors, 2, Colorf(0.0, 0.0, 0.0, 1.0));
    bar->setOffset(Vector2d(0.1, 0.8));
    GUIHealthBar* bar2 = guimgr->createHealthBar(25, 13, 5, colors, 2, Colorf(1.0, 1.0, 1.0, 1.0));
    bar2->setOffset(Vector2d(0.1, 0.75));

    // write all possible drawable ascii characters
    string ascii;
    for(int i=32; i<127; i++)
        ascii.push_back((char)i);
    GUIStaticText* text2 = guimgr->createStaticText(ascii, font);
    text2->setOffset(Vector2d(0.06, 0.95));


    initTicks();
    unsigned int FPS = 0;
    float ticks = getTicks();
    float lastTicks, delta;
    float life = 1.0;

    while( engine->run() )
    {
        lastTicks = ticks;
        ticks = getTicks();
        delta += ticks - lastTicks;

        if( delta >= 1.0f ) // when a second has passed
        {
            life = life-0.1;
            bar->update(life);
            bar2->update(life);
            cout << "fps " << FPS << endl;

            delta = 0.0f;
            FPS = 0;
        }

        FPS++;

        engine->draw();
    }

    return 1;
}

