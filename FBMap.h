/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FBMAP_H
#define FBMAP_H

#include <string.h>
#include <stdlib.h>
#include <TurbineVector3d.h>
#include "TurbineSceneManager.h"
#include "FBUnit.h"
#include "FBMapPosition.h"

namespace FB
{

enum CELLSTATE
{
    CELL_FREE,
    CELL_LOCKED,
    CELL_OCCUPIED
};

class Node
{
    public:

    MapPosition position;
    Node* parent;
    float F;
    float G;
    float H;

    Node(MapPosition pos, Node* par);
    ~Node();
};

bool nodeMoreThen(const Node* left, const Node* right);
struct NodeMoreThen
{
    bool operator()(const Node* left, const Node* right) const
    {
        return nodeMoreThen(left, right);
    }
};



class Unit;
class Map
{

public:

    Map(Turbine::SceneManager* smgr);
    ~Map();

    void clear();

    int getRow(){ return rownum; }
    int getColumn(){ return colnum; }
    int getcellSize(){ return cellsize; }

    // add the unit in the map
    bool addUnit(std::shared_ptr<Unit> unit, MapPosition pos);

    // add the unit in random position in the map and return the position
    MapPosition addUnitRandomMapPosition(std::shared_ptr<Unit> unit);

    bool removeUnit(std::shared_ptr<Unit> unit);

    // get the unit in pos
    std::shared_ptr<Unit> getUnit(MapPosition pos);

    // find the cell from the unit name
    MapPosition findUnit(std::string name);

    // get the cell center
    Turbine::Vector3d getCellCenter(MapPosition pos);

    void selectCell(MapPosition pos);
    void deselectCell();
    MapPosition getSelectedCell();

    bool isEmptyCell(MapPosition pos);
    bool lockCell(MapPosition pos);
    void unlockCell(MapPosition pos);

    std::list<MapPosition> findPath(MapPosition start, MapPosition goal, bool skipLast);
    float getRotation(const std::string& unitName, MapPosition pos);

private:

    const static int colnum = 15;
    const static int rownum = 15;
    const static int cellsize = 20;

    Turbine::SceneManager* _smgr;

    std::shared_ptr<Unit> units[rownum][colnum];
    CELLSTATE cells[rownum][colnum];

    void buildTerrainModel();

    friend std::ostream& operator<<(std::ostream& os, const Map& map);
};

}

#endif 	/* FBMAP_H */
