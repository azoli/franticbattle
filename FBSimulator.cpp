/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <TurbineEngine.h>
#include <TurbineMath.h>
#include "FBSimulator.h"
#include "FBEvent.h"
#include "TurbineMath.h"

//#define DEBUG 1

namespace FB
{

Simulator::Simulator(Map* map) : _map(map), _winnerTeam(0)
{
    queue = std::priority_queue<Event*, std::vector<Event*>, EventComp>();
}

Simulator::~Simulator()
{
    reset();
}

float Simulator::getTime()
{
    return _time;
}

void Simulator::schedule(Event* event)
{
    queue.push(event);
}

int Simulator::run(float time)
{
    if(queue.empty())
        return _winnerTeam;

    Event* event = queue.top();
    while( !queue.empty() && (event != NULL) && (event->time <= time) )
    {
        queue.pop();

        // skip actions if the unit is dead or dying
        if(event->unit->getHP() <= 0 && event->type != EV_DEATH && event->type != EV_END_SHOOT)
        {
#ifdef DEBUG
            MapPosition pos = _map->findUnit(event->unit->getName());
            std::cout << "Skipping event for dying unit" << event->unit->getName();
            std::cout << " in " << pos.x << " " << pos.y << std::endl;
#endif
            delete event;
            event = queue.top();
            continue;
        }

        // if there is a user action, start it after the end of an AI action
        if(event->unit->getUserMove().isValid() && ( event->type == EV_IDLE       || \
                                                     event->type == EV_END_ROTATE || \
                                                     event->type == EV_END_MOVE   || \
                                                     event->type == EV_END_SHOOT ))
        {
#ifdef DEBUG
            std::cout << "Starting user move" << std::endl;
#endif
            event->unit->startUserMove();
        }

#ifdef DEBUG
        std::cout << "Time: " << event->time << std::endl;
        MapPosition unitPos =_map->findUnit(event->unit->getName());
        std::cout << "Unit: " << event->unit->getName() << std::endl;
        std::cout << "Position: " << unitPos.x << " " << unitPos.y << std::endl;
        std::cout << "Action: ";
#endif
        // process the event
        switch(event->type)
        {
            case EV_IDLE:
            {
                event->unit->idle();
                Event* nextEvent = event->unit->getNextAction(event->time);
                schedule(nextEvent);
#ifdef DEBUG
                std::cout << "Idle" << std::endl;
#endif
                break;
            }
            case EV_ROTATE:
            {
                RotationEvent* re = (RotationEvent*) event;
                re->unit->rotate(re->degrees);

                // schedule a EndRotationEvent
                const float omega = 360 * re->unit->getMobility(); // angular velocity (degree/s)
                const float rotationTime = (std::abs(re->degrees) / omega); // add rotation time to current time
                EndRotationEvent* ere = new EndRotationEvent(re->unit, re->time+rotationTime, re->degrees);
                schedule(ere);
#ifdef DEBUG
                std::cout << "Rotate " << re->degrees << "degrees" << std::endl;
#endif
                break;
            }
            case EV_END_ROTATE:
            {
                EndRotationEvent* ere = (EndRotationEvent*) event;
                Turbine::Entity* entity = ere->unit->getEntity();
                Turbine::Vector3d rot = entity->getRotation();
                rot.y += ere->degrees;
                entity->setRotation(rot);

                Event* nextEvent = ere->unit->getNextAction(event->time);
                schedule(nextEvent);
#ifdef DEBUG
                std::cout << "EndRotate " << ere->degrees << "degrees" << std::endl;
#endif
                break;
            }
            case EV_MOVE:
            {
                MoveEvent* me = (MoveEvent*) event;
                std::shared_ptr<Unit> unit = me->unit;
                MapPosition start = _map->findUnit(unit->getName());
                MapPosition step = me->step;
#ifdef DEBUG
                std::cout << "Move to " << step.x << " " << step.y;
#endif
                // test step validity
                if( std::abs(start.x - step.x) > 1 || std::abs(start.y - step.y) > 1)
                {
                    Event* nextEvent = unit->getNextAction(event->time+0.1);
                    schedule(nextEvent);
#ifdef DEBUG
                    std::cout << " is invalid!" << std::endl;
#endif
                    break;
                }

                // Do the step!
                // if the step end in a free cell
                if( _map->lockCell(step) )
                {
                    me->unit->move();

                    // schedule a CrossingEvent at half time
                    float space;
                    // if it is the diagonal use sqrt(2)
                    if( (std::abs(start.x - step.x) == 1) && (std::abs(start.y - step.y) == 1) )
                        space = 1.414213562;
                    else
                        space = 1.0;

                    float time = space / me->unit->getMobility(); // how much time to schedule 1 square movement?
                    std::pair<MapPosition, MapPosition> cells(start, step);
                    CrossingEvent* cross = new CrossingEvent(unit, me->time+(time/2.0), cells);
                    schedule(cross);

                    // schedule an EndMoveEvent
                    EndMoveEvent* end = new EndMoveEvent(unit, me->time+time, _map->getcellSize());
                    schedule(end);
#ifdef DEBUG
                    std::cout << " is valid" << std::endl;
#endif
                }
                // Stuck. Let's get a new action
                else
                {
                    Event* nextEvent = unit->getNextAction(event->time+0.1);
                    schedule(nextEvent);
#ifdef DEBUG
                    std::cout << " is stuck!" << std::endl;
#endif
                }

                break;
            }
            case EV_END_MOVE:
            {
                EndMoveEvent* eme = (EndMoveEvent*) event;
                std::shared_ptr<Unit> unit = eme->unit;
                Turbine::Entity* entity = unit->getEntity();
                Turbine::Vector3d trans = entity->getPosition();
                Turbine::Vector3d rot = entity->getRotation();

                float deltax=0;
                float deltaz=0;

                float siny = Turbine::round(std::sin(Turbine::degToRad(rot.y)) * 100) / 100;
                float cosy = Turbine::round(std::cos(Turbine::degToRad(rot.y)) * 100) / 100;
                if(siny > 0)
                    deltax = eme->translation;
                else if(siny < 0)
                    deltax = -eme->translation;
                if(cosy > 0)
                    deltaz = eme->translation;
                else if(cosy < 0)
                    deltaz = -eme->translation;

                trans.x += deltax;
                trans.z += deltaz;
                unit->setPosition(trans);

                Event* nextEvent = unit->getNextAction(event->time);
                schedule(nextEvent);
#ifdef DEBUG
                std::cout << "EndMove" << std::endl;
#endif
                break;
            }
            case EV_CROSSING:
            {
                CrossingEvent* ce = (CrossingEvent*) event;
                _map->removeUnit(ce->unit);
                _map->unlockCell(ce->step.second);
                _map->addUnit(ce->unit, ce->step.second);
#ifdef DEBUG
                std::cout << "Crossing to " << ce->step.second.x << " " << ce->step.second.y << std::endl;
#endif
                break;
            }
            case EV_ATTACK:
            {
                AttackEvent* ae = (AttackEvent*) event;
                std::shared_ptr<Unit> unit = ae->unit;
                std::shared_ptr<Unit> enemy = ae->enemy;

                unit->attack();
                Event* shootEvent = new ShootEvent(unit, ae->time+unit->getAttackPrepTime(), enemy);
                schedule(shootEvent);

                Event* endReloadEvent = new Event(EV_END_RELOAD, unit, ae->time+unit->getReloadSpeed());
                schedule(endReloadEvent);
#ifdef DEBUG
                MapPosition enemyPos = _map->findUnit(enemy->getName());
                std::cout << "Attack enemy " << enemy->getName() << " in " << enemyPos.x << " " << enemyPos.y << std::endl;
#endif
                break;
            }
            case EV_SHOOT:
            {
                ShootEvent* ese = (ShootEvent*) event;
                std::shared_ptr<Unit> unit = ese->unit;
                std::shared_ptr<Unit> enemy = ese->enemy;

                // create/start bullet
                Turbine::Entity* entity = unit->getEntity();
                Turbine::Model* bulletModel = unit->getBullet();
                bulletModel->materials[0].useGlow = true;
                Turbine::SceneManager* smgr = Turbine::Engine::getInstance()->getSceneManager();
                Turbine::Entity* bulletEntity = smgr->createEntity(entity->getName()+"_bullet", bulletModel);
                bulletEntity->setAction("Attack");
                bulletEntity->setActionLoop(false);
                bulletEntity->enable();
                bulletEntity->setRotation(entity->getRotation());
                bulletEntity->setScale(entity->getScale());
                bulletEntity->setPosition(entity->getPosition());


                float t;
                MapPosition pos = _map->findUnit(unit->getName());
                MapPosition enemyPos = _map->findUnit(enemy->getName());
                switch(unit->getAttackType())
                {
                    case AT_PHASER:
                    {
                        // compute bullet lifetime
                        float deltax = enemyPos.x-pos.x;
                        float deltay = enemyPos.y-pos.y;
                        float offset = 0.5; // bullet space offset (the arm length)
                        float s = std::sqrt(deltax*deltax + deltay*deltay) - offset;
                        const float v = 1 / 0.125; // 1 cellsize every 1/8 sec
                        t = s / v;

                        break;
                    }
                    case AT_RAY:
                    {
                        /*
                         * Strategy return the nearest enemy of X that is extended
                         * for cyborg cone damage (using the case A or B).
                         * Es. the * cone is fired for the A right targets.
                         * Es. the + cone is fired for the three B bottom-left targets.
                         *  ---------------------
                         *  |   | B | A | B |   |
                         *  ---------------------
                         *  | B | B | A | B | B*|
                         *  ---------------------
                         *  | A | A | X | A*| A*|
                         *  ---------------------
                         *  | B+| B+| A | B | B*|
                         *  ---------------------
                         *  |   | B+| A | B |   |
                         *  --------------------- */

                        std::vector<MapPosition> mask;

                        // A case
                        int dx = enemyPos.x-pos.x;
                        int dy = enemyPos.y-pos.y;
                        if(dx == 0)
                        {
                            mask.resize(4);

                            // up
                            if(dy > 0)
                            {
                                mask[0] = MapPosition(1, 2);
                                mask[1] = MapPosition(0, 1);
                                mask[2] = MapPosition(0, 2);
                                mask[3] = MapPosition(-1, 2);
                            }
                            // down
                            else
                            {
                                mask[0] = MapPosition(-1, -2);
                                mask[1] = MapPosition(0, -1);
                                mask[2] = MapPosition(0, -2);
                                mask[3] = MapPosition(1, -2);
                            }
                        }
                        else if(dy == 0)
                        {
                            mask.resize(4);

                            // left
                            if(dx < 0)
                            {
                                mask[0] = MapPosition(-2, 1);
                                mask[1] = MapPosition(-1, 0);
                                mask[2] = MapPosition(-2, 0);
                                mask[3] = MapPosition(-2, -1);
                            }
                            // right
                            else
                            {
                                mask[0] = MapPosition(2, -1);
                                mask[1] = MapPosition(1, 0);
                                mask[2] = MapPosition(2, 0);
                                mask[3] = MapPosition(2, 1);
                            }
                        }

                        // B case
                        else
                        {
                            mask.resize(3);

                            if(dx < 0)
                            {
                                // down-left
                                if(dy < 0)
                                {
                                    mask[0] = MapPosition(-2, -1);
                                    mask[1] = MapPosition(-1, -1);
                                    mask[2] = MapPosition(-1, -2);
                                }
                                // up-left
                                else
                                {
                                    mask[0] = MapPosition(-1, 2);
                                    mask[1] = MapPosition(-1, 1);
                                    mask[2] = MapPosition(-2, 1);
                                }
                            }
                            else
                            {
                                // down-right
                                if(dy < 0)
                                {
                                    mask[0] = MapPosition(1, -2);
                                    mask[1] = MapPosition(1, -1);
                                    mask[2] = MapPosition(2, -1);
                                }
                                // up-right
                                else
                                {
                                    mask[0] = MapPosition(2, 1);
                                    mask[1] = MapPosition(1, 1);
                                    mask[2] = MapPosition(1, 2);
                                }
                            }
                        }

                        // compute ray hit cells timings
                        const float coneSize = 53.13;
                        const float raySpeed = coneSize / 1.0;
                        float times[4];
                        times[0] = 0.0;
                        times[1] = (coneSize/2.0)/raySpeed;
                        if(mask.size() == 3)
                        {
                            times[2] = coneSize/raySpeed;
                        }
                        else
                        {
                            times[2] = (coneSize/2.0)/raySpeed;
                            times[3] = coneSize/raySpeed;
                        }

                        // schedule hits
                        int rownum = _map->getRow();
                        int colnum = _map->getColumn();
                        for(unsigned int i=0; i<mask.size(); i++)
                        {
                            MapPosition target = pos+mask[i];
                            Event* hitCellEvent = new HitCellEvent(unit, ese->time+times[i], target);
                            schedule(hitCellEvent);
                        }

                        // do not use direct hit
                        enemy = std::shared_ptr<Unit>();
                        t = times[mask.size()-1];
                    }
                }
                Event* endShootEvent = new EndShootEvent(unit, ese->time+t, enemy, bulletEntity);
                schedule(endShootEvent);

                // start reloading
                unit->setReload(true);
#ifdef DEBUG
                if(enemy)
                    std::cout << "Direct shoot enemy " << enemy->getName() << " in " << enemyPos.x << " " << enemyPos.y << std::endl;
#endif
                break;
            }
            case EV_HIT_CELL:
            {
                HitCellEvent* he = (HitCellEvent*)event;
                std::shared_ptr<Unit> enemy = _map->getUnit(he->target);
#ifdef DEBUG
                std::cout << "Hit cell " << he->target.x << " " << he->target.y << std::endl;
#endif
                if(enemy && enemy->getTeamNum() != he->unit->getTeamNum())
                    _hit(enemy, he->unit->getAttackDamage(), event->time);
#ifdef DEBUG
                else
                    std::cout << "cell without enemy" << std::endl;
#endif
                break;
            }
            case EV_END_SHOOT:
            {
#ifdef DEBUG
                std::cout << "EndShoot ";
#endif
                EndShootEvent* se = (EndShootEvent*) event;
                std::shared_ptr<Unit> unit = se->unit;
                std::shared_ptr<Unit> enemy = se->enemy;

                // stop/destroy bullet
                Turbine::SceneManager* smgr = Turbine::Engine::getInstance()->getSceneManager();
                smgr->destroyEntity(se->bulletEntity);

                if(enemy)
                    _hit(enemy, unit->getAttackDamage(), event->time);

                if(unit->getHP() > 0)
                {
                    Event* nextEvent = event->unit->getNextAction(event->time);
                    schedule(nextEvent);
                }

                break;
            }
            case EV_END_RELOAD:
            {
                event->unit->setReload(false);
#ifdef DEBUG
                std::cout << "EndReload" << std::endl;
#endif
                break;
            }
            case EV_DEATH:
            {
                _map->removeUnit(event->unit);
#ifdef DEBUG
                std::cout << "Death" << std::endl;
#endif
                break;
            }
            case EV_WINNER:
            {
                event->unit->celebrate();
                _winnerTeam = event->unit->getTeamNum();
#ifdef DEBUG
                std::cout << "Winner " << std::endl;
#endif
                break;
            }

            default:
            {
#ifdef DEBUG
                std::cout << "error processing an event.." << std::endl;
#endif
            }
        }

#ifdef DEBUG
        std::cout << std::endl;
#endif

        // advance simulation time
        _time = event->time;

        // destroy event
        delete event;

        event = queue.top();
    }

    return _winnerTeam;
}

void Simulator::reset()
{
    _winnerTeam = 0;
    _time = 0;
    while(!queue.empty())
    {
        Event* event = queue.top();
        if(event->type == EV_END_SHOOT)
        {
           EndShootEvent* se = (EndShootEvent*) event;
           Turbine::SceneManager* smgr = Turbine::Engine::getInstance()->getSceneManager();
           smgr->destroyEntity(se->bulletEntity);
        }

        delete event;
        queue.pop();
    }
}

void Simulator::_hit(std::shared_ptr<Unit> enemy, float damage, float time)
{
    // waste the attack if the enemy is death/dying
    if(enemy && enemy->getHP()>0)
    {
#ifdef DEBUG
        MapPosition enemyPos = _map->findUnit(enemy->getName());
        std::cout << "hit " << enemy->getName() << " in " << enemyPos.x << " " << enemyPos.y << std::endl;
#endif
        enemy->decreaseHP(damage-enemy->getArmor());

        if(enemy->getHP() <= 0)
        {
#ifdef DEBUG
            std::cout << "Dying " << enemy->getName() << " in " << enemyPos.x << " " << enemyPos.y << std::endl;
#endif
            enemy->die();

            // the enemy is dead
            Event* deathEvent = new Event(EV_DEATH, enemy, time+3.0);
            schedule(deathEvent);
        }
#ifdef DEBUG
        else
            std::cout << "wasted hit" << std::endl;
#endif
    }
}

}
