#!/bin/bash
 
# See bottom of the file for export options
BLENDER_BIN=/home/athlon/blender-2.63a/blender

# check we have blender installed
if hash $BLENDER_BIN 2>/dev/null; then
    # do nothing
    echo $BLENDER_BIN "found in path..."
else
    echo $BLENDER_BIN "is not in the path, aborting!"
    exit 1
fi
                         
if [ $# -ne 1 ] ; then
    echo "Expected 1 argument!"
    echo "Usage: blend2vbo.sh from.blend"
    exit 1
fi
                                                 
if ! [ -e $1 ]; then
    echo "Blend file" $1 "does not exist, aborting!"
    exit 1
fi
                                                                 
# Temp Py file.
TEMP_PY=`mktemp`
touch $TEMP_PY
cat >> $TEMP_PY << EOF
import bpy, os
try: import io_export_vbo
except:
    print("error: io_export_vbo.py not found.")
    bpy.ops.wm.quit_blender()


file, ext = os.path.splitext("$1")
file += ".vbo"
print("converting "+file+" ..")
io_export_vbo.export_vbo(file)
print("conversion successfully finished!")
EOF
     
# Run blender
$BLENDER_BIN $1 --background --python $TEMP_PY
rm $TEMP_PY
