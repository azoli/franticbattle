# Blender exporter for the Turbine Engine
# tested only with blender 2.59, copy into blender-2.59-bin/2.59/script/addon

### VBO 1.8 file format #########################
# Header
''' "VBO1.8",
    name            (int namelen, char name[namelen])
    numvertices  NV (int)
    numindices   NI (int)
    nummaterials NM (int)
    numtextures  NT (int)
    numbones     NB (int)
    numactions   NA (int) '''

# Vertex coordiantes chunk
''' coordinate1    (float[3])
    coordinate2
    ...
    coordinatenv '''

# Normals chunk
''' normal1        (float[3])
    normal2
    ...
    normalnv '''

# Triangle indices chunk
''' index1     (int[3])
    index2
    ...
    index3 '''

# Texture coordinates chunk
# OPTIONAL(NT)
''' texcoord1      (float texcoord[2])
    texcoord2
    ...
    texcoordnv '''

# Materials chunk
''' material1      (int namelen, char name[namelen], float ambient, float diffuse[3], float specular[3], float hardness, float alpha, int vstart, int vend, int filenamelen, char filename[filenamelen])
    material2
    ...
    materialnm '''

# Bone weights chunk
# OPTIONAL(NB)
''' weight1        (int boneindex[4], float influences[4])
    weight2
    ...
    weightnv '''

# Tpose bones chunk (stored in pre-order)
# OPTIONAL(NB)
''' bone1          (int namelen, char name[namelen], float frame[16], float offset[16]), int numchilds)
    bone2
    ...
    bonesnb '''

# Actions chunk
# OPTIONAL(NB, NA)
''' action1        (int namelen, char name[namelen], int transformations)
        transformation1 (int namelen, char bone[namelen], int numkeys, char type, keyframe keyframes[numkeys])
        transformation2
        ...
        transformation
    action2
    ...
    actionna '''

### EOF #########################################

# A transformation has a type - value like:
# 't' - (x,y,z,0) for translation
# 'r' - (x,y,z,w) for rotation
# 's' - (x,y,z,0) for scale
#
# A pseudo-definition of keyframe:
# struct keyframe {
#     float time;
#     float transformation_value[4];
# }
#

bl_info = {
    "name": "Turbine model format (.vbo)",
    "author": "Andrea Zoli",
    "version": (1, 4, 0),
    "blender": (2, 5, 8),
    "api": 37702,
    "location": "File > Export > Turbine format (.vbo)",
    "description": "Export vbo for turbine game engine (.vbo)",
    "warning": "",
    "wiki_url": "http://wiki.blender.org/index.php/Extensions:2.5/Py/"\
        "Scripts/Import-Export/DirectX_Exporter",
    "tracker_url": "https://projects.blender.org/tracker/index.php?"\
        "func=detail&aid=22795",
    "category": "Import-Export"}

debug = False

import bpy
import os
import struct
from math import radians
from mathutils import *

rotate90 = Matrix( ((1, 0 ,0 ,0), (0, 0, 1, 0), (0, -1, 0, 0), (0, 0, 0, 1)) )
rotate90m = Matrix( ((1, 0 ,0 ,0), (0, 0, -1, 0), (0, 1, 0, 0), (0, 0, 0, 1)) )

def print_float(val):
    if val >= 0:
        print(end=" ")
    print('%0.3f' % val, end =" ")


def print_floats(tup):
    for val in tup:
        print_float(val)
    print()

def write_matrix(fd, matrix):
    for row in matrix:
        if debug:
            print_floats(row)
        for val in row:
            fd.write( struct.pack('<f', val) )
    if debug:
       print()

def get_unique_vertex(vdict, coords, normals, texcoords, bweights, bone_indices, matindex):
    tup = (coords[0], coords[1], coords[2], normals[0], normals[1], normals[2], texcoords[0], texcoords[1], bweights[0], bweights[1], bweights[2], bweights[3], bone_indices[0], bone_indices[1], bone_indices[2], bone_indices[3], matindex)
    try:
        isnew = False
        newindex = vdict[tup]
    except:
        isnew = True
        newindex = len(vdict)
        vdict[tup] = newindex
    return isnew, newindex, tup

def writebones_preorder(fd, bone, obj_matrix, arm_matrix):
    bone_matrix = bone.matrix
    bonep_matrix = Matrix()
    if bone.parent != None:
        bonep_matrix = bone.parent.matrix

    if debug:
        print(len(bone.name), bone.name.encode('utf-8'))
    fd.write( struct.pack('<i'+str(len(bone.name))+'s', len(bone.name), bone.name.encode('utf-8')) )

    bb_relative_matrix = bonep_matrix.inverted() * bone_matrix
    if debug:
        print("bone to parent relative matrix")
    write_matrix(fd, bb_relative_matrix)  # write bone frame matrix relative to the parent bone

    # write offset matrix (convert mesh vertices into bone space)
    # 1) object space   to world space
    # 2) world space    to armature space
    # 3) armature space to bone space
    # remember to change frame reference we need to use the inverse of the matrix between the first reference frame and the second reference frame
    offset_matrix = bone_matrix.inverted() * arm_matrix.inverted() * obj_matrix

    if debug:
        print("object to bone")
    write_matrix(fd, offset_matrix)

    if debug:
        print(len(bone.children))
    fd.write( struct.pack('<i', len(bone.children)) )

    for child in bone.children:
        writebones_preorder(fd, child, obj_matrix, arm_matrix)


def find_bone_preorder(bone, array, index):
    if debug:
        print(bone.name, index)
    array[bone.name] = index
    index+=1

    for child in bone.children:
        index = find_bone_preorder(child, array, index)

    return index

def export_vbo(filepath):
    bpy.ops.object.mode_set(mode='OBJECT')

    # export only first mesh object
    # XXX you can join more than one mesh and convert curve/surfaces into a single mesh with blender ops
    objs = [Object for Object in bpy.context.scene.objects if Object.type == 'MESH']
    name = objs[0].data.name

    mesh = objs[0].copy()
    for modifier in [modifier for modifier in mesh.modifiers if modifier.type == "ARMATURE"]:
        mesh.modifiers.remove(modifier)

    mesh = mesh.to_mesh(bpy.context.scene, True, 'PREVIEW') # apply modifiers

    armatures = [Object for Object in bpy.context.scene.objects if Object.type == 'ARMATURE']
    if len(armatures) == 0:
        numbones = 0
    elif len(armatures) == 1:
        bpy.context.scene.objects.active = armatures[0]
        bone_index = { Bone.name: Index for Index, Bone in enumerate(armatures[0].pose.bones) }
        bones = [Bone for Bone in armatures[0].pose.bones]
        roots = [Bone for Bone in bones if Bone.parent == None]
        bone_index_preorder = {}
        idx=2
        for root in roots:
            idx = find_bone_preorder(root, bone_index_preorder, idx)
        numbones = len(armatures[0].pose.bones)
    else:
        print("Error: found "+str(len(armatures))+" armatures. Exporter work only with a single armature.")
        return

    faces = []     # [ index1, index2, index3, ... ]
    materials = [] # [ (Material, vstart, vend, image), ... ]
    vertices = []  # [ (coord[0], coord[1], coord[2], normal[0], normal[1], normal[2], texcoord[0], texcoord[1] ]
                   #    bweight[0], bweight[1], bweight[2], bweight[3], bone_influence[0], bone_influence[1], bone_influence[2], bone_influence[3],
                   #    matindex), ... ]
    numtextures = 0
    vdict = {}
    vend = 0

    vertex_groups = {}
    object_vertex_groups = {i: group.name for (i, group) in enumerate(objs[0].vertex_groups)}

    faceblocks = [[] for Size in range(len(objs[0].material_slots))]
    for face in mesh.faces:
        if len(faceblocks) > 0:
            faceblocks[face.material_index].append(face)
    
    face_index_pair = {face: index for index, face in enumerate(mesh.faces)}
    if mesh.uv_textures.active:
        uv_layers = mesh.uv_textures.active.data

    for nummat, faceblock in enumerate(faceblocks):
        vstart = vend

        material = objs[0].material_slots[nummat].material
        if material == None:
            continue
            
        if material.texture_slots[0] != None and material.texture_slots[0].texture != 0 and material.texture_slots[0].texture.type == 'IMAGE':
            numtextures += 1
            image = material.texture_slots[0].texture.image
        else:
            image = None

        for face in faceblock: 
            findex = face_index_pair[face]
            newindex = []
            for i, vindex in enumerate(face.vertices):
                v = mesh.vertices[vindex]

                if image != None:
                    uv = uv_layers[findex].uv[i]
                else:
                    uv = (0, 0)

                bone_indices = [-1, -1, -1, -1]
                weights = [0.0, 0.0, 0.0, 0.0]
                index = 0
                count = 0
                while index < len(v.groups) and count < 4:
                    vgname = object_vertex_groups[v.groups[index].group]
                    if v.groups[index].weight > 0:
                        try:
                            bone_indices[count] = bone_index_preorder[vgname]
                            weights[count] = v.groups[index].weight
                            count += 1
                        except:
                            null = 0
                    index += 1

                if face.use_smooth == False:
                    isnew, idx, tup = get_unique_vertex(vdict, v.co, face.normal, uv, weights, bone_indices, nummat)
                else:
                    isnew, idx, tup = get_unique_vertex(vdict, v.co, v.normal, uv, weights, bone_indices, nummat)
                
                if isnew:
                    vertices.append(tup)
                newindex.append(idx)

            if len(face.vertices) == 3:
                faces.append(newindex)
                vend += 1
            else:
                faces.append((newindex[0], newindex[1], newindex[2]))
                faces.append((newindex[0], newindex[2], newindex[3]))
                vend += 2

        materials.append((material, vstart, vend, image))


    fd = open(filepath, "wb")
    
    # write header
    if debug:
        print("#header") 
        print(b'VBO1.8')
        print(name, len(mesh.vertices), len(faces*3), len(materials), numtextures, numbones, len(bpy.data.actions))
    fd.write( struct.pack('<6s', b'VBO1.8') )
    fd.write( struct.pack('<i'+str(len(name))+'s', len(name), name.encode('utf-8')) )
    fd.write( struct.pack('<6i', len(vertices), len(faces)*3, len(materials), numtextures, numbones, len(bpy.data.actions)) )

    # write vertex coordinates
    if debug:
        print("#coordinates")
    for vertex in vertices:
        if debug:
            print_floats( (vertex[0], vertex[2], -vertex[1]) )
        fd.write( struct.pack('<3f', vertex[0], vertex[1], vertex[2]) ) # convert from x-zy to xyz

    # write vertex normals
    if debug:
        print("#normals")
    for vertex in vertices:
        if debug:
            print_floats( (vertex[3], vertex[5], -vertex[4]) )
        fd.write( struct.pack('<3f', vertex[3], vertex[4], vertex[5]) )

    # write vertex texture coords
    if numtextures > 0:
        if debug:
            print("#texcoords")
        for vertex in vertices:
            if debug:
                print_floats( (vertex[6], vertex[7]) )
            fd.write( struct.pack('<2f', vertex[6], vertex[7]) )

    # write indices
    if debug:
        print("#indices")
    for index in faces:
        if debug:
            print(index[0], index[1], index[2])
        fd.write( struct.pack('<3i', index[0], index[1], index[2]) )

    # write materials
    if debug:
        print("#materials")
    for mat, vstart, vend, image in materials:
        if debug:
            print(mat.name)
            print_float(mat.ambient)
            print_floats( (mat.diffuse_color[0]*mat.diffuse_intensity, mat.diffuse_color[1]*mat.diffuse_intensity, mat.diffuse_color[2]*mat.diffuse_intensity) )
            print_floats( (mat.specular_color[0]*mat.specular_intensity, mat.specular_color[1]*mat.specular_intensity, mat.specular_color[2]*mat.specular_intensity) )
            print(mat.specular_hardness)
            print_float(mat.alpha)
            print(vstart)
            print(vend)
            if image != None:
                print(image.name)
            else:
                print("no texture")
        fd.write( struct.pack('<i'+str(len(mat.name))+'s', len(mat.name), mat.name.encode('utf-8')) )
        fd.write( struct.pack('<f', mat.ambient) )
        fd.write( struct.pack('<3f', mat.diffuse_color[0]*mat.diffuse_intensity, mat.diffuse_color[1]*mat.diffuse_intensity, mat.diffuse_color[2]*mat.diffuse_intensity) )
        fd.write( struct.pack('<3f', mat.specular_color[0]*mat.specular_intensity, mat.specular_color[1]*mat.specular_intensity, mat.specular_color[2]*mat.specular_intensity) )
        fd.write( struct.pack('<f', mat.specular_hardness) )
        fd.write( struct.pack('<f', mat.alpha) )
        fd.write( struct.pack('<2i', vstart, vend) )
        if image != None:
            fd.write( struct.pack('<i'+str(len(image.name))+'s', len(image.name), image.name.encode('utf-8')) )
        else:
            fd.write( struct.pack('<i', 0) )

    if numbones > 0:
        # write bone weights
        if debug:
            print("#bweights")
        for vertex in vertices:
            if debug:
                print(vertex[12], vertex[13], vertex[14], vertex[15])
                print_floats( (vertex[8], vertex[9], vertex[10], vertex[11]) )
            fd.write( struct.pack('<4i', vertex[12], vertex[13], vertex[14], vertex[15]) )
            fd.write( struct.pack('<4f', vertex[8], vertex[9], vertex[10], vertex[11]) )

        # write bones
        # first fake root bone (world to object transformation)
        rootname = "root"
        if debug:
            print(len(rootname), rootname.encode('utf-8'))
        fd.write( struct.pack('<i'+str(len(rootname))+'s', len(rootname), rootname.encode('utf-8')) )
        world_object_matrix = rotate90m
        if debug:
            print("world to object")
        write_matrix(fd, world_object_matrix)
        if debug:
            print("bone to object")
        write_matrix(fd, Matrix())
        if debug:
            print("1")
        fd.write( struct.pack('<i', 1) )

        # second fake armature bone (armature to world transfomation)
        armaturename = "armature"
        if debug:
            print(len(armaturename), armaturename.encode('utf-8'))
        fd.write( struct.pack('<i'+str(len(armaturename))+'s', len(armaturename), armaturename.encode('utf-8')) )
        armature_world_matrix = armatures[0].matrix_world
        if debug:
            print("armature to world")
            print(armature_world_matrix)
        write_matrix(fd, armature_world_matrix)
        if debug:
            print("bone to object")
        write_matrix(fd, Matrix())
        if debug:
            print("1")
        fd.write( struct.pack('<i', len(roots)) )

        # finally write armature bones recursively
        bpy.ops.object.mode_set(mode='POSE')
        bpy.ops.pose.transforms_clear()
        bpy.ops.object.mode_set(mode='OBJECT')
        for root in roots:
            writebones_preorder(fd, root, objs[0].matrix_world, armatures[0].matrix_world)

        # write actions
        if len(bpy.data.actions) > 0:
            def empty_array():
                counter = 0
                array = [ None, None, None, None ]
                return counter, array
        
            def validate(array, badvalues):
                for axis in range(len(badvalues)):
                    for key in range(len(array[0])):
                        if array[axis][key].co[1] != badvalues[axis]:
                            return True;
                return False

            if debug:
                print("#actions")

            fps = bpy.context.scene.render.fps
            for action in bpy.data.actions:
                if debug:
                    print(len(action.name), action.name.encode('utf-8'))
                fd.write( struct.pack('<i', len(action.name) ) )
                fd.write( struct.pack('<'+str(len(action.name))+'s', action.name.encode('utf-8')) )

                if len(action.fcurves) == 0:
                    fd.write( struct.pack('<i', 0) )
                    continue

                # leave 4 byte space for the transformation count
                fd.seek(4, os.SEEK_CUR)
                offset = 4
                ntrans = 0
                counter, array = empty_array()

                fcurve_prec = action.fcurves[0]
                for fcurve in action.fcurves:
                    array[counter] = fcurve.keyframe_points

                    if counter == 2 and fcurve.data_path == "pose.bones[\"{}\"].location".format(fcurve.group.name):
                        if validate(array, [0.0, 0.0, 0.0]):
                            if debug:
                                print(bones[bone_index[fcurve.group.name]].name, len(fcurve.keyframe_points), b't')

                            bone_name = bones[bone_index[fcurve.group.name]].name
                            fd.write( struct.pack('<i'+str(len(bone_name))+'s', len(bone_name), bone_name.encode('utf-8')) )
                            fd.write( struct.pack( '<i', len(fcurve.keyframe_points)) )
                            fd.write( struct.pack( '<c', b't') )
                            offset += 9+len(bone_name)
                            for i in range(len(array[0])):
                                if debug:
                                    print_floats( ((array[0][i].co[0]/fps), array[0][i].co[1], array[1][i].co[1], array[2][i].co[1]) )
                                fd.write( struct.pack('<5f', (array[0][i].co[0]/fps), array[0][i].co[1], array[1][i].co[1], array[2][i].co[1], 0.0) )
                                offset += 20
                            ntrans += 1
                        counter, array = empty_array()
         
                    elif counter == 2 and fcurve.data_path == "pose.bones[\"{}\"].scale".format(fcurve.group.name):
                        if validate(array, [1.0, 1.0, 1.0]):
                            if debug:
                                print(bones[bone_index[fcurve.group.name]].name, len(fcurve.keyframe_points), b's')

                            bone_name = bones[bone_index[fcurve.group.name]].name
                            fd.write( struct.pack('<i'+str(len(bone_name))+'s', len(bone_name), bone_name.encode('utf-8')) )
                            fd.write( struct.pack( '<i', len(fcurve.keyframe_points)) )
                            fd.write( struct.pack( '<c', b's') )
                            offset += 9+len(bone_name)
                            for i in range(len(array[0])):
                                if debug:
                                    print_floats( ((array[0][i].co[0]/fps), array[0][i].co[1], array[1][i].co[1], array[2][i].co[1]) )
                                fd.write( struct.pack('<5f', (array[0][i].co[0]/fps), array[0][i].co[1], array[1][i].co[1], array[2][i].co[1], 0.0) )
                                offset += 20
                            ntrans += 1
                        counter, array = empty_array()
                    
                    elif counter == 3 and fcurve.data_path == "pose.bones[\"{}\"].rotation_quaternion".format(fcurve.group.name):
                        if validate(array, [1.0, 0.0, 0.0, 0.0]):
                            if debug:
                                print(bones[bone_index[fcurve.group.name]].name, len(fcurve.keyframe_points), b'r')
                            
                            bone_name = bones[bone_index[fcurve.group.name]].name
                            bone_quat = bones[bone_index[fcurve.group.name]].matrix.to_quaternion()
                            fd.write( struct.pack('<i'+str(len(bone_name))+'s', len(bone_name), bone_name.encode('utf-8')) )
                            fd.write( struct.pack( '<i', len(fcurve.keyframe_points)) )
                            fd.write( struct.pack( '<c', b'r') )
                            offset+=9+len(bone_name)
                            quat = Quaternion();

                            for i in range(len(array[0])):
                                quat.w = array[0][i].co[1]
                                quat.x = array[1][i].co[1]
                                quat.y = array[2][i].co[1]
                                quat.z = array[3][i].co[1]

                                if debug:
                                    print_floats( ((array[0][i].co[0]/fps), quat.x, quat.y, quat.z, quat.w) )
                                fd.write( struct.pack('<5f', (array[0][i].co[0]/fps), quat.x, quat.y, quat.z, quat.w) )
                                offset += 20
                            ntrans += 1
                        counter, array = empty_array()
                
                    elif counter != 0 and fcurve_prec.group.name != fcurve.group.name:
                        print("errore fcurve not supported: "+fcurve.group.name+" !!!")
                        counter, array = empty_array()

                    elif counter > 4:
                        print("error on fcurves format!")
                        counter, array = emtpy_array()

                    else:
                        counter += 1
            
                    fcurve_prec = fcurve

                fd.seek(-offset, os.SEEK_CUR)
                if debug:
                    print(ntrans)
                fd.write( struct.pack('<i', ntrans) )
                fd.seek(offset-4, os.SEEK_CUR)

    fd.close()

### blender exporter class definition ###
from bpy.props import StringProperty

class vboexporter(bpy.types.Operator):
    bl_idname = "export.vbo"
    bl_label = "Export VBO"

    filepath = StringProperty(subtype='FILE_PATH')

    def execute(self, context):
        filepath = bpy.path.ensure_ext(self.filepath, ".vbo");
        export_vbo(filepath)
        return {"FINISHED"}
    
    # open the filepath save window
    def invoke(self, context, event):
        if not self.filepath:
            self.filepath = bpy.path.ensure_ext(bpy.data.filepath, ".vbo")
        WindowManager = context.window_manager
        WindowManager.fileselect_add(self)
        return {"RUNNING_MODAL"}


### registering functions ###
def menu_func(self, context):
    self.layout.operator(vboexporter.bl_idname, text="Turbine Engine (.vbo)")

def register():
    bpy.utils.register_module(__name__)
    bpy.types.INFO_MT_file_export.append(menu_func)

def unregister():
    bpy.utils.unregister_module(__name__)
    bpy.types.INFO_MT_file_export.remove(menu_func)

if __name__ == "__main__":
    register()

