/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef EVENTRECEIVER_H
#define EVENTRECEIVER_H

#include <memory>
#include <TurbineIEventReceiver.h>
#include <TurbineEngine.h>

class VBOViewerEReceiver : public Turbine::IEventReceiver
{
    std::shared_ptr<Turbine::Engine> e;

    Turbine::Camera *camera;
    Turbine::IObject *light;

    bool moveCamera;
    int lastx, lasty;

public:

    VBOViewerEReceiver() : moveCamera(false)
    {
        e = Turbine::Engine::getInstance();
    }

    ~VBOViewerEReceiver() {}

    bool onEvent(const Turbine::Event& event);

    void init();
};

#endif /* EVENTRECEIVER_H */
