/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include "EventReceiver.h"

using Turbine::Event;
using Turbine::Vector3d;
using Turbine::Engine;

bool VBOViewerEReceiver::onEvent(const Event& event)
{
    switch(event.type)
    {
case Turbine::EV_DRAW:
        break;

    case Turbine::EV_QUIT:
    {
        std::cout << "event quit!" << std::endl;
        return false;
    }

    case Turbine::EV_KEYBOARD:
    {
        Vector3d v;

        if(event.evKey.key == Turbine::KEY_ESCAPE)
            if( !event.evKey.pressed )
                return false;

        if(event.evKey.pressed && camera != 0)
        {
            switch(event.evKey.key)
            {
                // ALT + INVIO
            case Turbine::KEY_RETURN:
                if(event.evKey.alt)
                    e->toggleFullscreen();
                break;

                // polar coordinates
            case Turbine::KEY_ADD:
                camera->zoom(1.0f);
                break;

            case Turbine::KEY_SUBTRACT:
                camera->zoom(-1.0f);
                break;

            case Turbine::KEY_RIGHT:
                camera->rotateOriz(1.0f);
                break;

            case Turbine::KEY_LEFT:
                camera->rotateOriz(-1.0f);
                break;

            case Turbine::KEY_UP:
                camera->rotateVert(1.0f);
                break;

            case Turbine::KEY_DOWN:
                camera->rotateVert(-1.0f);
                break;

            case Turbine::KEY_KEY_E:
                Engine::getInstance()->toggleLighting();
                break;

            case Turbine::KEY_KEY_U:
                v=light->getPosition();
                v.x += 10.0f;
                light->setPosition(v);
                break;

            case Turbine::KEY_KEY_J:
                v=light->getPosition();
                v.x -= 10.0f;
                light->setPosition(v);
                break;

            case Turbine::KEY_KEY_I:
                v=light->getPosition();
                v.y += 10.0f;
                light->setPosition(v);
                break;

            case Turbine::KEY_KEY_K:
                v=light->getPosition();
                v.y -= 10.0f;
                light->setPosition(v);
                break;

            case Turbine::KEY_KEY_O:
                v=light->getPosition();
                v.z += 10.0f;
                light->setPosition(v);
                break;

            case Turbine::KEY_KEY_L:
                v=light->getPosition();
                v.z -= 10.0f;
                light->setPosition(v);
                break;

            default:
                break;
            }
        }

        break;
    }

    case Turbine::EV_MOUSE:
    {
        //cout << "mouse event! " << event.evMouse.button << " " << event.evMouse.pressed << endl;

        switch(event.evMouse.button)
        {
        case Turbine::BTN_MOVE:
        {
            if(moveCamera)
            {
                int oriz = (event.evMouse.x - lastx);
                int vert = (event.evMouse.y - lasty);

                if(oriz)
                    camera->rotateOriz(-oriz);

                if(vert)
                    camera->rotateVert(vert);

                lastx = event.evMouse.x;
                lasty = event.evMouse.y;
            }
            break;
        }

        case Turbine::BTN_WHEELUP:
        {
            if(event.evMouse.pressed)
                camera->zoom(3.0f);
            break;
        }

        case Turbine::BTN_WHEELDOWN:
        {
            if(event.evMouse.pressed)
                camera->zoom(-3.0f);
            break;
        }

        case Turbine::BTN_RIGHT:
        {
            if(event.evMouse.pressed)
            {
                moveCamera = true;
                lastx = event.evMouse.x;
                lasty = event.evMouse.y;
            }
            else
                moveCamera = false;
            break;
        }

        default:
            break;
        }
    }

    default:
        break;
    }

    return true;
}

void VBOViewerEReceiver::init()
{
    camera = e->getSceneManager()->getCamera("Camera");
    light = e->getSceneManager()->getLight("Light");
}
