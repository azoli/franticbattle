/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <stdio.h>
#include <stdlib.h>
#include <string>
#include <sys/time.h>
#include <TurbineEngine.h>
#include "EventReceiver.h"

using Turbine::Engine;
using Turbine::WindowSettings;
using Turbine::SceneManager;
using Turbine::Vector3d;
using Turbine::Model;
using Turbine::Entity;

struct timeval now, start;

float getTicks()
{
    gettimeofday(&now, NULL);
    return (now.tv_sec-start.tv_sec)+(now.tv_usec-start.tv_usec)/1000000.0f;
}

void initTicks()
{
    gettimeofday(&start, NULL);
}

int main(int argc, char**argv)
{
    if(argc<2)
    {
        std::cout << "No .blend file argument specified" << std::endl;
        exit(0);
    }

    //Create window
    WindowSettings sett;

    sett.title = "vboViewer";
    sett.width = 0;            // use current resolution
    sett.height = 0;
    sett.colorbits = 32;
    sett.depthbits = 32;
    sett.fullscreen = false;

    //Create engine with FranticBattle eventReceiver
    VBOViewerEReceiver eventReceiver;
    std::shared_ptr<Engine> engine = Engine::getInstance();
    engine->create(Turbine::OPENGL, sett, &eventReceiver);

    SceneManager* smgr = engine->getSceneManager();

    //Add camera
    smgr->createCamera("Camera", true, Vector3d(0.0, 0.0, 50.0));

    //Add light
    smgr->createLight("Light", Turbine::L_DIRECTIONAL, Vector3d(0.0, 0.0, 50.0));

    std::string entityName = argv[1];
    Model* model = smgr->loadModel(entityName.c_str());
    if(model == 0)
    {
        std::cout << "wrong mesh" << std::endl;
        exit(0);
    }

    bool animatedModel = false;
    Entity* m = smgr->createEntity(engine->getAbsolutePath(entityName).c_str(), model, Vector3d(0.0f,0.0f,0.0f), Vector3d(0.0f,0.0f,0.0f));
    std::string action = "Idle";
    if( m->setAction(action) )
        animatedModel = true;

    eventReceiver.init();

    initTicks();

    unsigned int FPS = 0;
    float ticks = getTicks();
    float lastTicks = 0;
    float counter = 0.0f;

    while( engine->run() )
    {
        lastTicks=ticks;
        ticks=getTicks();

        // print fps every second
        counter += ticks-lastTicks;
        if( counter >= 1.0f )
        {
            std::cout << "FPS: " << FPS << std::endl;
            FPS = 0;
            counter = 0;
        }
        FPS++;

        if(animatedModel)
            m->updateTime(ticks-lastTicks);

        engine->draw();
    }

    return 1;
}
