/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#include <iostream>
#include <memory>
#include <stdio.h>
#include <sstream>
#ifdef _WIN32
#include <windows.h>
#endif
#include <TurbineEngine.h>
#include <TurbineFontBuilder.h>
#include "FBEventReceiver.h"
#include "FBMap.h"

using std::string;
using std::cout;
using std::endl;
using std::stringstream;

using Turbine::WindowSettings;
using Turbine::Engine;
using Turbine::SceneManager;
using Turbine::Vector2d;
using Turbine::Vector3d;
using Turbine::Model;
using Turbine::Entity;
using Turbine::FontBuilder;
using Turbine::FontInstance;
using Turbine::Image;
using Turbine::ImageSet;
using Turbine::GUIManager;
using Turbine::GUIStaticText;

#ifdef _WIN32
int APIENTRY WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow){
#elif __unix__
int main(){
#endif
    bool printFps = false;
    bool resetTime = false;

    // create engine
    WindowSettings sett;
    sett.title = "Frantic Battle!";
    sett.width = 0;
    sett.height = 0;
    sett.colorbits = 32;
    sett.depthbits = 32;
    sett.fullscreen = false;
    std::shared_ptr<Engine> engine = Engine::getInstance();
    std::shared_ptr<Turbine::Timer> timer = engine->getNewTimer();
    engine->create(Turbine::OPENGL, sett, NULL);

    // load arial font
    FontBuilder fb;
    fb.init();
    FontInstance* arial12 = fb.build(engine->getAbsolutePath("arial.ttf"), 12);

    // create level0
    FB::Level* level0 = new FB::Level();
    Turbine::SceneManager* smgr = level0->getSceneManager();
    FB::Map* map = level0->getMap();
    FB::Simulator* sim = level0->getSimulator();
    smgr->createCamera("Camera", true, Vector3d(0.0f, 200.0f, 350.0f));
    smgr->createLight("Light", Turbine::L_DIRECTIONAL, Vector3d(0.0f, 50.0f, 0.0f));

    // create event handler
    FB::EventReceiver eventReceiver(map, sim, &resetTime, &printFps, level0);
    engine->setEventReceiver(&eventReceiver);

    // load models
    // TODO use model.clone() instead reloading from file..
    const unsigned int MODEL_NUM = 3;
    std::vector<Model*> models;
    models.resize(MODEL_NUM*2);
    models[0] = smgr->loadModel(engine->getAbsolutePath("armored.vbo"));
    models[1] = smgr->loadModel(engine->getAbsolutePath("robz.vbo"));
    models[2] = smgr->loadModel(engine->getAbsolutePath("cyborg.vbo"));
    models[3] = smgr->loadModel(engine->getAbsolutePath("armored.vbo"));
    models[4] = smgr->loadModel(engine->getAbsolutePath("robz.vbo"));
    models[5] = smgr->loadModel(engine->getAbsolutePath("cyborg.vbo"));

    std::vector<Model*> bullets;
    bullets.resize(MODEL_NUM);
    bullets[0] = smgr->loadModel(engine->getAbsolutePath("armored_bullet.vbo"));
    bullets[1] = smgr->loadModel(engine->getAbsolutePath("robz_bullet.vbo"));
    bullets[2] = smgr->loadModel(engine->getAbsolutePath("cyborg_ray.vbo"));

    // Create select indicator
    Turbine::Quad quad;
    int halfCell = map->getcellSize() / 2.0;
    quad.coords[0] = Vector3d(-halfCell, 0.0,  halfCell);
    quad.coords[1] = Vector3d( halfCell, 0.0,  halfCell);
    quad.coords[2] = Vector3d( halfCell, 0.0, -halfCell);
    quad.coords[3] = Vector3d(-halfCell, 0.0, -halfCell);
    for(int i=0; i<4; i++)
        quad.normals[i] = Vector3d(0.0, 1.0, 0.0);
    quad.texCoords[0] = Vector2d(0.0, 0.0);
    quad.texCoords[1] = Vector2d(1.0, 0.0);
    quad.texCoords[2] = Vector2d(1.0, 1.0);
    quad.texCoords[3] = Vector2d(0.0, 1.0);
    std::vector<Turbine::Quad> quads;
    quads.push_back(quad);
    Turbine::Material matSel;
    matSel.texture = Turbine::TextureLoader::getInstance()->loadPNG(engine->getAbsolutePath("selected.png"));
    Turbine::Model* sel = smgr->createQuadsModel(quads, matSel);

    // setup team colors
    Turbine::Colorf teamColor(0.31, 0.207, 0.0585, 1.0);
    Turbine::Colorf enemyColor(0.9, 1.0, 1.0, 1.0);

    for(unsigned int i=0; i<MODEL_NUM; i++)
    {
        std::vector<Turbine::Material>::iterator mat;
        for(mat = models[i]->materials.begin(); mat != models[i]->materials.end(); mat++)
        {
            if(mat->name == "TeamColor")
            {
                mat->kd = teamColor;
                break;
            }
        }
        for(mat = models[MODEL_NUM+i]->materials.begin(); mat != models[MODEL_NUM+i]->materials.end(); mat++)
        {
            if(mat->name == "TeamColor")
            {
                mat->kd = enemyColor;
                break;
            }
        }
    }

    const unsigned int ENTITY_NUM = 5;

    // properties:       health, armor, attackType, attackDamage, attackPrepTime, reloadSpeed, mobility, range
    const FB::UnitProperties prop[3] = { { 200, 20, FB::AT_PHASER, 30, 0.0, 1.5, 0.5, 1 },   // armored soldier
                                         { 100,  0, FB::AT_PHASER, 25, 0.0, 1.0, 1.0, 3 },   // robot
                                         { 300, 10, FB::AT_RAY,    40, 0.3, 2.0, 0.5, 2 } }; // cyborg
    Vector3d barOffsets[3] = { Vector3d(0.0, 19.0, 0.0),
                               Vector3d(0.0, 7.0, 0.0),
                               Vector3d(0.0, 9.0, 0.0) };

    // add units to the level
    for(unsigned int i=0; i<MODEL_NUM*2; i++)
    {
        int teamNum = (i < MODEL_NUM) ? 1 : 2;
        FB::UnitParameters params;
        params.name = models[i]->name;
        params.model = models[i];
        params.bullet = bullets[i%MODEL_NUM];
        params.selIndicator = sel;
        params.prop = prop[i%MODEL_NUM];
        params.teamNum = teamNum;
        params.barOffset = barOffsets[i%MODEL_NUM];
        level0->addUnits(ENTITY_NUM, params);
    }

    level0->start();

    unsigned int FPS = 0;
    float time = timer->getElapsedSeconds();
    float lastTime, delta = 0, counter = 0;
    GUIManager* guimgr = Turbine::Engine::getInstance()->getGUIManager();
    GUIStaticText* fpsText = 0;
    GUIStaticText* winText = 0;

    while( engine->run() ) // handle input
    {
        // update time
        if(resetTime)
        {
            resetTime = false;
            timer.reset();
            time = 0;
        }
        lastTime = time;
        time = timer->getElapsedSeconds();
        delta = time - lastTime;
        counter += delta;

        if( counter >= 1.0f ) // when a second has passed
        {
            if(fpsText != 0)
            {
                guimgr->destroyStaticText(fpsText);
                fpsText = 0;
            }

            if( printFps )
            {
                stringstream ss;
                ss << "FPS: " << FPS;
                fpsText = guimgr->createStaticText(ss.str(), arial12);
                fpsText->setOffset(Turbine::Vector2d(0.02, 0.95));

                counter = 0.0f;
                FPS = 0;
            }

        }
        FPS++;

        // update simulation
        int winnerTeam = sim->run(time);

        if(winText != 0)
        {
            guimgr->destroyStaticText(winText);
            winText = 0;
        }

        if(winnerTeam)
        {
            stringstream ss;
            ss << "Team " << winnerTeam << " won!";
            winText = guimgr->createStaticText(ss.str(), arial12);
            winText->setOffset(Turbine::Vector2d(0.48, 0.90));
        }

        // update animations
        smgr->updateTime(delta);

        // update armored healthbar
        for(int y=0; y<map->getColumn(); y++)
        {
            for(int x=0; x<map->getRow(); x++)
            {
                std::shared_ptr<FB::Unit> unit = map->getUnit(FB::MapPosition(x, y));
                if(unit)
                {
                    unit->updateHealthBar();
                    unit->updateSelectIndicator();
                }
            }
        }

        // draw a frame
        engine->draw();
    }

    for(unsigned int i=0; i<MODEL_NUM*2; i++)
    {
        smgr->destroyModel(models[i]);
    }

    return 1;
}

