/*  This file is a part of the Turbine game engine.

    Copyright (C) 2012 Andrea Zoli

    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
*/

#ifndef FBEVENT_H
#define FBEVENT_H

#include <utility>
#include <memory>
#include <TurbineEntity.h>
#include "FBUnit.h"
#include "FBMapPosition.h"

namespace FB
{

enum EventType
{
    EV_IDLE,
    EV_ROTATE,
    EV_END_ROTATE,
    EV_MOVE,
    EV_END_MOVE,
    EV_ATTACK,
    EV_SHOOT,
    EV_HIT_CELL,
    EV_END_SHOOT,
    EV_END_RELOAD,
    EV_CROSSING,
    EV_DEATH,
    EV_WINNER,
};

class Unit;
struct Event
{
    EventType type;
    float time;
    std::shared_ptr<Unit> unit;

    Event(EventType ltype, std::shared_ptr<Unit> lunit, float ltime) : type(ltype), time(ltime), unit(lunit){}
    virtual ~Event(){}
};

struct RotationEvent : public Event
{
    float degrees;
    RotationEvent(std::shared_ptr<Unit> lunit, float ltime, float ldegrees) : Event(EV_ROTATE, lunit, ltime), degrees(ldegrees){}
    ~RotationEvent(){}
};

struct EndRotationEvent : public Event
{
    float degrees;
    EndRotationEvent(std::shared_ptr<Unit> lunit, float ltime, float ldegrees) : Event(EV_END_ROTATE, lunit, ltime), degrees(ldegrees){}
    ~EndRotationEvent(){}
};

struct MoveEvent : public Event
{
    MapPosition step;
    MoveEvent(std::shared_ptr<Unit> lunit, float ltime, MapPosition lstep) : Event(EV_MOVE, lunit, ltime), step(lstep){}
    ~MoveEvent(){}
};

struct EndMoveEvent : public Event
{
    float translation;
    EndMoveEvent(std::shared_ptr<Unit> lunit, float ltime, float ltranslation) : Event(EV_END_MOVE, lunit, ltime), translation(ltranslation){}
    ~EndMoveEvent(){}
};

struct CrossingEvent : public Event
{
    std::pair<MapPosition, MapPosition> step;
    CrossingEvent(std::shared_ptr<Unit> lunit, float ltime, std::pair<MapPosition, MapPosition> lstep) : Event(EV_CROSSING, lunit, ltime), step(lstep){}
    ~CrossingEvent(){}
};

struct AttackEvent : public Event
{
    std::shared_ptr<Unit> enemy;
    AttackEvent(std::shared_ptr<Unit> lunit, float ltime, std::shared_ptr<Unit> lenemy) : Event(EV_ATTACK, lunit, ltime), enemy(lenemy){}
    ~AttackEvent(){}
};

struct ShootEvent : public Event
{
    std::shared_ptr<Unit> enemy;
    ShootEvent(std::shared_ptr<Unit> lunit, float ltime, std::shared_ptr<Unit> lenemy) : Event(EV_SHOOT, lunit, ltime), enemy(lenemy){}
    ~ShootEvent(){}
};

struct HitCellEvent : public Event
{
    MapPosition target;
    HitCellEvent(std::shared_ptr<Unit> lunit, float ltime, MapPosition ltarget) : Event(EV_HIT_CELL, lunit, ltime), target(ltarget){}
    ~HitCellEvent(){}
};

struct EndShootEvent : public Event
{
    std::shared_ptr<Unit> enemy;
    Turbine::Entity* bulletEntity;
    EndShootEvent(std::shared_ptr<Unit> lunit, float ltime, std::shared_ptr<Unit> lenemy, Turbine::Entity* lbulletEntity) : Event(EV_END_SHOOT, lunit, ltime), enemy(lenemy), bulletEntity(lbulletEntity){}
    ~EndShootEvent(){}
};

}

#endif
